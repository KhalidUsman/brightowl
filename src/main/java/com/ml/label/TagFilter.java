/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author shahzad
 */
public class TagFilter {
    
    public static void filterOverlappingTags(List<Tag> tags){
        List<Tag> filterTags = new ArrayList<>(); 
        Tag cTag = tags.get(0);
        for(int j = 1 ; j < tags.size() ; j++){
            for(int i = j; i < tags.size() ; i++){
                Tag current = tags.get(i);
                
                // Subset Case
                if(current.getStartIndex() >= cTag.getStartIndex() 
                        && current.getEndIndex() <= cTag.getEndIndex()){
                    filterTags.add(current);
                }
                // Other Subset case
                else if(cTag.getStartIndex() >= current.getStartIndex() 
                        && cTag.getEndIndex() <= current.getEndIndex()){
                    filterTags.add(cTag);
                    break;
                }

                // Overlapping case
                else if(cTag.getStartIndex() < current.getStartIndex() 
                        && cTag.getEndIndex() < current.getEndIndex()
                        && cTag.getEndIndex() > current.getStartIndex()){
                    
                    if(cTag.getPriority() > current.getPriority()){
                        filterTags.add(current);
                    }
                    else if(cTag.getPriority() < current.getPriority()){
                        filterTags.add(cTag);
                        break;
                    }
                    else{
                        if((cTag.getEndIndex() - cTag.getStartIndex()) > (current.getEndIndex() - current.getStartIndex())){
                            filterTags.add(current);
                        }
                        else{
                            //tags.remove(cTag);
                            filterTags.add(cTag);
                            break;
                        }
                    }  
                }    
            }
            cTag = tags.get(j);
        }
        
        tags.removeAll(filterTags);
    }
}
