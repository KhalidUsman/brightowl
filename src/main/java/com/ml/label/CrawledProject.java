/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import com.dbConnection.Connect_Database;
import com.dbConnection.Project;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author shahzad
 */
public class CrawledProject {

    public static List<String> getCrawledProjectsText(int startCursor, int offset) throws Exception {
        Connect_Database dao = new Connect_Database();
        dao.readDataBase();
        List<String> rawData = new ArrayList<>();
        try (ResultSet rs = dao.getProjectsWithLimit(startCursor, offset)) {
            while (rs.next()) {

                Project projectObj = new Project(rs.getInt("project_id"));
                if (projectObj._crawlId == 1) {
                    rawData.add(projectObj._crawlText);
                }
            }
        }

        return rawData;
    }
}
