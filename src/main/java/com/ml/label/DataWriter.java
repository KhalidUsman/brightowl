/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import com.ml.training.TextPreProcessor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.xml.transform.*;
import javax.xml.parsers.*;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

/**
 *
 * @author shahzad
 */
public class DataWriter {

    protected static final String TRAINING_DATA_PATH = "/Users/shahzad/sentiance/brightowl/data/TrainingDataNew";
    protected static final String RAW_DATA_PATH = "/Users/shahzad/sentiance/brightowl/data/RawDataNew";

    public static void writeRawDataToFile(List<String> rawData, String fileName) throws FileNotFoundException, IOException, UnsupportedEncodingException, SAXException, TikaException {
        File directory = new File(RAW_DATA_PATH);
        if (!directory.exists()) {
            directory.mkdir();
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
        }

        File fout = new File(RAW_DATA_PATH + File.separator + fileName);
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (String data : rawData) {
            bw.write(TextPreProcessor.normalizeText(data));
            bw.newLine();
        }
        bw.close();
    }

    public static List<String> readDataFromFile(String fileName) throws IOException {
        BufferedReader br = null;
        FileReader fr = null;
        List<String> rawData = new ArrayList<>();
        try {

            fr = new FileReader(RAW_DATA_PATH + File.separator + fileName);
            br = new BufferedReader(fr);

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                rawData.add(sCurrentLine);
            }

        } catch (IOException e) {
            throw e;
        } finally {
            try {

                if (br != null) {
                    br.close();
                }

                if (fr != null) {
                    fr.close();
                }
            } catch (IOException ex) {
                throw ex;
            }
        }
        return rawData;
    }

    public static void writeLabelData(List<String> labeledList, String fileName) throws ParserConfigurationException, SAXException, IOException, TransformerException {

        //String directoryName = TRAINING_DATA_PATH;
        File directory = new File(TRAINING_DATA_PATH);
        if (!directory.exists()) {
            directory.mkdir();
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
        }

        File fout = new File(TRAINING_DATA_PATH + File.separator + fileName);
        FileOutputStream fos = null;
        BufferedWriter bw = null;
        try {
            fos = new FileOutputStream(fout);
            bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write("<root>");
            for (String labeledData : labeledList) {
                bw.write("<s>" + labeledData.replaceAll("&", "&amp;") + "</s>");
                bw.newLine();
            }
            bw.write("</root>");
            bw.close();
        } catch (IOException e) {
            throw e;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                throw ex;
            }

        }

//        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
//                .newInstance();
//        DocumentBuilder documentBuilder = documentBuilderFactory
//                .newDocumentBuilder();
//        Document document = documentBuilder.newDocument();
//
//        // Root Element
//        Element rootElement = document.createElement("root");
//        document.appendChild(rootElement);
//
//        for (String labeledData : labeledList) {
//            // server elements
//            Element s = document.createElement("s");
//            s.appendChild(document.createTextNode(labeledData));
//            rootElement.appendChild(s);
//
//        }
//
//        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//        Transformer transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(document);
//
//        StreamResult result = new StreamResult(
//                TRAINING_DATA_PATH + File.separator + fileName);
//        transformer.transform(source, result);
        //normalizeTags(TRAINING_DATA_PATH + File.separator + fileName);
    }

    public static void writeFileForTraining(int startCursor, int offset) throws Exception {       
        String fileName = "RawData-" + startCursor + ".txt";
        System.out.println("Started writing: "+fileName);
        List<Integer> projects;
        List<String> rawDataList = CrawledProject.getCrawledProjectsText(startCursor, offset);
        writeRawDataToFile(rawDataList, fileName);
        System.out.println("Done writing: "+fileName);
    }

}
