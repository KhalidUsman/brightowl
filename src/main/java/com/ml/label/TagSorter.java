/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import java.util.List;

/**
 *
 * @author shahzad
 */
public class TagSorter {
    
    
    private Tag[] tags;
    private int length;
 
    public Tag[] sortTags(List<Tag> tags){
        this.tags  = tags.toArray(new Tag[tags.size()]);
        length = tags.size();
        quickSort(0, length - 1);
        return this.tags;
    }
    
//    public void sort(int[] inputArr) {
//         
//        if (inputArr == null || inputArr.length == 0) {
//            return;
//        }
//        
//    }
 
    private void quickSort(int lowerIndex, int higherIndex) {
         
        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = tags[lowerIndex+(higherIndex-lowerIndex)/2].getTagText().length();
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which 
             * is greater then the pivot value, and also we will identify a number 
             * from right side which is less then the pivot value. Once the search 
             * is done, then we exchange both numbers.
             */
            while (tags[i].getTagText().length() < pivot) {
                i++;
            }
            while (tags[j].getTagText().length() > pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(i, j);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < j)
            quickSort(lowerIndex, j);
        if (i < higherIndex)
            quickSort(i, higherIndex);
    }
 
    private void exchangeNumbers(int i, int j) {
        Tag temp = tags[i];
        tags[i] = tags[j];
        tags[j] = temp;
    }
    
    
    
//    public static void main(String a[]){
//         
//        TagSorter sorter = new TagSorter();
//        int[] input = {24,2,45,20,56,75,2,56,99,53,12};
//        sorter.sort(input);
//        for(int i:input){
//            System.out.print(i);
//            System.out.print(" ");
//        }
//    }
}
