/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label.spark;

import com.config.AppConfig;
import com.ml.label.DataWriter;
import com.ml.label.Tag;
import com.ml.training.TextPreProcessor;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Encoders;
import org.apache.spark.storage.StorageLevel;
import org.xml.sax.SAXException;
import scala.Tuple2;
//import com.databricks.spark.xml;

/**
 *
 * @author shahzad
 */
public class SparkRawDataCollector implements Serializable {

    private final AppConfig config;
    private final SparkSession spark;

    public SparkRawDataCollector(AppConfig config, SparkSession spark) {
        this.config = config;
        this.spark = spark;
    }

    public void getRawData(int offset) throws IOException {

        Configuration conf = new Configuration();
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        FileSystem spark_fs = FileSystem.get(URI.create(config.getRawDataLocation()), conf);
        String dumpFolder = config.getRawDataLocation() + File.separator + "Raw-" + offset;
        if (spark_fs.exists(new Path(dumpFolder))) {
            spark_fs.delete(new Path(dumpFolder), true);
        }

        // Hadoop conf properties
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", config.getDbUser());
        connectionProperties.put("password", config.getDbPassword());
        connectionProperties.put("characterEncoding", "UTF-8");

        Dataset<Row> jdbcDF = spark.read().jdbc(
                "jdbc:mysql://" + config.getDbHost() + "/" + config.getDbName(),
                config.getDbTable(),
                "project_id",
                1,
                70000,
                5,
                connectionProperties);

        jdbcDF.createOrReplaceTempView("projectsView");

        JavaRDD<String> crawlJobs = spark.sql("SELECT crawled_job_detail FROM projectsView WHERE is_crawled=1 LIMIT " + offset)
                .javaRDD()
                .map((Row row) -> TextPreProcessor.normalizeText(row.get(0).toString()));

        crawlJobs.persist(StorageLevel.MEMORY_AND_DISK());
        crawlJobs.repartition(100).saveAsTextFile(dumpFolder);
        System.out.println("Total Records" + crawlJobs.count());
    }

    public void getRawDataXML(int offset) throws IOException, ParserConfigurationException, SAXException, TransformerException {

        Configuration conf = new Configuration();
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        FileSystem spark_fs = FileSystem.get(URI.create(config.getRawDataLocation()), conf);
        String dumpFolder = config.getRawDataLocation() + File.separator + "Raw-" + offset;
        if (spark_fs.exists(new Path(dumpFolder))) {
            spark_fs.delete(new Path(dumpFolder), true);
        }

        // Hadoop conf properties
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", config.getDbUser());
        if (config.getDbPassword() == null) {
            connectionProperties.put("password", "");
        } else {
            connectionProperties.put("password", config.getDbPassword());
        }
        connectionProperties.put("characterEncoding", "UTF-8");

        Dataset<Row> jdbcDF = spark.read().jdbc(
                "jdbc:mysql://" + config.getDbHost() + "/" + config.getDbName() + "?autoReconnect=true",
                config.getDbTable(),
                "project_id",
                1,
                1000,
                6,
                connectionProperties);

        jdbcDF.createOrReplaceTempView("projectsView");

        Dataset<String> crawlJobs = spark.sql("SELECT project_id, crawled_job_detail FROM projectsView WHERE is_crawled=1 DISTRIBUTE BY project_id LIMIT " + offset)
                .map(
                        (MapFunction<Row, String>) row -> {
                            String nt = TextPreProcessor.normalizeText(row.get(1).toString());

                            //TagProcessor tp = new TagProcessor();
                            // Get all the unique applicable tags
                            List<Tag> tags = null;
                            try {
                                tags = TagProcessor.getUniqueTags(nt);
                            } catch (IOException ex) {
                                java.util.logging.Logger.getLogger(SparkLabelData.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if (tags.size() > 0) {
                                // label data
                                nt = SparkLabelData.lableData(nt, tags);

                            }
                            return nt;
                            //return row.get(0).toString();
                        },
                        Encoders.STRING()
                );
        System.out.println("Partitions:" + crawlJobs.rdd().getNumPartitions());
        //crawlJobs.persist(StorageLevel.MEMORY_AND_DISK());
        //System.out.println("Total projects founds: "+ crawlJobs.count());
        //crawlJobs.show();
//        crawlJobs.write()
//                .format("com.databricks.spark.xml")
//                .option("rootTag", "books")
//                .option("rowTag", "book")
//                .save(dumpFolder + File.separator + "newbooks.xml");

        List<String> labeledData = crawlJobs.collectAsList();
        DataWriter.writeLabelData(labeledData, "Training-" + offset + ".txt");
        //.map((Row row) -> TextPreProcessor.normalizeText(row.get(0).toString()));
//        crawlJobs.persist(StorageLevel.MEMORY_AND_DISK());
//        crawlJobs.repartition(
//                100).saveAsTextFile(dumpFolder);
//        System.out.println(
//                "Total Records" + crawlJobs.count());
    }

    public void getRawDataXMLByPair(int offset) throws IOException, ParserConfigurationException, SAXException, TransformerException {

        Configuration conf = new Configuration();
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        FileSystem spark_fs = FileSystem.get(URI.create(config.getTrainingDataLocation()), conf);
        String dumpFolder = config.getTrainingDataLocation() + File.separator + "Training-" + offset;
        if (spark_fs.exists(new Path(dumpFolder))) {
            spark_fs.delete(new Path(dumpFolder), true);
        }

        // Hadoop conf properties
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", config.getDbUser());
        if (config.getDbPassword() == null) {
            connectionProperties.put("password", "");
        } else {
            connectionProperties.put("password", config.getDbPassword());
        }
        connectionProperties.put("characterEncoding", "UTF-8");

        Dataset<Row> jdbcDF = spark.read().jdbc(
                "jdbc:mysql://" + config.getDbHost() + "/" + config.getDbName() + "?autoReconnect=true",
                config.getDbTable(),
                "project_id",
                1,
                10000,
                3,
                connectionProperties);

        jdbcDF.createOrReplaceTempView("projectsView");
        
        spark.sql("SELECT count(*) FROM projectsView WHERE is_crawled=1 LIMIT " + offset).show();
        
        Dataset<Row> crawlJobs = spark.sql("SELECT project_id, crawled_job_detail FROM projectsView WHERE is_crawled=1 DISTRIBUTE BY project_id LIMIT " + offset);
        //crawlJobs.show();
        JavaRDD<Row> rdd = crawlJobs.javaRDD().repartition(3);

        PairFunction<Row, Integer, String> keyData = new PairFunction<Row, Integer, String>() {
            @Override
            public Tuple2<Integer, String> call(Row x) {
                return new Tuple2(Integer.parseInt(x.get(0).toString()), x.get(0).toString());
            }
        };

        JavaPairRDD<Integer, String> pairs = rdd
                .mapToPair((Row row) -> new Tuple2(Integer.parseInt(row.get(0).toString()), row.get(1).toString()));

        System.out.println("Partitions:" + pairs.getNumPartitions());
        JavaPairRDD<Integer, String> labeledPair = pairs
                .filter(tuple -> {
                    String lang = TextPreProcessor.getTextLanguage(tuple._2);
                    return lang.equals("en");
                })
                .mapValues(project -> {
                    String nt = TextPreProcessor.normalizeText(project).replaceAll(",", "  ,").replaceAll("\\.", "  \\.");

                    //TagProcessor tp = new TagProcessor();
                    // Get all the unique applicable tags
                    List<Tag> tags = null;
                    try {
                        tags = TagProcessor.getUniqueTags(nt);
                    } catch (IOException ex) {
                        java.util.logging.Logger.getLogger(SparkLabelData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (tags.size() > 0) {
                        // label data
                        nt = SparkLabelData.lableData(nt, tags);

                    }
                    return nt;

                });
        
        labeledPair.map(tuple -> "<s>" + tuple._2 + "</s>").
                saveAsTextFile(dumpFolder);

    }

    public static void main(String[] args) throws AnalysisException, IOException, ParserConfigurationException, SAXException, TransformerException {
        int records = 30000;
        AppConfig config = AppConfig.createFromYamlSettings("local");
        SparkConf conf = new SparkConf().setMaster(config.getSparkMaster()).set("spark.ui.port", "7077");
        conf.setJars(new String[]{
            config.getAppJar()
        });
        conf.set("spark.executor.heartbeatInterval", "100000");
        conf.set("spark.akka.timeout", "60");
        conf.set("spark.executor.memory", "5g");
        conf.set("spark.executor.instances", "3");
        conf.set("spark.executor.cores", "1");

        SparkSession spark = SparkSession
                .builder()
                .config(conf)
                .appName("Raw Data Collector")
                .getOrCreate();

        SparkRawDataCollector test = new SparkRawDataCollector(config, spark);
        test.getRawDataXMLByPair(records);
        spark.stop();
        MUC6Compatible.makeFileMUC6Compatible(records, config);
    }

}
