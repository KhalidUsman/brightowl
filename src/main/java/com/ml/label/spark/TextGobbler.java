/**
 * Author: Shahzad 
 * 
 * Date: 09-11-2015
 */
package com.ml.label.spark;


import org.apache.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TextGobbler {

    private Logger logger = Logger.getLogger(TextGobbler.class);

    public BufferedWriter writer = null;
    private String runID;
    private String logLocation;
    private String type;

    TextGobbler(String type, String runID, String logLocation) {
        this.runID = runID;
        this.logLocation = logLocation;
        this.type = type;
        this.build();
    }

    /**
     * Initialize the output log file
     */
    private void build() {

        Path fileP = Paths.get(this.logLocation + File.separator + this.type + "_" + this.runID);

        Charset charset = Charset.forName("utf-8");
        try {
            this.writer = Files.newBufferedWriter(fileP, charset);
        } catch (IOException e) {
            logger.error("Error creating log file for process:" + this.type);
            logger.error(e);
        }
    }

    /**
     * Log in the file
     *
     * @param logText
     */
    public void logText(String logText) {
        try {
            this.writer.append(logText);
            this.writer.newLine();
            this.writer.flush();
        } catch (IOException e) {
            logger.error("Error writing to log file for process:" + this.type);
            logger.error(e);
        }
    }

    /**
     * Close the writer
     */
    public void close() {
        try {
            if (this.writer != null) {
                this.writer.close();
            }
        } catch (IOException e) {
            logger.error("Error closing log file for process:" + this.type);
            logger.error(e);
        }
    }

    protected void finalize() {
        try {
            if (this.writer != null) {
                this.writer.close();
            }
        } catch (IOException e) {
            logger.error("Error closing log file for process:" + this.type);
            logger.error(e);
        }
    }

}
