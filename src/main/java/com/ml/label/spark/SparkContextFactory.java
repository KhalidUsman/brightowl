package com.ml.label.spark;

import com.config.AppConfig;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SparkContextFactory {

    private static final Logger log = LoggerFactory.getLogger(SparkContextFactory.class);
    
    public static JavaSparkContext createFromConfig(AppConfig config, String appName) throws ClassNotFoundException {
        
        SparkConf conf = new SparkConf()
                .set("spark.local.dir", config.getTempFolder() + "/sparktmp").setAppName(appName);
        
        conf.registerKryoClasses(new Class<?>[]{
            Class.forName("org.apache.hadoop.io.LongWritable"),
            Class.forName("org.apache.hadoop.io.BytesWritable")
        });
        
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.hadoopConfiguration().set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem");
//    sc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", config.getS3AccesKeyId());
//    sc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", config.getS3SecretAccessKey());
//    sc.hadoopConfiguration().set("fs.s3a.awsAccessKeyId", config.getS3AccesKeyId());
//    sc.hadoopConfiguration().set("fs.s3a.awsSecretAccessKey", config.getS3SecretAccessKey());
//    sc.hadoopConfiguration().set("fs.s3a.access.key", config.getS3AccesKeyId());
//    sc.hadoopConfiguration().set("fs.s3a.secret.key", config.getS3SecretAccessKey());
        sc.hadoopConfiguration().set("fs.s3a.connection.timeout", "500000");
        sc.hadoopConfiguration().set("hadoop.tmp.dir", config.getTempFolder() + "/hadooptmp");
        return sc;
    }
    
    public static JavaSparkContext createFromConfig(AppConfig config) throws ClassNotFoundException {
        
        SparkConf conf = new SparkConf()
                .set("spark.local.dir", config.getTempFolder() + "/sparktmp")
                .setMaster(config.getSparkMaster()).set("spark.ui.port", "7077");
        conf.setJars(new String[]{
            config.getAppJar()
        })
                .setAppName("Training").setSparkHome(config.getSparkHome());
        
        conf.registerKryoClasses(new Class<?>[]{
            Class.forName("org.apache.hadoop.io.LongWritable"),
            Class.forName("org.apache.hadoop.io.BytesWritable")
        });
        
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.hadoopConfiguration().set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem");
//    sc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", config.getS3AccesKeyId());
//    sc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", config.getS3SecretAccessKey());
//    sc.hadoopConfiguration().set("fs.s3a.awsAccessKeyId", config.getS3AccesKeyId());
//    sc.hadoopConfiguration().set("fs.s3a.awsSecretAccessKey", config.getS3SecretAccessKey());
//    sc.hadoopConfiguration().set("fs.s3a.access.key", config.getS3AccesKeyId());
//    sc.hadoopConfiguration().set("fs.s3a.secret.key", config.getS3SecretAccessKey());
        sc.hadoopConfiguration().set("fs.s3a.connection.timeout", "500000");
        sc.hadoopConfiguration().set("hadoop.tmp.dir", config.getTempFolder() + "/hadooptmp");
        return sc;
    }
}
