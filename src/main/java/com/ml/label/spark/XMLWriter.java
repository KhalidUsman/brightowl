/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label.spark;

import com.config.AppConfig;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext ;
        
        /**
         *
         * @author shahzad
         */

import org.apache.spark.sql.SparkSession;
public class XMLWriter {

    public static void main(String args[]) {
        
        AppConfig config = AppConfig.createFromYamlSettings("local");
        SparkConf conf = new SparkConf().setMaster(config.getSparkMaster()).set("spark.ui.port", "7077");
        conf.setJars(new String[]{
            config.getAppJar()
        });
        conf.set("spark.executor.heartbeatInterval", "100000");
        conf.set("spark.akka.timeout", "60");
//        conf.set("spark.executor.memory", "4g");
//        conf.set("spark.executor.instances", "1");
//        conf.set("spark.executor.cores", "1");

        SparkSession spark = SparkSession
                .builder()
                .config(conf)
                .appName("Raw Data Collector")
                .getOrCreate();

        //SQLContext sqlContext = new SQLContext(sc);
        Dataset<Row> df = spark.read()
                .format("com.databricks.spark.xml")
                .option("rowTag", "book")
                .load("/Users/shahzad/Downloads/books.xml");

        df.select("author", "_id").coalesce(1).write()
                .format("com.databricks.spark.xml")
//                .option("rootTag", "books")
//                .option("rowTag", "book")
                .save("/Users/shahzad/Downloads/newbooks.xml");

// df.select("author", "_id").coalesce(1).write()
//                .format("json")
//             
//                .save("/Users/shahzad/Downloads/newbooks.xml");
    }
}
