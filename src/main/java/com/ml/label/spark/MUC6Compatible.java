/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label.spark;

import com.config.AppConfig;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 *
 * @author shahzad
 */
public class MUC6Compatible {

    public static void makeFileMUC6Compatible(int offset, AppConfig config) throws IOException {
        String tPath = config.getTrainingDataLocation().replaceAll("file://", "") + File.separator + "Training-" + offset;
        ArrayList<File> corpusFiles = new ArrayList<>();
        try (Stream<java.nio.file.Path> paths = Files.walk(Paths.get(tPath))) {
            paths
                    .filter(path -> Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS))
                    .forEach(f -> corpusFiles.add(f.toFile()));
        }

        for (int i = 0; i < corpusFiles.size(); ++i) {
            if (corpusFiles.get(i).isFile() && (!corpusFiles.get(i).getName().endsWith(".crc")) && !corpusFiles.get(i).getName().equals("_SUCCESS")) {
                try (FileReader fileReader = new FileReader(corpusFiles.get(i))) {
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("<root>");
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line.replaceAll("&", "&amp;"));
                        stringBuilder.append("\n");
                    }
                    stringBuilder.append("</root>");
                    if (corpusFiles.get(i).delete()) {
                        //File file = new File("path/to/file.txt");
                        try (BufferedWriter writer = new BufferedWriter(new FileWriter(corpusFiles.get(i)))) {
                            writer.write(stringBuilder.toString());
                        }
                    }
                }

                System.out.println("Done processing:" + corpusFiles.get(i));
            }
        }
    }
}
