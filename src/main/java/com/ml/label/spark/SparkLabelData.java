/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label.spark;

import com.config.AppConfig;
import com.ml.label.Tag;
import com.ml.label.TagSorter;
import com.ml.training.TextPreProcessor;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.regex.Pattern;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.tika.exception.TikaException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author shahzad
 */
public class SparkLabelData implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(SparkLabelData.class);

    private final AppConfig config;
    private final JavaSparkContext sc;
    //private final TagProcessor tp;

    public SparkLabelData(AppConfig config, JavaSparkContext sc) {
        this.config = config;
        this.sc = sc;
        //tp = new TagProcessor();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        System.out.println("Entering labelr");

        AppConfig config = AppConfig.createFromYamlSettings(args[0]);
        JavaSparkContext sc = SparkContextFactory.createFromConfig(config);

        SparkLabelData labelr = new SparkLabelData(config, sc);
        labelr.label(args[1]);
        sc.stop();
    }

    public void label(String startCursor) throws IOException {
        // Read input data
        //sc.broadcast(tp);
        String outputLocation = config.getTrainingDataLocation() + File.separator + "TrainingData-" + startCursor;
        FileSystem sparkFsOut = FileSystem.get(URI.create(config.getTrainingDataLocation()), sc.hadoopConfiguration());

        System.out.println("Clearing old directories");
        FileStatus[] status = sparkFsOut.listStatus(new Path(config.getTrainingDataLocation()));
        for (FileStatus fileStatus : status) {
            System.out.println("Clearing out: " + fileStatus.getPath());
            sparkFsOut.delete(fileStatus.getPath(), true);
        }

        JavaRDD<String> lines = sc.textFile(config.getRawDataLocation() + "/*");
        JavaRDD<String> filterData = lines.filter(line -> {
            String lang = TextPreProcessor.getTextLanguage(line);
            return lang.equals("en");
        });
        System.out.println("Done filtering the data");
        System.out.println("Lines:" + filterData.count());
        final AtomicInteger indexHolder = new AtomicInteger();

        JavaRDD<String> labelData = filterData.map(rawData -> {

            String nt = TextPreProcessor.normalizeText(rawData);
            //TagProcessor tp = new TagProcessor();
            // Get all the unique applicable tags
            List<Tag> tags = null;
            try {
                tags = TagProcessor.getUniqueTags(nt);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(SparkLabelData.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (tags.size() > 0) {
                // label data
                nt = lableData(nt, tags);

            }
            System.out.println("Item: " + (indexHolder.getAndIncrement() + 1) + " [Done]\n");
            return "<s>" + nt + "</s>";

        });
        labelData.coalesce(1, true).saveAsTextFile(outputLocation);
    }

    /**
     *
     * @param input
     * @param tags
     * @return
     */
    public static String lableData(String input, List<Tag> tags) {

        TagSorter sort = new TagSorter();
        System.out.println("\n*****After Sort\n");
        Tag[] sortedTags = sort.sortTags(tags);
        for (Tag tag : sortedTags) {
            System.out.println(tag.getTagText() + ", " + tag.getStartIndex() + ", " + tag.getEndIndex() + ", " + tag.getPriority() + ", " + tag.getType());
        }
        System.out.println("************************");
        for (int i = (sortedTags.length - 1); i >= 0; i--) {
            Tag tag = sortedTags[i];
            if (input.toLowerCase().contains(" " + tag.getTagText().toLowerCase() + " ")) {
                //input = input.replaceAll(tag.getTagText(), getTag(tag.getType(), tag.getTagText()));
                if (tag.getPriority() > 1) {
                    input = input.replaceAll("(?i)" + Pattern.quote(" " + tag.getTagText()), tag.getUid());
                } else {
                    input = input.replaceAll(Pattern.quote(" " + tag.getTagText()), tag.getUid());
                }
            }
        }

        for (Tag tag : sortedTags) {
            if (input.contains(tag.getUid())) {
                input = input.replaceAll(tag.getUid(), getTag(tag.getType(), tag.getTagText()));
            }
        }

        System.out.println(input);
        return input.trim().replaceAll("\\s+", " ").replaceAll(" ,",",").replaceAll(" \\.","\\.");
    }
    

    /**
     *
     * @param type
     * @param val
     * @return
     */
    private static String getTag(String type, String val) {
        return " <ENAMEX TYPE=\"" + type + "\">" + val + "</ENAMEX> ";
    }
}
