/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label.spark;

import com.config.AppConfig;
import java.io.IOException;
import java.util.UUID;
import org.apache.spark.launcher.SparkLauncher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author shahzad
 */
public class SparkJobLauncher {
    private final AppConfig config;
    private final String stage;
    public SparkJobLauncher(String stage) {
        this.stage = stage;
        this.config = AppConfig.createFromYamlSettings(this.stage);
       
    }
    
    public int prepareFileForTrainingFromFile(int startCursor) throws IOException, InterruptedException {
        
        UUID runID = UUID.randomUUID();
        
        Process spark = new SparkLauncher()
                .setAppName("LABEL_DATA")
                .setConf("spark.executor.instances", config.getDefaultNumberOfExecutors())
                .setConf(SparkLauncher.EXECUTOR_MEMORY, config.getDefaultExecutorMemory())
                .setConf(SparkLauncher.EXECUTOR_CORES, config.getDefaultExecutorCores())
                .setConf("spark.locality.wait", "0")
                .setAppResource(config.getAppJar())
                .addAppArgs(stage, Integer.toString(startCursor))
                .setMainClass(config.getLabelMainClass())
                .setMaster(config.getSparkMaster())
                .setSparkHome(config.getSparkHome())
                .setVerbose(config.isVerbose())
                .launch();

        StreamGobbler errorGobbler = new StreamGobbler(spark.getErrorStream(), "LABEL_ERROR", runID.toString(), config.getErrorLogDirectory());
        StreamGobbler outputGobbler = new StreamGobbler(spark.getInputStream(), "LABEL_OUTPUT", runID.toString(), config.getOutputLogDirectory());
        errorGobbler.start();
        outputGobbler.start();

        return spark.waitFor();
    }
    
    public static void main(String args[]) throws IOException, InterruptedException{
        SparkJobLauncher sp = new SparkJobLauncher("local");
        
        sp.prepareFileForTrainingFromFile(30000);
    }
}
