package com.ml.label.spark;

import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * 
 * @author shahzad
 */
public class StreamGobbler extends Thread {

    private Logger logger = Logger.getLogger(StreamGobbler.class);

    private InputStream is;
    private String type;
    private String runID;
    private String logLocation;

    public StreamGobbler(InputStream is, String type, String runID, String logLocation) {
        this.is = is;
        this.type = type;
        this.runID = runID;
        this.logLocation = logLocation;
        setDaemon(true);
    }

    public void run() {
        final Path destination = Paths.get(logLocation + File.separator + type + "_" + runID);
        try {
            Files.copy(is, destination);
        } catch (IOException e) {
            logger.error("Error capturing output from process");
            logger.error(e);
        }
    }
}
