/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import com.ml.training.TextPreProcessor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import static org.apache.commons.collections4.IterableUtils.forEach;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

/**
 *
 * @author shahzad
 */
public class LabelData {

    private final TagProcessor tp;

    public LabelData() {
        tp = new TagProcessor();
    }

    public void prepareFileForTrainingFromDB(int startCursor, int offset) throws Exception {
        String fileName = "TrainingData-" + startCursor + ".txt";
        List<String> rawDataList = CrawledProject.getCrawledProjectsText(startCursor, offset);
        List<String> labeledData = new ArrayList<>();
        for (String rawData : rawDataList) {

            String lang = TextPreProcessor.getTextLanguage(rawData);
            if (lang.equals("en")) {

                String nt = TextPreProcessor.normalizeText(rawData);

                // Get all the unique applicable tags
                List<Tag> tags = tp.getUniqueTags(nt);
                
                if(tags.size() > 0){
                    // label data
                    nt = lableData(nt, tags);
                    labeledData.add(nt);
                }

                System.out.println(" [Done]\n");

            }
        }
        // write labeled data to file
        DataWriter.writeLabelData(labeledData, fileName);
    }

    public void prepareFileForTrainingFromFile(int startCursor) throws Exception {
        String fileName = "RawData-" + startCursor + ".txt";
        List<String> rawDataList = DataWriter.readDataFromFile(fileName);
        List<String> labeledData = new ArrayList<>();
        List<String> strings = Arrays.asList("one", "two","three","four");
        //forEach(strings, (x, i, n) -> System.out.println("" + (i+1) + "/"+n+": " + x));
        final AtomicInteger indexHolder = new AtomicInteger();
        rawDataList.forEach(rawData -> {
            String lang = TextPreProcessor.getTextLanguage(rawData);
            if (lang.equals("en")) {

                String nt = null;
                try {
                    nt = TextPreProcessor.normalizeText(rawData);
                } catch (IOException | SAXException | TikaException ex) {
                    Logger.getLogger(LabelData.class.getName()).log(Level.SEVERE, null, ex);
                }

                // Get all the unique applicable tags
                List<Tag> tags = null;
                try {
                    tags = tp.getUniqueTags(nt);
                } catch (IOException ex) {
                    Logger.getLogger(LabelData.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(tags.size() > 0){
                    // label data
                    nt = lableData(nt, tags);
                    labeledData.add(nt);
                }

                System.out.println("Item: " + (indexHolder.getAndIncrement() + 1) + " [Done]\n");
            }
        });

//        for (String rawData : rawDataList) {
//            String lang = TextPreProcessor.getTextLanguage(rawData);
//            if (lang.equals("en")) {
//
//                String nt = TextPreProcessor.normalizeText(rawData);
//
//                // Get all the unique applicable tags
//                List<Tag> tags = tp.getUniqueTags(nt);
//
//                // label data
//                nt = lableData(nt, tags);
//                labeledData.add(nt);
//
//                System.out.println(" [Done]\n");
//            }
//
//        }
        // write labeled data to file
        DataWriter.writeLabelData(labeledData, "Training-" + startCursor + ".txt");
    }

    /**
     *
     * @param input
     * @param tags
     * @return
     */
    private String lableData(String input, List<Tag> tags) {

        TagSorter sort = new TagSorter();
        System.out.println("\n*****After Sort\n");
        Tag[] sortedTags = sort.sortTags(tags);
        for (Tag tag : sortedTags) {
            System.out.println(tag.getTagText() + ", " + tag.getStartIndex() + ", " + tag.getEndIndex() + ", " + tag.getPriority() + ", " + tag.getType());
        }
        System.out.println("************************");
        for (int i = (sortedTags.length - 1); i >= 0; i--) {
            Tag tag = sortedTags[i];
            if (input.toLowerCase().contains(" " + tag.getTagText().toLowerCase() + " ")) {
                //input = input.replaceAll(tag.getTagText(), getTag(tag.getType(), tag.getTagText()));
                if(tag.getPriority() >  1){
                    input = input.replaceAll("(?i)" + Pattern.quote(" " + tag.getTagText()), tag.getUid());
                }
                else{
                    input = input.replaceAll(Pattern.quote(" " + tag.getTagText()), tag.getUid());
                }
            }
        }

        for (Tag tag : sortedTags) {
            if (input.contains(tag.getUid())) {
                input = input.replaceAll(tag.getUid(), getTag(tag.getType(), tag.getTagText()));
            }
        }

        System.out.println(input);
        return input;
    }

    /**
     *
     * @param type
     * @param val
     * @return
     */
    private String getTag(String type, String val) {
        return " <ENAMEX TYPE=\"" + type + "\">" + val + "</ENAMEX> ";
    }
}
