/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import java.util.UUID;

/**
 *
 * @author shahzad
 */
public class Tag {
    
    private String tagText;
    
    private int startIndex;
    
    private int endIndex;
    
    private int priority;
    
    private String type;
    
    private String uid;
    
    public Tag(String text, int sIndex, int eIndex, int priority, String type){
        UUID uuid = UUID.randomUUID();
        uid = uuid.toString();
        tagText = text;
        startIndex = sIndex;
        endIndex = eIndex;
        this.priority = priority;
        this.type = type;
        
    }

    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    
    
}
