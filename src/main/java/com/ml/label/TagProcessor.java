/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.label;

import com.ML_Package.Muc6ChunkParser;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.ml.training.Dictionary;
import com.ml.training.Trainer;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author shahzad
 */
public class TagProcessor implements Serializable {
    
    static final String DEGREE_TAG = "DEGREE";
    static final String EDUCATION_TAG = "EDUCATION";
    static final String LOCATION_TAG = "LOCATION";
    static final String SKILL_TAG = "SKILL";
    static final String FUNCTIONAL_TITLE_TAG = "FUNCTIONTITLE";
    static final String KNOWLEDGE_TAG = "KNOWLEDGE";
    static final String ORGNIZATION_TAG = "ORGANIZATION";
    
    private final Dictionary dic;
    private final File dicFile;
    
    public TagProcessor(){
        dicFile = new File(Trainer.DIC_DATA_PATH);
        Parser<ObjectHandler<Chunking>> dicParser
                = new Muc6ChunkParser();

        dic = new Dictionary(dicParser);
    }
    
    /**
     *
     * @param input
     * @return
     * @throws IOException
     */
    public List<Tag> getUniqueTags(String input) throws IOException {
        StringBuilder sinput = new StringBuilder(input);

        List<Tag> tags = new ArrayList<>();
        getTagsOnType(sinput, DEGREE_TAG, 7, tags);
        getTagsOnType(sinput, FUNCTIONAL_TITLE_TAG, 6, tags);
        getTagsOnType(sinput, KNOWLEDGE_TAG, 5, tags);
        getTagsOnType(sinput, SKILL_TAG, 4, tags);
        getTagsOnType(sinput, ORGNIZATION_TAG, 3, tags);
        getTagsOnType(sinput, EDUCATION_TAG, 2, tags);
        getTagsOnType(sinput, LOCATION_TAG, 1, tags);
        
        if(tags.size() > 0){
            for (Tag tag : tags) {
                System.out.println(tag.getTagText() + ", " + tag.getStartIndex() + ", " + tag.getEndIndex() + ", " + tag.getPriority() + ", " + tag.getType());
            }
            System.out.println("\n*****After Filter\n");
            TagFilter.filterOverlappingTags(tags);
            for (Tag tag : tags) {
                System.out.println(tag.getTagText() + ", " + tag.getStartIndex() + ", " + tag.getEndIndex() + ", " + tag.getPriority() + ", " + tag.getType());
            }
            System.out.println("************************");
        }
        return tags;
    }

    /**
     *
     * @param input
     * @param type
     * @param priority
     * @param tags
     * @throws IOException
     */
    private void getTagsOnType(StringBuilder input, String type, int priority, List<Tag> tags) throws IOException {
        //System.out.println("Searching and replacing "+type);
        int sectionPriority = priority + 5;
        Set<String> ft = dic.getNGramSpecificTypeFromDic(dicFile, type, 5);
        for (String search : ft) {
            if (input.toString().toLowerCase().contains(" " + search.toLowerCase() + " ")) {
                int sIndex = input.toString().toLowerCase().indexOf(search.toLowerCase());
                tags.add(new Tag(search, sIndex, (search.length() + sIndex), sectionPriority, type));
            }
        }
        sectionPriority = priority + 4;
        ft = dic.getNGramSpecificTypeFromDic(dicFile, type, 4);
        for (String search : ft) {
            if (input.toString().toLowerCase().contains(" " + search.toLowerCase() + " ")) {
                int sIndex = input.toString().toLowerCase().indexOf(search.toLowerCase());
                tags.add(new Tag(search, sIndex, (search.length() + sIndex), sectionPriority, type));
            }
        }

        sectionPriority = priority + 3;
        ft = dic.getNGramSpecificTypeFromDic(dicFile, type, 3);
        for (String search : ft) {
            if (input.toString().toLowerCase().contains(" " + search.toLowerCase() + " ")) {
                int sIndex = input.toString().toLowerCase().indexOf(search.toLowerCase());
                tags.add(new Tag(search, sIndex, (search.length() + sIndex), sectionPriority, type));
            }
        }

        sectionPriority = priority + 2;
        ft = dic.getNGramSpecificTypeFromDic(dicFile, type, 2);
        for (String search : ft) {
            if (input.toString().toLowerCase().contains(" " + search.toLowerCase() + " ")) {
                int sIndex = input.toString().toLowerCase().indexOf(search.toLowerCase());
                tags.add(new Tag(search, sIndex, (search.length() + sIndex), sectionPriority, type));
            }
        }

        if (type.equals(DEGREE_TAG) 
                || type.equals(LOCATION_TAG) 
                || type.equals(ORGNIZATION_TAG) 
                || type.equals(EDUCATION_TAG)) 
        {
            sectionPriority = priority;
            ft = dic.getNGramSpecificTypeFromDic(dicFile, type, 1);
            for (String search : ft) {
                if (input.toString().toLowerCase().contains(" " + search.toLowerCase() + " ")) {
                    int sIndex = input.toString().toLowerCase().indexOf(" " + search.toLowerCase() + " ");
                    tags.add(new Tag(search, sIndex, (search.length() + sIndex), sectionPriority, type));
                }
            }
        }
        //return input;
    }
}
