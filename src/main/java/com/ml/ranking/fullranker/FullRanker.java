/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking.fullranker;

import com.ml.ranking.fullranker.ProjectRanker;
import com.dbConnection.Connect_Database;
import com.ml.ranking.Ranker;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author shahzad
 */
public class FullRanker implements Ranker{

    @Override
    public void rank() throws Exception{
        System.out.println("Starting the full run ranking");
        Connect_Database dao = new Connect_Database();
        dao.getDBConnection();

//        ResultSet expertsForExperts = dao.getExperts();
//        ResultSet projectsForExperts = dao.getProjectsWithId();

        ResultSet projectsForProjects = dao.getProjects();
        ResultSet expertsForProjects = dao.getExpertsWithId();

        //Save into Arrays
        java.util.List<Integer> listExpForExperts = new ArrayList<>();
        java.util.List<Integer> listProForExperts = new ArrayList<>();
//        while (expertsForExperts.next()) {
//            listExpForExperts.add(expertsForExperts.getInt("expert_Id"));
//        }
//        while (projectsForExperts.next()) {
//            listProForExperts.add(projectsForExperts.getInt("project_id"));
//            System.out.println("Project id = " + projectsForExperts.getInt("project_id"));
//        }
        java.util.List<Integer> listProForProjects = new ArrayList<>();
        java.util.List<Integer> listExpForProjects = new ArrayList<>();
        while (projectsForProjects.next()) {
            listProForProjects.add(projectsForProjects.getInt("project_id"));
        }
        while (expertsForProjects.next()) {
            listExpForProjects.add(expertsForProjects.getInt("expert_Id"));
            //System.out.println("Expert id = " + expertsForProjects.getInt("expert_Id"));
        }
        dao.close();
        System.out.println("Experts found:" + listExpForProjects.size());
        System.out.println("Projects found:" + listProForProjects.size());

        ProjectRankerStream pr = new ProjectRankerStream();
        pr.calculateMatchingProjectsForExpert(listExpForProjects, listProForProjects);
        //Main.calculateMatchingexpertsForProject(listExpForExperts, listProForExperts);
    }
}
