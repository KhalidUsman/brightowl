/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking.fullranker;

import com.Keyword_Extraction.Rake;
import com.ML_Package.NER_Testing;
import com.dbConnection.Connect_Database;
import com.dbConnection.Expert;
import com.dbConnection.Project;
import com.esotericsoftware.kryo.Kryo;
import com.ml.model.NERModel;
import com.ml.ranking.BOVector;
import com.ml.ranking.BOVectorPair;
import com.ml.ranking.CosineSimilarity;
import com.ml.ranking.Distance;
import static com.ml.ranking.Main.PROFILE;
import static com.ml.ranking.Main.VERBOSE_DEBUG;
import com.ml.ranking.TextToVector;
import static com.ml.ranking.fullranker.ProjectRanker.SKILL_WEIGHT;
import static com.ml.ranking.fullranker.ProjectRanker.deepClone;
import com.ml.ranking.util.JoinArray;
import com.ml.training.TextPreProcessor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

/**
 *
 * @author shahzad
 */
public class ProjectRankerStream {

    static final float SKILL_WEIGHT = 1.0f;
    static final float DEGREEL_WEIGHT = 1.0f;
    static final float EDUCATION_WEIGHT = 1.0f;
    static final float LOCATION_WEIGHT = 1.0f;
    static final float FUNCTIONTITLE_WEIGHT = 1.0f;
    static final float KNOWLEDGE_WEIGHT = 1.0f;
    static final float ORGANIZATION_WEIGHT = 1.0f;
    static final float LANGAUGE_WEIGHT = 1.0f;
    static final float PERSONALITY_WEIGHT = 1.0f;
    private final ReentrantLock lock = new ReentrantLock();

    public void calculateMatchingProjectsForExpert(List<Integer> experts, List<Integer> projects) throws Exception {
        Connect_Database saveRec = new Connect_Database();
        saveRec.getDBConnection();
        if (!VERBOSE_DEBUG) {
            saveRec.createAndTruncateMlTable();
        }
        ArrayList<String> matchingDicAll = new ArrayList<>();

        try {
            NERModel testObj = NERModel.getInstance(PROFILE);

            HashMap<String, Map<String, String>> cachePerdictions = new HashMap<>();
            TextToVector ttv = new TextToVector();
            CosineSimilarity cosSim = new CosineSimilarity();

            for (int i = 0; i < experts.size(); i++) {
                ArrayList<Map<String, Double>> matchingArr = new ArrayList<>();
                System.out.println("***********************************************************");
                System.out.println("Processed: " + i + "/" + experts.size());
                System.out.println("Processing expertid: " + experts.get(i));
                Expert expertObj = new Expert(experts.get(i));

                String expertPersonalities[] = expertObj.getExpertPersonalities();
                String expertKnowledge[] = expertObj.getExpertKnowledge();
                String expertExpertise[] = expertObj.getExpertExpertise();
                String expertEducation[] = expertObj.getExpertEducation();
                String expertLanguages[] = expertObj.getExpertLanguages();
                String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
                String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
                ArrayList<String> summaryCat = new ArrayList<>();
                //System.out.println("Processed basic expert details ");

                String expertSummary = expertObj.getExpertSummary();
//                if (expertSummary.length() > 0) {
//                    Map<String, String> summaryPredictions = testObj.predictResults(expertSummary);
//
//                    for (Map.Entry<String, String> entry : summaryPredictions.entrySet()) {
//                        summaryCat.add(entry.getKey());
//                    }
//                }

                String[] combineFuncTitles = (String[]) ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
                String expertLocation[] = expertObj.getExpertLocation();
                String expertCoordinates[] = expertObj.getExpertCoordinates();

                String[] combineExpert = JoinArray.joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation, expertSummary.split(" "));
                String combineExpertStr = expertSummary + " " + Arrays.toString(combineExpert).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");

                if (VERBOSE_DEBUG) {
                    //System.out.println(combineExpertStr);
                }
                expertObj.close();

                HashMap<String, BOVectorPair> word_freq_vector = new HashMap<>();
                ArrayList<String> Distinct_words_text_1_2 = new ArrayList<>();
                //System.out.println("Started expert vectors ");
                HashMap<String, HashMap<String,String>> Distinct_words_subsets_catType = new HashMap<>();
                BOVector vecExpSkill = ttv.evaluateWithWeight(expertExpertise, word_freq_vector, Distinct_words_text_1_2, SKILL_WEIGHT, 1, "SKILL", Distinct_words_subsets_catType);
                BOVector vecExpFunc = ttv.evaluateWithWeight(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, FUNCTIONTITLE_WEIGHT, 1, "FUNCTION_TITLE", vecExpSkill.Distinct_words_subset_cattype);
                BOVector vecExpLoc = ttv.evaluateWithWeight(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, LOCATION_WEIGHT, 1, "LOCATION", vecExpFunc.Distinct_words_subset_cattype);
                BOVector vecExpEdu = ttv.evaluateWithWeight(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, EDUCATION_WEIGHT, 1, "EDUCATION", vecExpLoc.Distinct_words_subset_cattype);
                BOVector vecExpLang = ttv.evaluateWithWeight(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, LANGAUGE_WEIGHT, 1, "LANGUAGE", vecExpEdu.Distinct_words_subset_cattype);
                BOVector vecExpPer = ttv.evaluateWithWeight(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, PERSONALITY_WEIGHT, 1, "PERSONALITY", vecExpLang.Distinct_words_subset_cattype);

                BOVector vecExpSummary = ttv.evaluateWithWeight(summaryCat.toArray(new String[summaryCat.size()]), vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 1, "SUMMARY", vecExpPer.Distinct_words_subset_cattype);
                BOVector vecExpKnow = ttv.evaluateWithWeight(expertKnowledge, vecExpSummary.word_freq_vector, vecExpSummary.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 1, "KNOWLEDGE", vecExpSummary.Distinct_words_subset_cattype);

                //System.out.println("expert vectors done");
                if (VERBOSE_DEBUG) {
                    System.out.println("******** Expert Details");
                    System.out.println(vecExpKnow.toString());
                }
//                for (int j = 0; j < projects.size(); j++) {
//
//                }

                // Calculate the preditions from model in parallel
//                 projects.parallelStream()
//                        .map(projectId -> {
//                            Map<String, String> categories = new HashMap<>();
//                            categories.put(projectId.toString(), "One");
//                            return categories;
//                        }).collect(Collectors.toMap((entry) -> entry.getKey(), (entry) -> entry.getValue()));
//                Map<Integer, Map<Integer, Map<String, String>>> abc = projects.parallelStream()
//                        //.filter(projectId -> projectId > 0)
//                        .collect(Collectors.groupingBy(Function.identity(), Collectors.toMap(projectId -> projectId,
//                                projectId -> {
//
//                                    Map<String, String> categories = new HashMap<>();
//                                    categories.put("two", "One");
//                                    return categories;
//                                })));
//                for (Entry<Integer, Map<Integer, Map<String, String>>> e : abc.entrySet()) {
//                    for (Entry<Integer, Map<String, String>> e1 : e.getValue().entrySet()) {
//                        System.out.println("ProjectId " + e1.getKey());
//                        for (Entry<String, String> e2 : e1.getValue().entrySet()) {
//                            System.out.println("Key " + e2.getKey() + " , Val + " + e2.getValue());
//                        }
//                    }
//                }
                final AtomicInteger count = new AtomicInteger();
                if (cachePerdictions.isEmpty()) {
                    System.out.println("Getting the categories from Model");
                    projects.parallelStream()
                            .forEach(projectId -> {
                                Project projectObj = null;
                                try {
                                    projectObj = new Project(projectId);

                                    if (projectObj._crawlId == 1) {
                                        try {
                                            String crawlString = TextPreProcessor.normalizeText(projectObj._crawlText);
                                            String lang = TextPreProcessor.getTextLanguage(crawlString);
                                            Map<String, String> categories;
                                            if (lang.equals("en")) {
                                                if (VERBOSE_DEBUG) {
                                                    System.out.println("******** Project Text:");
                                                    System.out.println(crawlString);
                                                }
                                                categories = testObj.predictResults(WordUtils.capitalize(crawlString));
                                                cachePerdictions.put(Integer.toString(projectObj._projectId), categories);
                                            }
                                        } catch (IOException ex) {
                                            Logger.getLogger(ProjectRankerStream.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (SAXException ex) {
                                            Logger.getLogger(ProjectRankerStream.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (TikaException ex) {
                                            Logger.getLogger(ProjectRankerStream.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    System.out.println("Processed by Model:" + count.incrementAndGet() + "/" + projects.size());
                                } catch (Exception ex) {
                                    System.out.println(ex.getMessage());
                                    ex.printStackTrace();
                                    
                                } finally {
                                    if (projectObj != null) {
                                        projectObj.close();
                                    }
                                }
                            });
                }
                saveRec.close();
                count.set(0);
                System.out.println("Calculating the scores from the categories from model");
                cachePerdictions.entrySet().parallelStream()
                        .forEach(predictionEntry -> {

                            System.out.println("Processing next");
                            BOVector vecExpKnowClone = (BOVector) deepClone(vecExpKnow);
                            //System.out.println("Got clone");
                            Map<String, String> categories = predictionEntry.getValue();

                            Project projectObj = null;
                            try {
                                projectObj = new Project(Integer.parseInt(predictionEntry.getKey()));
                                String crawlString = TextPreProcessor.normalizeText(projectObj._crawlText);

                                List<String> crawlSkills = new ArrayList<>();
                                List<String> crawlFunctionTitle = new ArrayList<>();
                                List<String> crawlLocation = new ArrayList<>();
                                List<String> crawlEducation = new ArrayList<>();
                                List<String> crawlLanguage = new ArrayList<>();
                                List<String> crawlPersonality = new ArrayList<>();
                                List<String> crawlKnowledge = new ArrayList<>();

                                for (Map.Entry<String, String> entry : categories.entrySet()) {
                                    if (entry.getValue().equals("SKILL") == true) {
                                        crawlSkills.add(entry.getKey());
                                    } else if (entry.getValue().equals("FUNCTIONTITLE") == true) {
                                        crawlFunctionTitle.add(entry.getKey());
                                    } else if (entry.getValue().equals("LOCATION") == true) {
                                        crawlLocation.add(entry.getKey());
                                    } else if ((entry.getValue().equals("EDUCATION")) == true || (entry.getValue().equals("DEGREE") == true)) {
                                        crawlEducation.add(entry.getKey());
                                    } else if (entry.getValue().equals("LANGUAGE") == true) {
                                        crawlLanguage.add(entry.getKey());
                                    } else if (entry.getValue().equals("PERSONALITY") == true) {
                                        crawlPersonality.add(entry.getKey());
                                    } else if (entry.getValue().equals("KNOWLEDGE") == true) {
                                        crawlKnowledge.add(entry.getKey());
                                    } else {
                                        //System.out.println("Useless Category");
                                    }
                                }
                                //System.out.println("Prepated the project vector");
                                crawlLocation.addAll(Arrays.asList(projectObj.getProjectLocation()));
                                if (VERBOSE_DEBUG) {
                                    System.out.println("Locations found: " + crawlLocation.size());
                                }

                                String crawlSkillArr[] = crawlSkills.toArray(new String[crawlSkills.size()]);
                                String crawlFunctionTitleArr[] = crawlFunctionTitle.toArray(new String[crawlFunctionTitle.size()]);
                                String crawlLocationArr[] = crawlLocation.toArray(new String[crawlLocation.size()]);
                                String crawlEducationArr[] = crawlEducation.toArray(new String[crawlEducation.size()]);
                                String crawlLanguageArr[] = crawlLanguage.toArray(new String[crawlLanguage.size()]);
                                String crawlPersonalityArr[] = crawlPersonality.toArray(new String[crawlPersonality.size()]);
                                String crawlKnowledgeArr[] = crawlKnowledge.toArray(new String[crawlKnowledge.size()]);

                                //System.out.println("Started the project vector calc");
                                
                                BOVector vecProSkill = ttv.evaluateWithWeightNGram(crawlSkillArr, vecExpKnowClone.word_freq_vector, vecExpKnowClone.Distinct_words_text_1_2, SKILL_WEIGHT, 0, "SKILL", vecExpKnowClone.Distinct_words_subset_cattype);
                                BOVector vecProFunc = ttv.evaluateWithWeightNGram(crawlFunctionTitleArr, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, FUNCTIONTITLE_WEIGHT, 0, "FUNCTION_TITLE", vecExpKnowClone.Distinct_words_subset_cattype);
                                BOVector vecProLoc = ttv.evaluateWithWeightNGram(crawlLocationArr, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, LOCATION_WEIGHT, 0, "LOCATION", vecExpKnowClone.Distinct_words_subset_cattype);
                                BOVector vecProEdu = ttv.evaluateWithWeightNGram(crawlEducationArr, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, EDUCATION_WEIGHT, 0, "EDUCATION", vecExpKnowClone.Distinct_words_subset_cattype);
                                BOVector vecProLang = ttv.evaluateWithWeightNGram(crawlLanguageArr, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, LANGAUGE_WEIGHT, 0, "LANGUAGE",vecExpKnowClone.Distinct_words_subset_cattype);
                                BOVector vecProPer = ttv.evaluateWithWeightNGram(crawlPersonalityArr, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, PERSONALITY_WEIGHT, 0, "PERSONALITY", vecExpKnowClone.Distinct_words_subset_cattype);
                                BOVector vecProKnow = ttv.evaluateWithWeightNGram(crawlKnowledgeArr, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 0, "KNOWLEDGE", vecExpKnowClone.Distinct_words_subset_cattype);
                                //System.out.println("Done the project vector calc");
                                //double cosine_Score = cosSim.getScoreOnBOVectorPerCategory(vecProKnow);
                                Map<String,Double> cosine_Score = cosSim.getScoreOnBOVectorPerCategoryExplicitly(vecProKnow);
                                //System.out.println("Got the cosine");
                                //System.out.println(cosine_Score);
                                //if(cosine_Score >= 0.5){

                                //String crawlFeaturesStr = Arrays.toString(crawlFeatures).replace("amp;", "").replace("[", "").replace("]", "");
                                String crawlFeaturesStr = crawlString.replace("amp;", "").replace("[", "").replace("]", "");
                                double featureScore = cosSim.getScoreOnText(combineExpertStr, crawlFeaturesStr);
                                //double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
                                double avgScore = 0.8 * (cosine_Score.get("total")) + 0.2 * (featureScore);
                                //System.out.println("Got the overall score");

                                if (!Double.isNaN(avgScore)) {
                                    final HashMap<String, Double> matchingDic = new HashMap<>();
                                    matchingDic.put("expert_id", (double) expertObj._expertId);
                                    matchingDic.put("project_id", (double) Double.parseDouble(predictionEntry.getKey()));
                                    matchingDic.put("ml_score", avgScore);
                                    matchingDic.put("ml_score_cosine", cosine_Score.get("total"));
                                    matchingDic.put("ml_score_cosine_text", featureScore);
                                    matchingDic.put("ml_score_distance", 0.0);
                                    
                                    
                                    matchingDic.put("ml_score_skill", cosine_Score.containsKey("skill") ? cosine_Score.get("skill") : 0.0);
                                    matchingDic.put("ml_score_function_title", cosine_Score.containsKey("function_title") ? cosine_Score.get("function_title") : 0.0);
                                    matchingDic.put("ml_score_location", cosine_Score.containsKey("location") ? cosine_Score.get("location") : 0.0);
                                    matchingDic.put("ml_score_education", cosine_Score.containsKey("education") ? cosine_Score.get("education") : 0.0);
                                    matchingDic.put("ml_score_language", cosine_Score.containsKey("language") ? cosine_Score.get("language") : 0.0);
                                    matchingDic.put("ml_score_personality", cosine_Score.containsKey("personality") ? cosine_Score.get("personality") : 0.0);
                                    matchingDic.put("ml_score_knowledge", cosine_Score.containsKey("knowledge") ? cosine_Score.get("knowledge") : 0.0);
                                    matchingDic.put("ml_factor", cosine_Score.containsKey("ml_factor") ? cosine_Score.get("ml_factor") : 0.0);
                                    
//                                    if (cosine_Score >= 0.3 || featureScore >= 0.3) {
//                                        matchingDicAll.add("ExpertId = " + expertObj._expertId + ", Projectid = " + predictionEntry.getKey() + ", Avg Score = " + round(avgScore, 4) + ", Cosine Score = " + round(cosine_Score, 4) + ", Features Score = " + round(featureScore, 4));
//                                    }
                                    if (!VERBOSE_DEBUG) {
                                        boolean isLockAcquired = lock.tryLock(10, TimeUnit.SECONDS);
                                        if (isLockAcquired) {
                                            try {
                                                //Critical section here
                                                Connect_Database saveRecInt = null;
                                                try {
                                                    saveRecInt = new Connect_Database();

                                                    saveRecInt.InsertOrUpdateRecordSingleTableContinous(matchingDic);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                } finally {
                                                    if (saveRecInt != null) {
                                                        saveRecInt.close();
                                                    }
                                                }

                                            } finally {
                                                lock.unlock();
                                            }
                                        } else {
                                            matchingArr.add(matchingDic);
                                        }
                                    }
                                    //
                                }
                                System.out.println("ExpertId = " + expertObj._expertId + ",Projectid = " + predictionEntry.getKey() + ", Avg Score = " + round(avgScore, 4) + ", Cosine Score = " + round(cosine_Score.get("total"), 4) + ", Features Score = " + round(featureScore, 4));
                                System.out.println("Processed :" + count.incrementAndGet() + "/" + cachePerdictions.size());
                            } catch (Exception ex) {
                                System.out.println(ex.getMessage());
                                ex.printStackTrace();
                                //Logger.getLogger(ProjectRankerStream.class.getName()).log(Level.SEVERE, null, ex);
                            } finally {
                                if (projectObj != null) {
                                    projectObj.close();
                                }
                            }

                        });

                // Calculate the score in parallel
                if (!VERBOSE_DEBUG) {
                    Connect_Database saveRecInt = null;
                    try {
                        saveRecInt = new Connect_Database();

                        saveRecInt.insertRemaining();
                        if (matchingArr.size() > 0) {
                            saveRecInt.InsertOrUpdateRecord(matchingArr);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (saveRecInt != null) {
                            saveRecInt.close();
                        }
                    }

                }
            }
            // Check if Table exists
            //String sql = "INSERT OR REPLACE INTO expert_project_complete (expert_id, project_id, match_ml, ml_score_kh) " + "VALUES (?, ?, ?, ?)";

//            for (int k = 0; k < experts.size(); k++) {
//                String sql = "UPDATE expert SET k_ml_update = 0 WHERE expert_id = " + experts.get(k);
//                try {
//                    saveRec.updateRecords(sql);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            saveRec.close();
        }
        //System.out.println("**************************** Final Score *********************");xzhn,,kuytdst3erdsz
        
              
//        for (String result : matchingDicAll) {
//            System.out.println(result);
//        }
    }

//    public static HashMap<String, Map<String, String>> calculateFromModel(int projectId) throws Exception {
//        Project projectObj = new Project(projectId);
//        if (projectObj._crawlId == 1) {
//            String crawlString = TextPreProcessor.normalizeText(projectObj._crawlText);
//            String lang = TextPreProcessor.getTextLanguage(crawlString);
//            Map<String, String> categories;
//            if (lang.equals("en")) {
//                if (VERBOSE_DEBUG) {
//                    System.out.println("******** Project Text:");
//                    System.out.println(crawlString);
//                }
//                categories = testObj.predictResults(WordUtils.capitalize(crawlString));
//            }
//        }
//
//        return null;
//    }
//    private static void getPrediction() {
//        Project projectObj = new Project(projects.get(j));
//        if (projectObj._crawlId == 0) {
//
////                        BOVector vecExpKnowClone = (BOVector) deepClone(vecExpKnow);
////                        //ReturnVector vecExpKnowClone = (ReturnVector) vecExpKnow;
////
////                        String projectPersonlities[] = projectObj.getProjectPersonalities();
////                        String projectKnowledge[] = projectObj.getProjectKnowledge();
////                        String projectExperties[] = projectObj.getProjectExpertise();
////                        String projectEducation[] = projectObj.getProjectEducation();
////                        String projectLanguages[] = projectObj.getProjectLanguages();
////                        String projectFuncTitles[] = projectObj.getProjectFunctionalTitle();
////                        String projectLocation[] = projectObj.getProjectLocation();
////                        String projectCoordinates[] = projectObj.getProjectCoordinates();
////
////                        String[] combineProject = JoinArray.joinArrayGeneric(projectPersonlities, projectKnowledge, projectExperties, projectEducation, projectLanguages, projectFuncTitles, projectLocation);
////                        String combineProjectStr = Arrays.toString(combineProject).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");
////
////                        BOVector vecProSkill = ttv.evaluateWithWeight(projectExperties, vecExpKnowClone.word_freq_vector, vecExpKnowClone.Distinct_words_text_1_2, 0.1, 0, "SKILL");
////                        BOVector vecProFunc = ttv.evaluateWithWeight(projectFuncTitles, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0, "FUNCTION_TITLE");
////                        BOVector vecProLoc = ttv.evaluateWithWeight(projectLocation, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0, "LOCATION");
////                        BOVector vecProEdu = ttv.evaluateWithWeight(projectEducation, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0, "EDUCATION");
////                        BOVector vecProLang = ttv.evaluateWithWeight(projectLanguages, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0, "LANGUAGE");
////                        BOVector vecProPer = ttv.evaluateWithWeight(projectPersonlities, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0, "PERSONALITY");
////                        BOVector vecProKnow = ttv.evaluateWithWeight(projectKnowledge, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0, "KNOWLDGE");
////
////                        double featureScore = cosSim.getScoreOnText(combineExpertStr, combineProjectStr);
////                        double avgDistance = Distance.calculate(expertCoordinates, projectCoordinates);
////                        double cosine_Score = cosSim.getScoreOnBOVector(vecProKnow);
////                        double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
////
////                        final HashMap<String, Double> matchingDic = new HashMap<>();
////                        matchingDic.put("expert_id", (double) expertObj._expertId);
////                        matchingDic.put("project_id", (double) projectObj._projectId);
////                        matchingDic.put("ml_score", avgScore);
////                        matchingArr.add(matchingDic);
////                        System.out.println("Expert = " + experts.get(i) + " Project = " + projects.get(j) + " Cosine = " + featureScore + " Category Cosine = " + cosine_Score + " Avg Score = " + avgScore);
//        } else {
//            BOVector vecExpKnowClone = (BOVector) deepClone(vecExpKnow);
//            String crawlCoordinates[] = projectObj.getProjectCoordinates();
//            double avgDistance = Distance.calculate(expertCoordinates, crawlCoordinates);
//            //System.out.println("Before Text: "+ projectObj._crawlText);
//            String crawlString = TextPreProcessor.normalizeText(projectObj._crawlText);
//            String lang = TextPreProcessor.getTextLanguage(crawlString);
//            if (lang.equals("en")) {
//                if (VERBOSE_DEBUG) {
//                    System.out.println("******** Project Text:");
//                    System.out.println(crawlString);
//                }
//                //                    Rake rakeObj = new Rake();
//                //                    String crawlFeatures[] = rakeObj.run(crawlString);
//
//                // LingPipe Categories
//                //NER_Testing testObj = new NER_Testing();
//                final Map<String, String> categories;
//                if (!cachePerdictions.containsKey(Integer.toString(projectObj._projectId))) {
//                    System.out.println("************ Calculating from Model:");
//
//                    categories = testObj.predictResults(WordUtils.capitalize(crawlString));
//                    //categories = testObj.predictResults(crawlString);
//                    cachePerdictions.put(Integer.toString(projectObj._projectId), categories);
//                } else {
//                    System.out.println("************* From Cache");
//                    categories = cachePerdictions.get(Integer.toString(projectObj._projectId));
//                }
//
//                List<String> crawlSkills = new ArrayList<String>();
//                List<String> crawlFunctionTitle = new ArrayList<String>();
//                List<String> crawlLocation = new ArrayList<String>();
//                List<String> crawlEducation = new ArrayList<String>();
//                List<String> crawlLanguage = new ArrayList<String>();
//                List<String> crawlPersonality = new ArrayList<String>();
//                List<String> crawlKnowledge = new ArrayList<String>();
//
//                for (Map.Entry<String, String> entry : categories.entrySet()) {
//                    if (entry.getValue().equals("SKILL") == true) {
//                        crawlSkills.add(entry.getKey());
//                    } else if (entry.getValue().equals("FUNCTIONTITLE") == true) {
//                        crawlFunctionTitle.add(entry.getKey());
//                    } else if (entry.getValue().equals("LOCATION") == true) {
//                        crawlLocation.add(entry.getKey());
//                    } else if ((entry.getValue().equals("EDUCATION")) == true || (entry.getValue().equals("DEGREE") == true)) {
//                        crawlEducation.add(entry.getKey());
//                    } else if (entry.getValue().equals("LANGUAGE") == true) {
//                        crawlLanguage.add(entry.getKey());
//                    } else if (entry.getValue().equals("PERSONALITY") == true) {
//                        crawlPersonality.add(entry.getKey());
//                    } else if (entry.getValue().equals("KNOWLEDGE") == true) {
//                        crawlKnowledge.add(entry.getKey());
//                    } else {
//                        //System.out.println("Useless Category");
//                    }
//                }
//
//                for (String projectLocation : projectObj.getProjectLocation()) {
//                    crawlLocation.add(projectLocation);
//                }
//
//                String crawlSkillArr[] = crawlSkills.toArray(new String[crawlSkills.size()]);
//                String crawlFunctionTitleArr[] = crawlFunctionTitle.toArray(new String[crawlFunctionTitle.size()]);
//                String crawlLocationArr[] = crawlLocation.toArray(new String[crawlLocation.size()]);
//                String crawlEducationArr[] = crawlEducation.toArray(new String[crawlEducation.size()]);
//                String crawlLanguageArr[] = crawlLanguage.toArray(new String[crawlLanguage.size()]);
//                String crawlPersonalityArr[] = crawlPersonality.toArray(new String[crawlPersonality.size()]);
//                String crawlKnowledgeArr[] = crawlKnowledge.toArray(new String[crawlKnowledge.size()]);
//
//                BOVector vecProSkill = ttv.evaluateWithWeightNGram(crawlSkillArr, vecExpKnowClone.word_freq_vector, vecExpKnowClone.Distinct_words_text_1_2, SKILL_WEIGHT, 0, "SKILL");
//                BOVector vecProFunc = ttv.evaluateWithWeightNGram(crawlFunctionTitleArr, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, FUNCTIONTITLE_WEIGHT, 0, "FUNCTION_TITLE");
//                BOVector vecProLoc = ttv.evaluateWithWeightNGram(crawlLocationArr, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, LOCATION_WEIGHT, 0, "LOCATION");
//                BOVector vecProEdu = ttv.evaluateWithWeightNGram(crawlEducationArr, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, EDUCATION_WEIGHT, 0, "EDUCATION");
//                BOVector vecProLang = ttv.evaluateWithWeightNGram(crawlLanguageArr, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, LANGAUGE_WEIGHT, 0, "LANGUAGE");
//                BOVector vecProPer = ttv.evaluateWithWeightNGram(crawlPersonalityArr, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, PERSONALITY_WEIGHT, 0, "PERSONALITY");
//                BOVector vecProKnow = ttv.evaluateWithWeightNGram(crawlKnowledgeArr, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 0, "KNOWLEDGE");
//
//                double cosine_Score = cosSim.getScoreOnBOVectorPerCategory(vecProKnow);
//                //System.out.println(cosine_Score);
//                //if(cosine_Score >= 0.5){
//
//                //String crawlFeaturesStr = Arrays.toString(crawlFeatures).replace("amp;", "").replace("[", "").replace("]", "");
//                String crawlFeaturesStr = crawlString.replace("amp;", "").replace("[", "").replace("]", "");
//                double featureScore = cosSim.getScoreOnText(combineExpertStr, crawlFeaturesStr);
//                //double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
//                double avgScore = 0.8 * (cosine_Score) + 0.2 * (featureScore);
//
//                if (!Double.isNaN(avgScore)) {
//                    final HashMap<String, Double> matchingDic = new HashMap<String, Double>();
//                    matchingDic.put("expert_id", (double) expertObj._expertId);
//                    matchingDic.put("project_id", (double) projectObj._projectId);
//                    matchingDic.put("ml_score", avgScore);
//                    matchingDic.put("ml_score_cosine", cosine_Score);
//                    matchingDic.put("ml_score_cosine_text", featureScore);
//                    matchingDic.put("ml_score_distance", avgDistance);
//                    if (cosine_Score >= 0.3 || featureScore >= 0.3) {
//                        matchingDicAll.add("ExpertId = " + expertObj._expertId + ", Projectid = " + projectObj._projectId + ", Avg Score = " + round(avgScore, 4) + ", Cosine Score = " + round(cosine_Score, 4) + ", Features Score = " + round(featureScore, 4));
//                    }
//                    matchingArr.add(matchingDic);
//                }
//                System.out.println("ExpertId = " + expertObj._expertId + ",Projectid = " + projectObj._projectId + ", Avg Score = " + round(avgScore, 4) + ", Cosine Score = " + round(cosine_Score, 4) + ", Features Score = " + round(featureScore, 4));
//                //}
//                // else{
//                //System.out.println("Ignoring!! Projectid =" + projectObj._projectId + ", Cosine Score = " + round(cosine_Score, 4));
//                //}
//            }
//        }
//        projectObj.close();
//    }
    public static Object deepClone(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            String s1 = e.toString();
            System.out.println("Not able to clone");
            return null;
        }
    }

    public static double round(double value, int places) {
        if (value > 0) {
            if (places < 0) {
                throw new IllegalArgumentException();
            }

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
        return value;
    }

}
