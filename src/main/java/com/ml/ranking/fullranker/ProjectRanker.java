/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking.fullranker;

import com.Keyword_Extraction.Rake;
import com.ML_Package.NER_Testing;
import com.dbConnection.Connect_Database;
import com.dbConnection.Expert;
import com.dbConnection.Project;
import com.esotericsoftware.kryo.Kryo;
import com.ml.model.NERModel;
import com.ml.ranking.BOVector;
import com.ml.ranking.BOVectorPair;
import com.ml.ranking.CosineSimilarity;
import com.ml.ranking.Distance;
import static com.ml.ranking.Main.PROFILE;
import com.ml.ranking.TextToVector;
import static com.ml.ranking.Main.VERBOSE_DEBUG;
import com.ml.ranking.util.JoinArray;
import com.ml.training.TextPreProcessor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author shahzad
 */
public class ProjectRanker {

    static final float SKILL_WEIGHT = 1.0f;
    static final float DEGREEL_WEIGHT = 1.0f;
    static final float EDUCATION_WEIGHT = 1.0f;
    static final float LOCATION_WEIGHT = 1.0f;
    static final float FUNCTIONTITLE_WEIGHT = 1.0f;
    static final float KNOWLEDGE_WEIGHT = 1.0f;
    static final float ORGANIZATION_WEIGHT = 1.0f;
    static final float LANGAUGE_WEIGHT = 1.0f;
    static final float PERSONALITY_WEIGHT = 1.0f;

    public void calculateMatchingProjectsForExpert(List<Integer> experts, List<Integer> projects) throws Exception {
        Connect_Database saveRec = new Connect_Database();
        saveRec.getDBConnection();
        if (!VERBOSE_DEBUG) {
            saveRec.createAndTruncateMlTable();
        }
        ArrayList<String> matchingDicAll = new ArrayList<>();
        try {
            NERModel testObj = NERModel.getInstance(PROFILE);

            HashMap<String, Map<String, String>> cachePerdictions = new HashMap<>();
            TextToVector ttv = new TextToVector();
            CosineSimilarity cosSim = new CosineSimilarity();

            for (int i = 0; i < experts.size(); i++) {
                ArrayList<Map<String, Double>> matchingArr = new ArrayList<>();
                System.out.println("***********************************************************");
                System.out.println("Processed: " + i + "/" + experts.size());
                System.out.println("Processing expertid: " + experts.get(i));
                Expert expertObj = new Expert(experts.get(i));

                String expertPersonalities[] = expertObj.getExpertPersonalities();
                String expertKnowledge[] = expertObj.getExpertKnowledge();
                String expertExpertise[] = expertObj.getExpertExpertise();
                String expertEducation[] = expertObj.getExpertEducation();
                String expertLanguages[] = expertObj.getExpertLanguages();
                String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
                String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
                ArrayList<String> summaryCat = new ArrayList<>();

                String expertSummary = expertObj.getExpertSummary();
//                if (expertSummary.length() > 0) {
//                    Map<String, String> summaryPredictions = testObj.predictResults(expertSummary);
//
//                    for (Map.Entry<String, String> entry : summaryPredictions.entrySet()) {
//                        summaryCat.add(entry.getKey());
//                    }
//                }

                String[] combineFuncTitles = (String[]) ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
                String expertLocation[] = expertObj.getExpertLocation();
                String expertCoordinates[] = expertObj.getExpertCoordinates();

                String[] combineExpert = JoinArray.joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation, expertSummary.split(" "));
                String combineExpertStr = expertSummary + " " + Arrays.toString(combineExpert).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");

                if (VERBOSE_DEBUG) {
                    //System.out.println(combineExpertStr);
                }
                expertObj.close();

                HashMap<String, BOVectorPair> word_freq_vector = new HashMap<>();
                ArrayList<String> Distinct_words_text_1_2 = new ArrayList<>();

                HashMap<String, HashMap<String, String>> Distinct_words_subsets_catType = new HashMap<>();
                BOVector vecExpSkill = ttv.evaluateWithWeight(expertExpertise, word_freq_vector, Distinct_words_text_1_2, SKILL_WEIGHT, 1, "SKILL", Distinct_words_subsets_catType);
                BOVector vecExpFunc = ttv.evaluateWithWeight(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, FUNCTIONTITLE_WEIGHT, 1, "FUNCTION_TITLE", vecExpSkill.Distinct_words_subset_cattype);
                BOVector vecExpLoc = ttv.evaluateWithWeight(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, LOCATION_WEIGHT, 1, "LOCATION", vecExpFunc.Distinct_words_subset_cattype);
                BOVector vecExpEdu = ttv.evaluateWithWeight(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, EDUCATION_WEIGHT, 1, "EDUCATION", vecExpLoc.Distinct_words_subset_cattype);
                BOVector vecExpLang = ttv.evaluateWithWeight(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, LANGAUGE_WEIGHT, 1, "LANGUAGE", vecExpEdu.Distinct_words_subset_cattype);
                BOVector vecExpPer = ttv.evaluateWithWeight(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, PERSONALITY_WEIGHT, 1, "PERSONALITY", vecExpLang.Distinct_words_subset_cattype);

                BOVector vecExpSummary = ttv.evaluateWithWeight(summaryCat.toArray(new String[summaryCat.size()]), vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 1, "SUMMARY", vecExpPer.Distinct_words_subset_cattype);
                BOVector vecExpKnow = ttv.evaluateWithWeight(expertKnowledge, vecExpSummary.word_freq_vector, vecExpSummary.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 1, "KNOWLEDGE", vecExpSummary.Distinct_words_subset_cattype);

                if (VERBOSE_DEBUG) {
                    System.out.println("******** Expert Details");
                    System.out.println(vecExpKnow.toString());
                }
                Kryo kryo = new Kryo();
                for (int j = 0; j < projects.size(); j++) {
                    Project projectObj = new Project(projects.get(j));
                    if (projectObj._crawlId == 0) {

//                        BOVector vecExpKnowClone = (BOVector) deepClone(vecExpKnow);
//                        //ReturnVector vecExpKnowClone = (ReturnVector) vecExpKnow;
//
//                        String projectPersonlities[] = projectObj.getProjectPersonalities();
//                        String projectKnowledge[] = projectObj.getProjectKnowledge();
//                        String projectExperties[] = projectObj.getProjectExpertise();
//                        String projectEducation[] = projectObj.getProjectEducation();
//                        String projectLanguages[] = projectObj.getProjectLanguages();
//                        String projectFuncTitles[] = projectObj.getProjectFunctionalTitle();
//                        String projectLocation[] = projectObj.getProjectLocation();
//                        String projectCoordinates[] = projectObj.getProjectCoordinates();
//
//                        String[] combineProject = JoinArray.joinArrayGeneric(projectPersonlities, projectKnowledge, projectExperties, projectEducation, projectLanguages, projectFuncTitles, projectLocation);
//                        String combineProjectStr = Arrays.toString(combineProject).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");
//
//                        BOVector vecProSkill = ttv.evaluateWithWeight(projectExperties, vecExpKnowClone.word_freq_vector, vecExpKnowClone.Distinct_words_text_1_2, 0.1, 0, "SKILL");
//                        BOVector vecProFunc = ttv.evaluateWithWeight(projectFuncTitles, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0, "FUNCTION_TITLE");
//                        BOVector vecProLoc = ttv.evaluateWithWeight(projectLocation, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0, "LOCATION");
//                        BOVector vecProEdu = ttv.evaluateWithWeight(projectEducation, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0, "EDUCATION");
//                        BOVector vecProLang = ttv.evaluateWithWeight(projectLanguages, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0, "LANGUAGE");
//                        BOVector vecProPer = ttv.evaluateWithWeight(projectPersonlities, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0, "PERSONALITY");
//                        BOVector vecProKnow = ttv.evaluateWithWeight(projectKnowledge, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0, "KNOWLDGE");
//
//                        double featureScore = cosSim.getScoreOnText(combineExpertStr, combineProjectStr);
//                        double avgDistance = Distance.calculate(expertCoordinates, projectCoordinates);
//                        double cosine_Score = cosSim.getScoreOnBOVector(vecProKnow);
//                        double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
//
//                        final HashMap<String, Double> matchingDic = new HashMap<>();
//                        matchingDic.put("expert_id", (double) expertObj._expertId);
//                        matchingDic.put("project_id", (double) projectObj._projectId);
//                        matchingDic.put("ml_score", avgScore);
//                        matchingArr.add(matchingDic);
//                        System.out.println("Expert = " + experts.get(i) + " Project = " + projects.get(j) + " Cosine = " + featureScore + " Category Cosine = " + cosine_Score + " Avg Score = " + avgScore);
                    } else {
                        BOVector vecExpKnowClone = (BOVector) deepClone(vecExpKnow);
                        String crawlCoordinates[] = projectObj.getProjectCoordinates();
                        double avgDistance = Distance.calculate(expertCoordinates, crawlCoordinates);
                        //System.out.println("Before Text: "+ projectObj._crawlText);
                        String crawlString = TextPreProcessor.normalizeText(projectObj._crawlText);
                        String lang = TextPreProcessor.getTextLanguage(crawlString);
                        if (lang.equals("en")) {
                            if (VERBOSE_DEBUG) {
                                System.out.println("******** Project Text:");
                                System.out.println(crawlString);
                            }
                            //                    Rake rakeObj = new Rake();
                            //                    String crawlFeatures[] = rakeObj.run(crawlString);

                            // LingPipe Categories
                            //NER_Testing testObj = new NER_Testing();
                            final Map<String, String> categories;
                            if (!cachePerdictions.containsKey(Integer.toString(projectObj._projectId))) {
                                System.out.println("************ Calculating from Model:");

                                categories = testObj.predictResults(WordUtils.capitalize(crawlString));
                                //categories = testObj.predictResults(crawlString);
                                cachePerdictions.put(Integer.toString(projectObj._projectId), categories);
                            } else {
                                System.out.println("************* From Cache");
                                categories = cachePerdictions.get(Integer.toString(projectObj._projectId));
                            }

                            List<String> crawlSkills = new ArrayList<String>();
                            List<String> crawlFunctionTitle = new ArrayList<String>();
                            List<String> crawlLocation = new ArrayList<String>();
                            List<String> crawlEducation = new ArrayList<String>();
                            List<String> crawlLanguage = new ArrayList<String>();
                            List<String> crawlPersonality = new ArrayList<String>();
                            List<String> crawlKnowledge = new ArrayList<String>();

                            for (Map.Entry<String, String> entry : categories.entrySet()) {
                                if (entry.getValue().equals("SKILL") == true) {
                                    crawlSkills.add(entry.getKey());
                                } else if (entry.getValue().equals("FUNCTIONTITLE") == true) {
                                    crawlFunctionTitle.add(entry.getKey());
                                } else if (entry.getValue().equals("LOCATION") == true) {
                                    crawlLocation.add(entry.getKey());
                                } else if ((entry.getValue().equals("EDUCATION")) == true || (entry.getValue().equals("DEGREE") == true)) {
                                    crawlEducation.add(entry.getKey());
                                } else if (entry.getValue().equals("LANGUAGE") == true) {
                                    crawlLanguage.add(entry.getKey());
                                } else if (entry.getValue().equals("PERSONALITY") == true) {
                                    crawlPersonality.add(entry.getKey());
                                } else if (entry.getValue().equals("KNOWLEDGE") == true) {
                                    crawlKnowledge.add(entry.getKey());
                                } else {
                                    //System.out.println("Useless Category");
                                }
                            }

                            for (String projectLocation : projectObj.getProjectLocation()) {
                                crawlLocation.add(projectLocation);
                            }

                            String crawlSkillArr[] = crawlSkills.toArray(new String[crawlSkills.size()]);
                            String crawlFunctionTitleArr[] = crawlFunctionTitle.toArray(new String[crawlFunctionTitle.size()]);
                            String crawlLocationArr[] = crawlLocation.toArray(new String[crawlLocation.size()]);
                            String crawlEducationArr[] = crawlEducation.toArray(new String[crawlEducation.size()]);
                            String crawlLanguageArr[] = crawlLanguage.toArray(new String[crawlLanguage.size()]);
                            String crawlPersonalityArr[] = crawlPersonality.toArray(new String[crawlPersonality.size()]);
                            String crawlKnowledgeArr[] = crawlKnowledge.toArray(new String[crawlKnowledge.size()]);

                            BOVector vecProSkill = ttv.evaluateWithWeightNGram(crawlSkillArr, vecExpKnowClone.word_freq_vector, vecExpKnowClone.Distinct_words_text_1_2, SKILL_WEIGHT, 0, "SKILL", vecExpKnowClone.Distinct_words_subset_cattype);
                            BOVector vecProFunc = ttv.evaluateWithWeightNGram(crawlFunctionTitleArr, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, FUNCTIONTITLE_WEIGHT, 0, "FUNCTION_TITLE", vecProSkill.Distinct_words_subset_cattype);
                            BOVector vecProLoc = ttv.evaluateWithWeightNGram(crawlLocationArr, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, LOCATION_WEIGHT, 0, "LOCATION", vecProFunc.Distinct_words_subset_cattype);
                            BOVector vecProEdu = ttv.evaluateWithWeightNGram(crawlEducationArr, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, EDUCATION_WEIGHT, 0, "EDUCATION", vecProLoc.Distinct_words_subset_cattype);
                            BOVector vecProLang = ttv.evaluateWithWeightNGram(crawlLanguageArr, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, LANGAUGE_WEIGHT, 0, "LANGUAGE", vecProEdu.Distinct_words_subset_cattype);
                            BOVector vecProPer = ttv.evaluateWithWeightNGram(crawlPersonalityArr, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, PERSONALITY_WEIGHT, 0, "PERSONALITY", vecProLang.Distinct_words_subset_cattype);
                            BOVector vecProKnow = ttv.evaluateWithWeightNGram(crawlKnowledgeArr, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, KNOWLEDGE_WEIGHT, 0, "KNOWLEDGE", vecProPer.Distinct_words_subset_cattype);

                            double cosine_Score = cosSim.getScoreOnBOVectorPerCategory(vecProKnow);
                            //System.out.println(cosine_Score);
                            //if(cosine_Score >= 0.5){

                            //String crawlFeaturesStr = Arrays.toString(crawlFeatures).replace("amp;", "").replace("[", "").replace("]", "");
                            String crawlFeaturesStr = crawlString.replace("amp;", "").replace("[", "").replace("]", "");
                            double featureScore = cosSim.getScoreOnText(combineExpertStr, crawlFeaturesStr);
                            //double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
                            double avgScore = 0.8 * (cosine_Score) + 0.2 * (featureScore);

                            if (!Double.isNaN(avgScore)) {
                                final HashMap<String, Double> matchingDic = new HashMap<String, Double>();
                                matchingDic.put("expert_id", (double) expertObj._expertId);
                                matchingDic.put("project_id", (double) projectObj._projectId);
                                matchingDic.put("ml_score", avgScore);
                                matchingDic.put("ml_score_cosine", cosine_Score);
                                matchingDic.put("ml_score_cosine_text", featureScore);
                                matchingDic.put("ml_score_distance", avgDistance);
                                if (cosine_Score >= 0.3 || featureScore >= 0.3) {
                                    matchingDicAll.add("ExpertId = " + expertObj._expertId + ", Projectid = " + projectObj._projectId + ", Avg Score = " + round(avgScore, 4) + ", Cosine Score = " + round(cosine_Score, 4) + ", Features Score = " + round(featureScore, 4));
                                }
                                matchingArr.add(matchingDic);
                            }
                            System.out.println("ExpertId = " + expertObj._expertId + ",Projectid = " + projectObj._projectId + ", Avg Score = " + round(avgScore, 4) + ", Cosine Score = " + round(cosine_Score, 4) + ", Features Score = " + round(featureScore, 4));
                            //}
                            // else{
                            //System.out.println("Ignoring!! Projectid =" + projectObj._projectId + ", Cosine Score = " + round(cosine_Score, 4));
                            //}
                        }
                    }
                    projectObj.close();

                }
                if (!VERBOSE_DEBUG) {
                    saveRec.InsertOrUpdateRecordSingleTable(matchingArr);
                }
            }
            // Check if Table exists
            //String sql = "INSERT OR REPLACE INTO expert_project_complete (expert_id, project_id, match_ml, ml_score_kh) " + "VALUES (?, ?, ?, ?)";

//            for (int k = 0; k < experts.size(); k++) {
//                String sql = "UPDATE expert SET k_ml_update = 0 WHERE expert_id = " + experts.get(k);
//                try {
//                    saveRec.updateRecords(sql);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            saveRec.close();
        }
        System.out.println("**************************** Final Score *********************");
        for (String result : matchingDicAll) {
            System.out.println(result);
        }
    }

    public static Object deepClone(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            String s1 = e.toString();
            System.out.println("Not able to clone");
            return null;
        }
    }

    public static double round(double value, int places) {
        if (value > 0) {
            if (places < 0) {
                throw new IllegalArgumentException();
            }

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
        return value;
    }

}
