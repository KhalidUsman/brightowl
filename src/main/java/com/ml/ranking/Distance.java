/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

/**
 *
 * @author shahzad
 */
public class Distance {
    public static double calculate(String[] coord1, String[] coord2) {

        final int R = 6371; // Radius of the earth
        double shortestDistance = 500.0;
        if (coord1.length > 0 && coord2.length > 0) {
            for (String coord1Str : coord1) {
                String[] coord1Arr = coord1Str.split(" ");
                if ((!coord1Arr[0].equals("null") && !coord1Arr[0].isEmpty()) && (!coord1Arr[1].equals("null") && !coord1Arr[1].isEmpty())) {
                    //if (!StringUtils.isNullOrEmpty(coord1Arr[0]) && !StringUtils.isNullOrEmpty(coord1Arr[1])) {
                    double lat1 = Double.parseDouble(coord1Arr[0]);
                    double lon1 = Double.parseDouble(coord1Arr[1]);

                    for (String coord2Str : coord2) {
                        String[] coord2Arr = coord2Str.split(" ");
                        if ((!coord2Arr[0].equals("null") && !coord2Arr[0].isEmpty()) && (!coord2Arr[1].equals("null") && !coord2Arr[1].isEmpty())) {
                            double lat2 = Double.parseDouble(coord2Arr[0]);
                            double lon2 = Double.parseDouble(coord2Arr[1]);

                            Double latDistance = Math.toRadians(lat2 - lat1);
                            Double lonDistance = Math.toRadians(lon2 - lon1);
                            Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                                    + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                                    * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
                            Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                            double distance = R * c; // convert to meters
                            if (distance < shortestDistance) {
                                shortestDistance = distance;
                            }
                            return shortestDistance;
                        } else {
                            return shortestDistance;
                        }
                    }
                } else {
                    return shortestDistance;
                }
            }
        } else {
            return shortestDistance;
        }
        return shortestDistance;
    }
}
