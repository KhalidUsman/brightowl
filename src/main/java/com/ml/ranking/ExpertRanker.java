/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import com.ml.ranking.util.JoinArray;
import com.Keyword_Extraction.Rake;
import com.ML_Package.NER_Testing;
import com.dbConnection.Connect_Database;
import com.dbConnection.Expert;
import com.dbConnection.Project;
import com.esotericsoftware.kryo.Kryo;
import com.ml.ranking.util.JoinArray;
import java.lang.reflect.Array;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author shahzad
 */
public class ExpertRanker {
    
        public void calculateMatchingExpertsForProject(List<Integer> experts, List<Integer> projects) throws Exception {

//        ArrayList<Map<String, Double>> matchingArr = new ArrayList<Map<String, Double>>();
//
//        TextToVector ttv = new TextToVector();
//        CosineSimilarity cosSim = new CosineSimilarity();
//        
//
//        for (int j = 0; j < projects.size(); j++) {
//            Project projectObj = new Project(projects.get(j));
//            if (projectObj._crawlId == 0) {
//
//                String projectPersonlities[] = projectObj.getProjectPersonalities();
//                String projectKnowledge[] = projectObj.getProjectKnowledge();
//                String projectExperties[] = projectObj.getProjectExpertise();
//                String projectEducation[] = projectObj.getProjectEducation();
//                String projectLanguages[] = projectObj.getProjectLanguages();
//                String projectFuncTitles[] = projectObj.getProjectFunctionalTitle();
//                String projectLocation[] = projectObj.getProjectLocation();
//                String projectCoordinates[] = projectObj.getProjectCoordinates();
//
//                String[] combineProject = JoinArray.joinArrayGeneric(projectPersonlities, projectKnowledge, projectExperties, projectEducation, projectLanguages, projectFuncTitles, projectLocation);
//                String combineProjectStr = Arrays.toString(combineProject).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");
//
//                HashMap<String, BOVectorPair> word_freq_vector = new HashMap<>();
//                ArrayList<String> Distinct_words_text_1_2 = new ArrayList<>();
//                HashMap<String, HashMap<String,String>> Distinct_words_subsets_catType = new HashMap<>();
//                BOVector vecProSkill = ttv.evaluateWithWeight(projectExperties, word_freq_vector, Distinct_words_text_1_2, 0.1, 0, "SKILL", Distinct_words_subsets_catType);
//                BOVector vecProFunc = ttv.evaluateWithWeight(projectFuncTitles, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0, "FUNCTION_TITLE", vecProSkill.Distinct_words_subset_cattype);
//                BOVector vecProLoc = ttv.evaluateWithWeight(projectLocation, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0, "LOCATION", vecProFunc.Distinct_words_subset_cattype);
//                BOVector vecProEdu = ttv.evaluateWithWeight(projectEducation, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0, "EDUCATION", vecProLoc.Distinct_words_subset_cattype);
//                BOVector vecProLang = ttv.evaluateWithWeight(projectLanguages, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0, "LANGUAGE", vecProEdu.Distinct_words_subset_cattype);
//                BOVector vecProPer = ttv.evaluateWithWeight(projectPersonlities, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0, "PERSONALITY", vecProLang.Distinct_words_subset_cattype);
//                BOVector vecProKnow = ttv.evaluateWithWeight(projectKnowledge, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0, "KNOWLEDGE", vecProPer.Distinct_words_subset_cattype);
//                Kryo kryo = new Kryo();
//                for (int i = 0; i < experts.size(); i++) {
//                    BOVector vecProKnowClone = (BOVector) kryo.copy(vecProKnow);
//                    Expert expertObj = new Expert(experts.get(i));
//                    String expertPersonalities[] = expertObj.getExpertPersonalities();
//                    String expertKnowledge[] = expertObj.getExpertKnowledge();
//                    String expertExpertise[] = expertObj.getExpertExpertise();
//                    String expertEducation[] = expertObj.getExpertEducation();
//                    String expertLanguages[] = expertObj.getExpertLanguages();
//                    String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
//                    String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
//                    String[] combineFuncTitles = (String[]) ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
//                    String expertLocation[] = expertObj.getExpertLocation();
//                    String expertCoordinates[] = expertObj.getExpertCoordinates();
//
//                    String[] combineExpert = JoinArray.joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation);
//                    String combineExpertStr = Arrays.toString(combineExpert).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");
//
//                    expertObj.close();
//
//
//                    BOVector vecExpSkill = ttv.evaluateWithWeight(expertExpertise, vecProKnowClone.word_freq_vector, vecProKnow.Distinct_words_text_1_2, 0.1, 1, "SKILL", vecProKnowClone.Distinct_words_subset_cattype);
//                    BOVector vecExpFunc = ttv.evaluateWithWeight(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, 0.1, 1, "FUNCTION_TITLE", vecProKnowClone.Distinct_words_subset_cattype);
//                    BOVector vecExpLoc = ttv.evaluateWithWeight(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, 0.1, 1, "LOCATION", vecProKnowClone.Distinct_words_subset_cattype);
//                    BOVector vecExpEdu = ttv.evaluateWithWeight(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, 0.1, 1, "EDUCATION", vecProKnowClone.Distinct_words_subset_cattype);
//                    BOVector vecExpLang = ttv.evaluateWithWeight(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, 2.0, 1, "LANGUAGE", vecProKnowClone.Distinct_words_subset_cattype);
//                    BOVector vecExpPer = ttv.evaluateWithWeight(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, 2.0, 1, "PERSONALITY", vecProKnowClone.Distinct_words_subset_cattype);
//                    BOVector vecExpKnow = ttv.evaluateWithWeight(expertKnowledge, vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, 1.3, 1, "KNOWLEDGE", vecProKnowClone.Distinct_words_subset_cattype);
//
//                    double cosine_Score = cosSim.getScoreOnBOVector(vecExpKnow);
//                    double featureScore = cosSim.getScoreOnText(combineExpertStr, combineProjectStr);
//                    double avgDistance = Distance.calculate(expertCoordinates, projectCoordinates);
//                    double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
//
//                    final Map<String, Double> matchingDic = new HashMap<String, Double>();
//                    matchingDic.put("expert_id", (double) expertObj._expertId);
//                    matchingDic.put("project_id", (double) projectObj._projectId);
//                    matchingDic.put("ml_score", avgScore);
//                    matchingArr.add(matchingDic);
//                    //System.out.println("Expert = " + experts.get(i) + " Project = " + projects.get(j) + " Cosine = " + featureScore + " Category Cosine = " + cosine_Score + " Avg Score = " + avgScore);
//                }
//            } else {
//                String crawlString = (projectObj._crawlText).replaceAll("\\<[^>]*>", " ").replace("nbsp;", "").replace("amp;", "").replace("&", " and ");
//                Rake rakeObj = new Rake();
//                String crawlFeatures[] = rakeObj.run(crawlString);
//
//                // LingPipe Categories
//                NER_Testing testObj = new NER_Testing();
//                final Map<String, String> categories = testObj.getCategories(crawlFeatures);
//
//                List<String> crawlSkills = new ArrayList<String>();
//                List<String> crawlFunctionTitle = new ArrayList<String>();
//                List<String> crawlLocation = new ArrayList<String>();
//                List<String> crawlEducation = new ArrayList<String>();
//                List<String> crawlLanguage = new ArrayList<String>();
//                List<String> crawlPersonality = new ArrayList<String>();
//                List<String> crawlKnowledge = new ArrayList<String>();
//
//                for (Map.Entry<String, String> entry : categories.entrySet()) {
//                    if (entry.getValue().equals("SKILL") == true) {
//                        crawlSkills.add(entry.getKey());
//                    } else if (entry.getValue().equals("FUNCTIONTITLE") == true) {
//                        crawlFunctionTitle.add(entry.getKey());
//                    } else if (entry.getValue().equals("LOCATION") == true) {
//                        crawlLocation.add(entry.getKey());
//                    } else if ((entry.getValue().equals("EDUCATION")) == true || (entry.getValue().equals("DEGREE") == true)) {
//                        crawlEducation.add(entry.getKey());
//                    } else if (entry.getValue().equals("LANGUAGE") == true) {
//                        crawlLanguage.add(entry.getKey());
//                    } else if (entry.getValue().equals("PERSONALITY") == true) {
//                        crawlPersonality.add(entry.getKey());
//                    } else if (entry.getValue().equals("KNOWLEDGE") == true) {
//                        crawlKnowledge.add(entry.getKey());
//                    } else {
//                        //System.out.println("Useless Category");
//                    }
//                }
//
//                String crawlSkillArr[] = crawlSkills.toArray(new String[crawlSkills.size()]);
//                String crawlFunctionTitleArr[] = crawlFunctionTitle.toArray(new String[crawlFunctionTitle.size()]);
//                String crawlLocationArr[] = crawlLocation.toArray(new String[crawlLocation.size()]);
//                String crawlEducationArr[] = crawlEducation.toArray(new String[crawlEducation.size()]);
//                String crawlLanguageArr[] = crawlLanguage.toArray(new String[crawlLanguage.size()]);
//                String crawlPersonalityArr[] = crawlPersonality.toArray(new String[crawlPersonality.size()]);
//                String crawlKnowledgeArr[] = crawlKnowledge.toArray(new String[crawlKnowledge.size()]);
//
//                HashMap<String, BOVectorPair> word_freq_vector = new HashMap<>();
//                ArrayList<String> Distinct_words_text_1_2 = new ArrayList<>();
//
//                BOVector vecProSkill = ttv.evaluateWithWeight(crawlSkillArr, word_freq_vector, Distinct_words_text_1_2, 0.1, 0, "SKILL");
//                BOVector vecProFunc = ttv.evaluateWithWeight(crawlFunctionTitleArr, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0, "FUNCTION_TITLE");
//                BOVector vecProLoc = ttv.evaluateWithWeight(crawlLocationArr, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0, "LOCATION");
//                BOVector vecProEdu = ttv.evaluateWithWeight(crawlEducationArr, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0, "EDUCATION");
//                BOVector vecProLang = ttv.evaluateWithWeight(crawlLanguageArr, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0, "LANGUAGE");
//                BOVector vecProPer = ttv.evaluateWithWeight(crawlPersonalityArr, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0, "PERSONALITY");
//                BOVector vecProKnow = ttv.evaluateWithWeight(crawlKnowledgeArr, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0, "KNOWLEDGE");
//                Kryo kryo = new Kryo();
//                
//                for (int i = 0; i < experts.size(); i++) {
//                    BOVector vecProKnowClone = (BOVector) kryo.copy(vecProKnow);
//                    Expert expertObj = new Expert(experts.get(i));
//                    String expertPersonalities[] = expertObj.getExpertPersonalities();
//                    String expertKnowledge[] = expertObj.getExpertKnowledge();
//                    String expertExpertise[] = expertObj.getExpertExpertise();
//                    String expertEducation[] = expertObj.getExpertEducation();
//                    String expertLanguages[] = expertObj.getExpertLanguages();
//                    String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
//                    String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
//                    String[] combineFuncTitles = (String[]) ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
//                    String expertLocation[] = expertObj.getExpertLocation();
//                    String expertCoordinates[] = expertObj.getExpertCoordinates();
//
//                    String[] combineExpert = JoinArray.joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation);
//                    String combineExpertStr = Arrays.toString(combineExpert).replace("amp;", "").replace("[", "").replace("]", "").replace("&", " and ");
//
//                    expertObj.close();
//                    
//                    String crawlCoordinates[] = projectObj.getProjectCoordinates();
//                    BOVector vecExpSkill = ttv.evaluateWithWeight(expertExpertise, vecProKnowClone.word_freq_vector, vecProKnow.Distinct_words_text_1_2, 0.1, 1, "SKILL");
//                    BOVector vecExpFunc = ttv.evaluateWithWeight(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, 0.1, 1, "FUNCTION_TITLE");
//                    BOVector vecExpLoc = ttv.evaluateWithWeight(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, 0.1, 1, "LOCATION");
//                    BOVector vecExpEdu = ttv.evaluateWithWeight(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, 0.1, 1, "EDUCATION");
//                    BOVector vecExpLang = ttv.evaluateWithWeight(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, 2.0, 1, "LANGUAGE");
//                    BOVector vecExpPer = ttv.evaluateWithWeight(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, 2.0, 1, "PERSOANLITY");
//                    BOVector vecExpKnow = ttv.evaluateWithWeight(expertKnowledge, vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, 1.3, 1, "KNOWLEDGE");
//                    
//                    String crawlFeaturesStr = Arrays.toString(crawlFeatures).replace("amp;", "").replace("[", "").replace("]", "");
//
//                    double cosine_Score = cosSim.getScoreOnBOVector(vecExpKnow);
//                    double featureScore = cosSim.getScoreOnText(combineExpertStr, crawlFeaturesStr);
//                    double avgDistance = Distance.calculate(expertCoordinates, crawlCoordinates);
//                    double avgScore = 0.6 * (cosine_Score) + 0.2 * (featureScore) + 0.2 * ((500 - avgDistance) / 500);
//                    
//
//                    final Map<String, Double> matchingDic = new HashMap<String, Double>();
//                    matchingDic.put("expert_id", (double) expertObj._expertId);
//                    matchingDic.put("project_id", (double) projectObj._projectId);
//                    matchingDic.put("ml_score", avgScore);
//                    matchingArr.add(matchingDic);
//
//                    //System.out.println(" Avg Score = " + avgScore  + " Cosine Score = " + cosine_Score + " Features Score = " + featureScore);
//                }
//            }
//            projectObj.close();
//        }
//        // Check if Table exists
//        //String sql = "INSERT OR REPLACE INTO expert_project_complete (expert_id, project_id, match_ml, ml_score_kh) " + "VALUES (?, ?, ?, ?)";
//        Connect_Database saveRec = new Connect_Database();
//        saveRec.readDataBase();
//        try {
////            saveRec.InsertOrUpdateRecord(matchingArr);
////            for (int k = 0; k < projects.size(); k++) {
////                String sql = "UPDATE projects SET k_ml_update = 0 WHERE project_id = " + projects.get(k);
////                try {
////                    saveRec.updateRecords(sql);
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            saveRec.close();
//        }
    }

}
