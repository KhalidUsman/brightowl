/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import java.io.Serializable;

/**
 *
 * @author shahzad
 */
public class BOVectorPair implements Serializable {
    double val1;
    double val2;
    String type;
    public String phrase;

    public BOVectorPair(double v1, double v2, String type) {
        this.val1 = v1;
        this.val2 = v2;
        this.type = type;
    }

    public void Update_VAl(double v1, double v2) {
        this.val1 = v1;
        this.val2 = v2;
    }
}
