/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

/**
 *
 * @author shahzad
 */
public class StringToNGram {

    public static HashMap<String, Integer> getNGramSimple(String sentence) {
        HashMap<String, Integer> subset = new HashMap<>();
        
        if (!isStopWord(sentence)) {
            StringBuilder subSetString = new StringBuilder();
            String[] array = sentence.split(" ");
            if(array.length > 4){
                subset.put(sentence, 1);
            }else{
                if (array.length > 1) {
                    for (int i = 1, max = 1 << array.length; i < max; ++i) {
                        for (int j = 0, k = 1; j < array.length; ++j, k <<= 1) {
                            if ((k & i) != 0) {
                                subSetString.append(array[j]).append(" ");    
                            }
                        }
                        //if(subSetString.toString().trim().split(" ").length > 1){
                        if(!com.ml.training.StopWords.isStopWord(subSetString.toString().trim())){
                            subset.put(subSetString.toString().trim(), subSetString.toString().trim().split(" ").length);
                        }
                        //}
                        subSetString.setLength(0);
                    }
                } else {
                    subset.put(sentence, 1);
                }
            }
        } else {
            subset.put(sentence, 1);
        }

        return subset;
    }

    public static HashMap<String, Integer> getNGramShingle(String sentence) throws IOException {
        System.out.println("Spliting Sentence: " + sentence);
        HashMap<String, Integer> subset = new HashMap<>();
        if (!isStopWord(sentence)) {
            if (sentence.split(" ").length > 1) {

                StringReader reader = new StringReader(sentence);
                StandardTokenizer source = new StandardTokenizer(Version.LUCENE_46, reader);
                TokenStream tokenStream = new StandardFilter(Version.LUCENE_46, source);
                int shingleLength = (sentence.split(" ").length > 2) ? sentence.split(" ").length - 1 : 2;
                try (ShingleFilter sf = new ShingleFilter(tokenStream, shingleLength)) {
                    sf.setOutputUnigrams(true);
                    CharTermAttribute charTermAttribute = sf.addAttribute(CharTermAttribute.class);
                    sf.reset();

                    while (sf.incrementToken()) {
                        subset.put(charTermAttribute.toString(), charTermAttribute.toString().split(" ").length);
                        //System.out.println(charTermAttribute.toString() + " ----- " + charTermAttribute.toString().split(" ").length);
                    }

                    sf.end();
                }
            } else {
                subset.put(sentence, 1);
            }
        } else {
            subset.put(sentence, 1);
        }
        return subset;
    }

    public static boolean isStopWord(String sentence) {
        String[] stopWords = new String[]{
            "b. sc.",
            "b sc",
            "bs c",
            "bs. c.",
            "bs. c",
            "b. s. c.",
            "m. sc.",
            "m. s. c.",
            "m. sc.",
            "ms. c.",
            "ms. c",
            "ms c",
            "m sc",
            "p. hd.",
            "ph. d.",
            "ph. d",
            "p. h. d.",
            "ph d"

        };

        return Arrays.asList(stopWords).contains(sentence);

    }

}
