/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

/**
 *
 * @author shahzad
 */
public class TextToVector {
    //private final ReentrantLock lock = new ReentrantLock();

    public BOVector evaluateWithWeight(String featureArray[], HashMap<String, BOVectorPair> word_freq_text, ArrayList<String> Distinct_words, double weight, int typeId, String catType, HashMap<String, HashMap<String,String>> Distinct_words_subsets_catType) {

        //prepare word frequency vector by using Text1
        for (String feature : featureArray) {
            feature = feature.trim().toLowerCase();
            if (feature.length() > 0) {
                if (word_freq_text.containsKey(feature)) {
                    BOVectorPair vals1 = word_freq_text.get(feature);
                    // ValueId = 1 for Expert and else for Project
                    if (typeId == 1) {
                        double freq1 = vals1.val1 + 1;
                        double freq2 = vals1.val2;
                        vals1.Update_VAl(freq1, freq2);
                    } else {
                        double freq1 = vals1.val1;
                        double freq2 = vals1.val2 + 1;
                        vals1.Update_VAl(freq1, freq2);
                    }
                    word_freq_text.put(feature, vals1);
                } else {
                    if (typeId == 1) {
                        BOVectorPair vals1 = new BOVectorPair(1, 0, catType);
                        word_freq_text.put(feature, vals1);
                    } else {
                        BOVectorPair vals1 = new BOVectorPair(0, 1, catType);
                        word_freq_text.put(feature, vals1);
                    }
                    Distinct_words.add(feature);
                }
            }
        }
        for (int i = 0; i < Distinct_words.size(); i++) {
            String keyword = Distinct_words.get(i);
            BOVectorPair vals1 = word_freq_text.get(keyword);
            if (typeId == 1) {
                double freq1 = vals1.val1 * weight;
                double freq2 = vals1.val2;
                vals1.Update_VAl(freq1, freq2);
            } else {
                double freq1 = vals1.val1;
                double freq2 = vals1.val2 * weight;
                vals1.Update_VAl(freq1, freq2);
            }
            word_freq_text.put(keyword, vals1);
        }
        
        HashMap<String, Integer> subsets = new HashMap<>();
        HashMap<String, String> Distinct_words_subsets = new HashMap<>();
        for (Map.Entry<String, BOVectorPair> dicWord : word_freq_text.entrySet()) {
            BOVectorPair vals1 = word_freq_text.get(dicWord.getKey());
            // ValueId = 1 for Expert and else for Project
            if (vals1.type.toLowerCase().equalsIgnoreCase(catType)) {
                subsets = StringToNGram.getNGramSimple(dicWord.getKey().toLowerCase());
                for (Map.Entry<String, Integer> subset : subsets.entrySet()) {
                    Distinct_words_subsets.put(subset.getKey(), dicWord.getKey());
                }
            }
        }
        BOVector retVector = new BOVector(word_freq_text, Distinct_words);
        Distinct_words_subsets_catType.put(catType, Distinct_words_subsets);
        retVector.Distinct_words_subset_cattype = Distinct_words_subsets_catType;
        
        return retVector;
    }

    public BOVector evaluateWithWeightNGram(String featureArray[], HashMap<String, BOVectorPair> word_freq_text, ArrayList<String> Distinct_words, double weight, int typeId, String catType, HashMap<String, HashMap<String,String>> words_subsets) throws IOException {
        HashMap<String, Integer> subsets = new HashMap<>();

        //HashMap<String, String> Distinct_words_subsets = words_subsets.get(catType);
        //prepare word frequency vector by using Text1
//        for (Map.Entry<String, BOVectorPair> dicWord : word_freq_text.entrySet()) {
//            BOVectorPair vals1 = word_freq_text.get(dicWord.getKey());
//            // ValueId = 1 for Expert and else for Project
//            if (vals1.type.toLowerCase().equalsIgnoreCase(catType)) {
//                subsets = StringToNGram.getNGramSimple(dicWord.getKey().toLowerCase());
//                for (Map.Entry<String, Integer> subset : subsets.entrySet()) {
//                    Distinct_words_subsets.put(subset.getKey(), dicWord.getKey());
//                }
//            }
//        }

        for (String feature : featureArray) {
            feature = feature.replaceAll("\\s+", " ").trim().toLowerCase();
            if (feature.length() > 0) {
                subsets = StringToNGram.getNGramSimple(feature);
                if (!subsets.containsKey(feature)) {
                    subsets.put(feature, feature.split(" ").length);
                }
                for (Map.Entry<String, Integer> subset : subsets.entrySet()) {
                    String subsetString = subset.getKey();
                    float subsetWeight = calcWeight(feature, subsetString);
                    if (word_freq_text.containsKey(subsetString)) {
                        BOVectorPair vals1 = word_freq_text.get(subsetString);
                        // ValueId = 1 for Expert and else for Project
                        if (vals1.type.toLowerCase().equalsIgnoreCase(catType)) {
                            if (typeId == 1) {
                                double freq1 = vals1.val1 + subsetWeight;
                                double freq2 = vals1.val2;
                                vals1.Update_VAl(freq1, freq2);
                            } else {
                                double freq1 = vals1.val1;
                                double freq2 = vals1.val2 + subsetWeight;
                                vals1.Update_VAl(freq1, freq2);
                            }
                            word_freq_text.put(subsetString, vals1);
                        }
                    } else {
                        boolean foundInSideString = false;

                        if (words_subsets.get(catType).containsKey(subsetString)) {
                            BOVectorPair vals1 = word_freq_text.get(words_subsets.get(catType).get(subsetString));
                            if (vals1.type.toLowerCase().equalsIgnoreCase(catType)) {
                                if (typeId == 1) {
                                    double freq1 = vals1.val1 + subsetWeight;
                                    double freq2 = vals1.val2;
                                    vals1.Update_VAl(freq1, freq2);
                                } else {
                                    double freq1 = vals1.val1;
                                    double freq2 = vals1.val2 + subsetWeight;
                                    vals1.Update_VAl(freq1, freq2);
                                }
                                word_freq_text.put(words_subsets.get(catType).get(subsetString), vals1);
                            }
                        }

                        //Pattern re = Pattern.compile("\\bCA\\b",Pattern.CASE_INSENSITIVE);
                        //synchronized(this){
                        //Pattern pattern = Pattern.compile(".*\\b" + subsetString + "\\b.*", Pattern.CASE_INSENSITIVE);
                        //for (Map.Entry<String, BOVectorPair> dicWord : word_freq_text.entrySet()) {
//                            //if(dicWord.getKey().matches(".*\\b" + subsetString + "\\b.*")){
//                            //if (pattern.matcher(dicWord.getKey()).find()) { 
//
//                            if (subset.getValue() == 1) {
//                                Pattern pattern = Pattern.compile(".*\\b" + subsetString + "\\b.*", Pattern.CASE_INSENSITIVE);
//                                foundInSideString = pattern.matcher(dicWord.getKey()).find();
//                            } else if (dicWord.getKey().split(" ").length > 1 && subset.getValue() > 1) {
//                                foundInSideString = dicWord.getKey().contains(subsetString);
//                            } else {
//                                foundInSideString = dicWord.getKey().contains(subsetString);
//                            }
//                            if (foundInSideString) {
//                                BOVectorPair vals1 = word_freq_text.get(dicWord.getKey());
//                                // ValueId = 1 for Expert and else for Project
//                                if (vals1.type.toLowerCase().equalsIgnoreCase(catType)) {
//                                    if (typeId == 1) {
//                                        double freq1 = vals1.val1 + subsetWeight;
//                                        double freq2 = vals1.val2;
//                                        vals1.Update_VAl(freq1, freq2);
//                                    } else {
//                                        double freq1 = vals1.val1;
//                                        double freq2 = vals1.val2 + subsetWeight;
//                                        vals1.Update_VAl(freq1, freq2);
//                                    }
//                                    word_freq_text.put(dicWord.getKey(), vals1);
//                                }
//                            }
//                        }
                        //}
                        if (!foundInSideString) {
                            if (typeId == 1) {
                                BOVectorPair pair = new BOVectorPair(subsetWeight, 0, catType);
                                word_freq_text.put(subsetString, pair);
                            } else {
                                BOVectorPair pair = new BOVectorPair(0, subsetWeight, catType);
                                word_freq_text.put(subsetString, pair);
                            }
                            Distinct_words.add(subsetString);
                            words_subsets.get(catType).put(subsetString, subsetString);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < Distinct_words.size(); i++) {
            String keyword = Distinct_words.get(i);
            BOVectorPair vals1 = word_freq_text.get(keyword);
            if (typeId == 1) {
                double freq1 = vals1.val1 * weight;
                double freq2 = vals1.val2;
                vals1.Update_VAl(freq1, freq2);
            } else {
                double freq1 = vals1.val1;
                double freq2 = vals1.val2 * weight;
                vals1.Update_VAl(freq1, freq2);
            }
            word_freq_text.put(keyword, vals1);
        }

        BOVector retVector = new BOVector(word_freq_text, Distinct_words, words_subsets);
        return retVector;
    }

    public float calcWeight(String baseString, String subset) {

        float baseWordCount = baseString.split(" ").length;
        float subsetWordCount = subset.split(" ").length;

        return subsetWordCount / baseWordCount;
    }
}
