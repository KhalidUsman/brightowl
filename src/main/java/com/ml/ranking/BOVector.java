/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author shahzad
 */
public class BOVector implements Serializable {
    public HashMap<String, BOVectorPair> word_freq_vector = new HashMap<>();
    public ArrayList<String> Distinct_words_text_1_2 = new ArrayList<>();
    public HashMap<String, HashMap<String,String>> Distinct_words_subset_cattype = new HashMap<>();
   
    public BOVector(HashMap<String, BOVectorPair> word_freq_text, ArrayList<String> Distinct_words) {
        this.word_freq_vector = word_freq_text;
        this.Distinct_words_text_1_2 = Distinct_words;
    }
    
    public BOVector(HashMap<String, BOVectorPair> word_freq_text, ArrayList<String> Distinct_words, HashMap<String, HashMap<String,String>> Distinct_words_subset_cattype) {
        this.word_freq_vector = word_freq_text;
        this.Distinct_words_text_1_2 = Distinct_words;
        this.Distinct_words_subset_cattype = Distinct_words_subset_cattype;
    }
    
    public String toString(){
        
        StringBuilder sb = new StringBuilder();
        for(String section: Distinct_words_text_1_2){
            BOVectorPair pair = word_freq_vector.get(section);
            sb.append("\n " + section + " - " + pair.type);
        }
        return sb.toString();
    }
}
