/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import com.config.AppConfig;
import com.ml.ranking.fullranker.FullRanker;
import com.ml.ranking.appendranker.AppendRanker;
import com.ml.ranking.dailyranker.DailyRanker;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author shahzad
 */
public class Main {

    public static boolean VERBOSE_DEBUG = false;
    public static int PROJECT_ID = 168190;
    public static int EXPERT_ID = 3479;
    public static String PROFILE = "prod";

    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        AppConfig config = AppConfig.createFromYamlSettings(PROFILE);
        Ranker ranker = null;
        if (config.getRunType().equalsIgnoreCase("full_run")) {
            ranker = new FullRanker();
        }
        else if (config.getRunType().equalsIgnoreCase("daily_run")) {
            ranker = new DailyRanker();
        }else {
            ranker = new AppendRanker();
        }
        ranker.rank();

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Total time spent in execution: " + TimeUnit.MILLISECONDS.toSeconds(elapsedTime) + "secs");

        System.out.println("Done");
    }
}
