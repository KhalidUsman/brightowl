/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking;

import static com.ml.ranking.Main.VERBOSE_DEBUG;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author shahzad
 */
public class CosineSimilarity {

    /**
     *
     * @param Text1
     * @param Text2
     * @return
     */
    public double getScoreOnText(String Text1, String Text2) {

        double sim_score = 0.0000000;
        //1. Identify distinct words from both documents
        String[] word_seq_text1 = Text1.split(" ");
        String[] word_seq_text2 = Text2.split(" ");
        HashMap<String, BOVectorPair> word_freq_vector = new HashMap<>();
        ArrayList<String> Distinct_words_text_1_2 = new ArrayList<>();

        //prepare word frequency vector by using Text1
        for (int i = 0; i < word_seq_text1.length; i++) {
            String tmp_wd = word_seq_text1[i].trim();
            if (tmp_wd.length() > 0) {
                if (word_freq_vector.containsKey(tmp_wd)) {
                    BOVectorPair vals1 = word_freq_vector.get(tmp_wd);
                    double freq1 = vals1.val1 + 1;
                    double freq2 = vals1.val2;
                    vals1.Update_VAl(freq1, freq2);
                    word_freq_vector.put(tmp_wd, vals1);
                } else {
                    BOVectorPair vals1 = new BOVectorPair(1, 0, "TEXT");
                    word_freq_vector.put(tmp_wd, vals1);
                    Distinct_words_text_1_2.add(tmp_wd);
                }
            }
        }

        //prepare word frequency vector by using Text2
        for (int i = 0; i < word_seq_text2.length; i++) {
            String tmp_wd = word_seq_text2[i].trim();
            if (tmp_wd.length() > 0) {
                if (word_freq_vector.containsKey(tmp_wd)) {
                    BOVectorPair vals1 = word_freq_vector.get(tmp_wd);
                    double freq1 = vals1.val1;
                    double freq2 = vals1.val2 + 1;
                    vals1.Update_VAl(freq1, freq2);
                    word_freq_vector.put(tmp_wd, vals1);
                } else {
                    BOVectorPair vals1 = new BOVectorPair(0, 1, "TEXT");
                    word_freq_vector.put(tmp_wd, vals1);
                    Distinct_words_text_1_2.add(tmp_wd);
                }
            }
        }

        //calculate the cosine similarity score.
        double VectAB = 0.0000000;
        double VectA_Sq = 0.0000000;
        double VectB_Sq = 0.0000000;

        for (int i = 0; i < Distinct_words_text_1_2.size(); i++) {
            BOVectorPair vals12 = word_freq_vector.get(Distinct_words_text_1_2.get(i));
            double freq1 = (double) vals12.val1;
            double freq2 = (double) vals12.val2;
            //System.out.println(Distinct_words_text_1_2.get(i)+"#"+freq1+"#"+freq2);

            VectAB = VectAB + (freq1 * freq2);
            VectA_Sq = VectA_Sq + freq1 * freq1;
            VectB_Sq = VectB_Sq + freq2 * freq2;
        }
        //System.out.println("VectAB "+VectAB+" VectA_Sq "+VectA_Sq+" VectB_Sq "+VectB_Sq);
        sim_score = ((VectAB) / (Math.sqrt(VectA_Sq) * Math.sqrt(VectB_Sq)));

        return (sim_score);
    }

    /**
     *
     * @param rv
     * @return
     */
    public double getScoreOnBOVector(BOVector rv) {
        double sim_score = 0.0000000;
        //calculate the cosine similarity score.
        double VectAB = 0.0000000;
        double VectA_Sq = 0.0000000;
        double VectB_Sq = 0.0000000;

        for (int i = 0; i < rv.Distinct_words_text_1_2.size(); i++) {
            BOVectorPair vals12 = rv.word_freq_vector.get(rv.Distinct_words_text_1_2.get(i));
            double freq1 = (double) vals12.val1;
            double freq2 = (double) vals12.val2;
            if (freq1 > 0 && freq2 > 0) {
                if (VERBOSE_DEBUG) {
                    //System.out.println(rv.Distinct_words_text_1_2.get(i) + " -- Freq1= " + freq1 + " -- Freq2= " + freq2);
                }
            }

            // Calculate the dot product
            VectAB += freq1 * freq2;

            // Calculate the magnitude of val1
            VectA_Sq += Math.pow(freq1, 2);

            // Calculate the magnitude of val2
            VectB_Sq += Math.pow(freq2, 2);
        }

        //System.out.println("VectAB "+VectAB+" VectA_Sq "+VectA_Sq+" VectB_Sq "+VectB_Sq);
        //Calculate the CosSim of vectors
        sim_score = ((VectAB) / (Math.sqrt(VectA_Sq * VectB_Sq)));

        return sim_score;
    }
    
    /**
     *
     * @param rv
     * @return
     */
    public double getScoreOnBOVectorPerCategory(BOVector rv) {
        double sim_score = 0.0000000;
        
        Map<String,ArrayList<BOVectorPair>> vectorByCategory = collectBOVectorPairByType(rv);
        
        int totalCategoriesFound = vectorByCategory.size();
        
        double multiplicationFactor = 1.0 / (double) totalCategoriesFound;
        
        if (VERBOSE_DEBUG) {
            System.out.println("ML Factor: " + multiplicationFactor);
        }
        for(Entry<String,ArrayList<BOVectorPair>> entry: vectorByCategory.entrySet()){
            //calculate the cosine similarity score.
            double VectAB = 0.0000000;
            double VectA_Sq = 0.0000000;
            double VectB_Sq = 0.0000000;

            for (int i = 0; i < entry.getValue().size(); i++) {
                BOVectorPair vals12 = entry.getValue().get(i);
                double freq1 = (double) vals12.val1;
                double freq2 = (double) vals12.val2;
                if (freq1 > 0 && freq2 > 0) {
                    if (VERBOSE_DEBUG) {
                        System.out.println(vals12.phrase + " -- Freq1= " + freq1 + " -- Freq2= " + freq2+ " -- TYPE= " + vals12.type);
                    }
                }

                // Calculate the dot product
                VectAB += freq1 * freq2;

                // Calculate the magnitude of val1
                VectA_Sq += Math.pow(freq1, 2);

                // Calculate the magnitude of val2
                VectB_Sq += Math.pow(freq2, 2);
            }

            //System.out.println("VectAB "+VectAB+" VectA_Sq "+VectA_Sq+" VectB_Sq "+VectB_Sq);
            //Calculate the CosSim of vectors
            double  curentSimScore = ((VectAB) / (Math.sqrt(VectA_Sq * VectB_Sq)));
            if(Double.isNaN(curentSimScore)){  
                curentSimScore = 0.00d;
            }
            if (VERBOSE_DEBUG) {
                System.out.println("Consine Similarity of type: "+ entry.getKey() + ", value: "+curentSimScore);
                System.out.println("Consine Similarity with ML Factor: "+ entry.getKey() + ", value: "+ (curentSimScore * multiplicationFactor));
            }
            
            if(!Double.isNaN(curentSimScore)){       
                sim_score += curentSimScore * multiplicationFactor;
            }
        }

        return sim_score;
    }
    
    /**
     *
     * @param rv
     * @return
     */
    public Map<String,Double> getScoreOnBOVectorPerCategoryExplicitly(BOVector rv) {
        Map<String,Double> cat_sim_score = new HashMap<>();
        double sim_score = 0.0000000;
        
        Map<String,ArrayList<BOVectorPair>> vectorByCategory = collectBOVectorPairByType(rv);
        
        int totalCategoriesFound = vectorByCategory.size();
        
        double multiplicationFactor = 1.0 / (double) totalCategoriesFound;
        
        if (VERBOSE_DEBUG) {
            System.out.println("ML Factor: " + multiplicationFactor);
        }
        for(Entry<String,ArrayList<BOVectorPair>> entry: vectorByCategory.entrySet()){
            //calculate the cosine similarity score.
            double VectAB = 0.0000000;
            double VectA_Sq = 0.0000000;
            double VectB_Sq = 0.0000000;

            for (int i = 0; i < entry.getValue().size(); i++) {
                BOVectorPair vals12 = entry.getValue().get(i);
                double freq1 = (double) vals12.val1;
                double freq2 = (double) vals12.val2;
                if (freq1 > 0 && freq2 > 0) {
                    if (VERBOSE_DEBUG) {
                        System.out.println(vals12.phrase + " -- Freq1= " + freq1 + " -- Freq2= " + freq2+ " -- TYPE= " + vals12.type);
                    }
                }

                // Calculate the dot product
                VectAB += freq1 * freq2;

                // Calculate the magnitude of val1
                VectA_Sq += Math.pow(freq1, 2);

                // Calculate the magnitude of val2
                VectB_Sq += Math.pow(freq2, 2);
            }

            //System.out.println("VectAB "+VectAB+" VectA_Sq "+VectA_Sq+" VectB_Sq "+VectB_Sq);
            //Calculate the CosSim of vectors
            double  curentSimScore = ((VectAB) / (Math.sqrt(VectA_Sq * VectB_Sq)));
            if(Double.isNaN(curentSimScore)){  
                curentSimScore = 0.00d;
            }
            cat_sim_score.put(entry.getKey().toLowerCase(), curentSimScore);
            if (VERBOSE_DEBUG) {
                System.out.println("Consine Similarity of type: "+ entry.getKey() + ", value: "+curentSimScore);
                System.out.println("Consine Similarity with ML Factor: "+ entry.getKey() + ", value: "+ (curentSimScore * multiplicationFactor));
            }
            
            if(!Double.isNaN(curentSimScore)){       
                sim_score += curentSimScore * multiplicationFactor;
            }
        }
        cat_sim_score.put("total", sim_score);
        cat_sim_score.put("ml_factor", multiplicationFactor);
        return cat_sim_score;
    }
    
    private Map<String,ArrayList<BOVectorPair>> collectBOVectorPairByType(BOVector rv){
        Map<String,ArrayList<BOVectorPair>> typeMap = new HashMap<>();
        
        for(Entry<String, BOVectorPair> entry: rv.word_freq_vector.entrySet()){
            if(typeMap.containsKey(entry.getValue().type)){
                ArrayList<BOVectorPair> pairList = typeMap.get(entry.getValue().type);
                entry.getValue().phrase = entry.getKey();
                pairList.add(entry.getValue());
                typeMap.put(entry.getValue().type, pairList);
                
            }else{
                ArrayList<BOVectorPair> pairList = new ArrayList<>();
                entry.getValue().phrase = entry.getKey();
                pairList.add(entry.getValue());
                typeMap.put(entry.getValue().type, pairList);
            }
        }
        
        return typeMap;
    }
}
