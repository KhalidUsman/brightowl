/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking.appendranker;

import com.dbConnection.Connect_Database;
import com.ml.ranking.Ranker;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author shahzad
 */
public class AppendRanker implements Ranker {
    
    /**
     * select e.expert_id, p.project_id 
from 
(SELECT expert_id FROM expert WHERE profile_complete_percent >= 60 AND k_ml_update = 1) e, 
(SELECT project_id FROM projects where is_crawled=1) p
WHERE 
(e.expert_id, p.project_id) 
NOT IN 
(select expert_id, project_id from z_expert_project_complete_all) limit 10
     * @throws Exception 
     */
    
    @Override
    public void rank() throws Exception{
        System.out.println("Starting the partial run ranking");
        Connect_Database dao = new Connect_Database();
        dao.getDBConnection();

        ResultSet expertsForExperts = dao.getExperts();
        ResultSet projectsForExperts = dao.getProjectsWithId();

        //ResultSet projectsForProjects = dao.getProjects();
        ResultSet expertsForProjects = dao.getExpertsWithIdAndProjects();
        
        Map<Integer,Set<Integer>> listExpForProjects = new HashMap<>();
        
        Set<Integer> listProjects = null;
        
        while (expertsForProjects.next()) {
            if(listExpForProjects.containsKey(expertsForProjects.getInt("expert_id"))){
                //listProjects.clear();
                listProjects = listExpForProjects.get(expertsForProjects.getInt("expert_id"));
                listProjects.add(expertsForProjects.getInt("project_id"));
                listExpForProjects.put(expertsForProjects.getInt("expert_id"), listProjects);
                System.out.println("In");
            }
            else{
                listProjects = new HashSet<>();
                listProjects.add(expertsForProjects.getInt("project_id"));
                listExpForProjects.put(expertsForProjects.getInt("expert_id"), listProjects);
                System.out.println("out");
            }
            //listExpForProjects.add(expertsForProjects.getInt("expert_Id"));
            System.out.println("Expert id = " + expertsForProjects.getInt("expert_id") + ", Project id = " + expertsForProjects.getInt("project_id"));
        }
        
        for (Entry<Integer,Set<Integer>> e : listExpForProjects.entrySet()){
            System.out.println("ExpertId: " + e.getKey());
            System.out.println("Projects: ");
            for(Integer project: e.getValue()){
                System.out.print(project + ", ");
            }
            System.out.println("---");
        }

        //Save into Arrays
//        java.util.List<Integer> listExpForExperts = new ArrayList<>();
//        java.util.List<Integer> listProForExperts = new ArrayList<>();
//        while (expertsForExperts.next()) {
//            listExpForExperts.add(expertsForExperts.getInt("expert_Id"));
//        }
//        while (projectsForExperts.next()) {
//            listProForExperts.add(projectsForExperts.getInt("project_id"));
//            System.out.println("Project id = " + projectsForExperts.getInt("project_id"));
//        }
//        java.util.List<Integer> listProForProjects = new ArrayList<>();
//        java.util.List<Integer> listExpForProjects = new ArrayList<>();
//        while (projectsForProjects.next()) {
//            listProForProjects.add(projectsForProjects.getInt("project_id"));
//        }
//        while (expertsForProjects.next()) {
//            listExpForProjects.add(expertsForProjects.getInt("expert_Id"));
//            //System.out.println("Expert id = " + expertsForProjects.getInt("expert_Id"));
//        }
//        dao.close();
//        System.out.println("Experts found:" + listExpForProjects.size());
//        System.out.println("Projects found:" + listProForProjects.size());
//
        ProjectRanker pr = new ProjectRanker();
        pr.calculateMatchingProjectsForExpert(listExpForProjects);
        //Main.calculateMatchingexpertsForProject(listExpForExperts, listProForExperts);
    }
}
