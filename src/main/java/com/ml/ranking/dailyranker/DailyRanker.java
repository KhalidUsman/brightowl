/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.ranking.dailyranker;

import com.config.AppConfig;
import com.dbConnection.Connect_Database;
import com.ml.ranking.Ranker;
import com.ml.ranking.appendranker.AppendRanker;
import com.ml.ranking.fullranker.FullRanker;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author shahzad
 */
public class DailyRanker implements Ranker {
    

    @Override
    public void rank() throws Exception {
        System.out.println("Starting the daily run ranking");

        Connect_Database dao = new Connect_Database();
        dao.getDBConnection();

//        ResultSet expertsForExperts = dao.getExperts();
//        ResultSet projectsForExperts = dao.getProjectsWithId();
        ResultSet projectsForProjects = dao.getProjectsByDate(this.getYesterdayDate());
        ResultSet expertsForProjects = dao.getExpertsWithId();

        //Save into Arrays
        java.util.List<Integer> listExpForExperts = new ArrayList<>();
        java.util.List<Integer> listProForExperts = new ArrayList<>();
//        while (expertsForExperts.next()) {
//            listExpForExperts.add(expertsForExperts.getInt("expert_Id"));
//        }
//        while (projectsForExperts.next()) {
//            listProForExperts.add(projectsForExperts.getInt("project_id"));
//            System.out.println("Project id = " + projectsForExperts.getInt("project_id"));
//        }
        java.util.ArrayList<Integer> listProForProjects = new ArrayList<>();
        java.util.List<Integer> listExpForProjects = new ArrayList<>();
        while (projectsForProjects.next()) {
            listProForProjects.add(projectsForProjects.getInt("project_id"));
        }
        while (expertsForProjects.next()) {
            listExpForProjects.add(expertsForProjects.getInt("expert_Id"));
            //System.out.println("Expert id = " + expertsForProjects.getInt("expert_Id"));
        }
        dao.close();
        System.out.println("Experts found:" + listExpForProjects.size());
        System.out.println("Projects found:" + listProForProjects.size());

        ProjectRankerStream pr = new ProjectRankerStream();
        pr.calculateMatchingProjectsForExpert(listExpForProjects, listProForProjects);
        //Main.calculateMatchingexpertsForProject(listExpForExperts, listProForExperts);
    }

    private String getYesterdayDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        // Create a calendar object with today date. Calendar is in java.util pakage.
        Calendar calendar = Calendar.getInstance();

        // Move calendar to yesterday
        calendar.add(Calendar.DATE, -1);

        // Get current date of calendar which point to the yesterday now
        Date yesterday = calendar.getTime();

        return dateFormat.format(yesterday);
    }
    
    
    

    
}
