/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.training;

/**
 *
 * @author shahzad
 */
public class StringNormalizer {

    public static String normalizeDegrees(String input) {
        input = input.replaceAll("([\\ud800-\\udbff\\udc00-\\udfff])", "");
        input = input.replace("B. sc.", "B.Sc");
        input = input.replace("B. Sc.", "B.Sc");
        input = input.replace("B. s. c.", "B.Sc");
        input = input.replace("M. sc.", "M.sc");
        input = input.replace("M. s. c.", "M.Sc");
        input = input.replace("M. Sc.", "M.Sc");
        input = input.replace("P. hd.", "P.hd");
        input = input.replace("P. Hd.", "P.hd");
        input = input.replace("P. h. d.", "P.hc");
        return input;
    }
    
    public static String[] textToLines(String input){
        return input.split("(?<=[.!?]) +");
    }
}
