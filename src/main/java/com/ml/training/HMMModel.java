/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.training;

import com.Keyword_Extraction.Rake;
import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.Chunking;
import com.aliasi.util.AbstractExternalizable;
import static com.ml.ranking.Main.VERBOSE_DEBUG;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author shahzad
 */
public class HMMModel {

    private Chunker chunker = null;

    public HMMModel(String modleFilePath) throws IOException, ClassNotFoundException {
        System.out.println("Reading model from path: "+ modleFilePath);
        File modelFile = new File(modleFilePath);
        chunker = (Chunker) AbstractExternalizable.readObject(modelFile);
    }
    
    /**
     * 
     * @param input
     * @throws ClassNotFoundException
     * @throws IOException 
     */
    public void validate(String input) throws ClassNotFoundException, IOException {
        String[] lines = StringNormalizer.textToLines(StringNormalizer.normalizeDegrees(input));

        for (final String line : lines) {
            Chunking chunking = chunker.chunk(line);
            Set<Chunk> cs = chunking.chunkSet();
            for (Chunk c : cs) {
                System.out.println("Test String is (" + line.substring(c.start(), c.end()) + ") Starts: " + c.start() + " Ends:" + c.end() + " Score: " + c.score() + " Category is = " + c.type());
            }
        }
    }
    
    /**
     * 
     * @param input
     * @return 
     */
    public Map<String, String> getPredictedCategories(String input){
        
        Map<String, String> pc = new HashMap<>();
        
        //String[] lines = StringNormalizer.textToLines(StringNormalizer.normalizeDegrees(input));

        for (final String line : Arrays.asList(input)) {
            //System.out.println("Line: " + input);
            Chunking chunking = chunker.chunk(line);
            Set<Chunk> cs = chunking.chunkSet();
            for (Chunk c : cs) {  
                if(!StopWords.isStopWord(line.substring(c.start(), c.end()))){
                    if(line.substring(c.start(), c.end()).length() > 1){
                        if(VERBOSE_DEBUG){
                            System.out.println("Predicted String is (" + line.substring(c.start(), c.end()) + ") Starts: " + c.start() + " Ends:" + c.end() + " Score: " + c.score() + " Category is = " + c.type());
                        }
                        pc.put(line.substring(c.start(), c.end()), c.type());
                    }
                }
            }
        }
        
        return pc;
    }
}
