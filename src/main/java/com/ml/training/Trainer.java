/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.training;

import com.ML_Package.Muc6ChunkParser;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.config.AppConfig;
import com.ml.validation.PrintStatistics;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

/**
 *
 * @author shahzad
 */
public class Trainer {

    public static final String MODEL_PATH = "/Users/shahzad/sentiance/brightowl/data/Models";
    protected static final String PRODUCTION_MODEL_PATH = MODEL_PATH + "/inProduction";
    protected static final String TRAINING_DATA_PATH = "/Users/shahzad/sentiance/brightowl/data/TrainingDataNew";
    public static final String DIC_DATA_PATH = "/Users/shahzad/sentiance/brightowl/data/EducationSkillsLocationFuncPersonKnow.txt";

    private final PrintStatistics printer;
    private ChunkerEvaluator eval;
    private String latestModel;
    private AppConfig config;

    public Trainer() {
        printer = new PrintStatistics();
        config = AppConfig.createFromYamlSettings("local");
    }

    public void trainModel() throws ClassNotFoundException, IOException {
        File dicFile = new File(DIC_DATA_PATH);
        Parser<ObjectHandler<Chunking>> dicParser
                = new Muc6ChunkParser();

        Dictionary dic = new Dictionary(dicParser);
        Map<String, String> dicMap = dic.getDictionaryItems(dicFile, true);

        Parser<ObjectHandler<Chunking>> parser
                = new Muc6ChunkParser();

        ArrayList<File> files = new ArrayList<>();

        try (Stream<Path> paths = Files.walk(Paths.get(config.getTrainingDataLocation().replaceAll("file:", "")))) {
            paths
                    .filter(path -> Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS))
                    .forEach(f -> files.add(f.toFile()));
        }
        
        //System.out.println(files.get(3));
        if (files.size() > 0) {
            File[] corpus = (File[]) files.toArray(new File[files.size()]);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
            Date date = new Date();

            HMMModelTrainer mt = new HMMModelTrainer(parser);
            latestModel = "HMM-" + dateFormat.format(date) + ".model";
            File modelFile = new File(MODEL_PATH + File.separator + latestModel);
            mt.trainSaveAndValidateWithDic(corpus, modelFile, dicMap);
            copyModelToProduction(MODEL_PATH + File.separator + latestModel);
        }
        else{
            System.out.println("No files found for training");
        }
        // mt.trainSaveAndValidate(corpus, modelFile);
    }
    
    public void trainModelFromHDFS(int offset) throws ClassNotFoundException, IOException {
        String tPath = config.getTrainingDataLocation().replaceAll("file:", "") + File.separator + "Training-" + offset;
        File dicFile = new File(DIC_DATA_PATH);
        Parser<ObjectHandler<Chunking>> dicParser
                = new Muc6ChunkParser();

        Dictionary dic = new Dictionary(dicParser);
        Map<String, String> dicMap = dic.getDictionaryItems(dicFile, true);

        Parser<ObjectHandler<Chunking>> parser
                = new Muc6ChunkParser();

        ArrayList<File> files = new ArrayList<>();

        try (Stream<Path> paths = Files.walk(Paths.get(tPath))) {
            paths
                    .filter(path -> Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS))
                    .forEach(f -> files.add(f.toFile()));
        }
        
        //System.out.println(files.get(3));
        if (files.size() > 0) {
            File[] corpus = (File[]) files.toArray(new File[files.size()]);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
            Date date = new Date();

            HMMModelTrainer mt = new HMMModelTrainer(parser);
            latestModel = "HMM-" + dateFormat.format(date) + ".model";
            File modelFile = new File(MODEL_PATH + File.separator + latestModel);
            mt.trainSaveAndValidateWithDicFromHDFS(corpus, modelFile, dicMap);
            copyModelToProduction(MODEL_PATH + File.separator + latestModel);
        }
        else{
            System.out.println("No files found for training");
        }
        // mt.trainSaveAndValidate(corpus, modelFile);
    }

    public Map<String, String> getResultsFromModel(String modelName, String input) throws ClassNotFoundException, IOException {
        HMMModel hm = new HMMModel(MODEL_PATH + File.separator + modelName);
        return hm.getPredictedCategories(input);
    }

    public void printResultsFromModel(String modelName, String input) throws ClassNotFoundException, IOException {
        if (modelName.equals("")) {
            modelName = latestModel;
        }
        HMMModel hm = new HMMModel(MODEL_PATH + File.separator + modelName);
        hm.validate(input);
    }

    public void printResults(Map<String, String> results) {
        System.out.println("Printing the results");
        for (Entry e : results.entrySet()) {
            System.out.println("String: [" + e.getKey() + "], Category: [" + e.getValue() + "]");
        }
    }

    private void copyModelToProduction(String modelPath) throws IOException {
        Path FROM = Paths.get(modelPath);
        Path TO = Paths.get(PRODUCTION_MODEL_PATH + File.separator + "HMMModel.model");
        //overwrite existing file, if exists
        CopyOption[] options = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES
        };
        Files.copy(FROM, TO, options);
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        //File refFile = new File("/Users/shahzad/sentiance/brightowl/data/TrainingData/TrainingNew.txt");
        //File refFile1 = new File("/Users/shahzad/sentiance/brightowl/data/TrainingData/TrainingNew1.txt");
        Trainer trainer = new Trainer();
        //trainer.trainModel();
        trainer.trainModelFromHDFS(30000);
        String text = "Territory# 61 K03 - Baltimore S, MD ( Red Team) Covering: Greenbelt, Waldorf, Clinton, La Plata, Bowie, \n"
                + "Annapolis, Prince Frederick, Leonardtown No relocation assistance is provided for this position. Amgen\n"
                + "therapeutics have changed the practice of medicine, helping millions of people around the world in the fight against\n"
                + "cancer, cardiovascular disease, kidney disease, rheumatoid arthritis, and other serious illnesses. With a broad and\n"
                + "deep pipeline of potential new medicines, Amgen remains committed to advancing science to dramatically improve\n"
                + "people's lives. The Sr Specialty Rep, acts as the primary customer contact for demand creation by executing\n"
                + "marketing strategy and promoting Amgen products as lead by the District Manager. In this strategic role, the Sr \n"
                + "Specialty Rep provides current and comprehensive clinical knowledge of Amgen's products and effectively\n"
                + "communicates the clinical and economic benefits of the products. As a sales leader, the Sr Specialty Rep is\n"
                + "expected to achieve territory sales by executing Plan of Action (POA) marketing strategies, which includes\n"
                + "delivering branded sales messages, executing planned programs, scheduling and following-up with medical educational\n"
                + "programs, and achieving or exceeding sales targets. Sr Specialty Representatives are also responsible for\n"
                + "servicing and managing accounts, which includes ensuring product access, resolving/triage reimbursement issues, and\n"
                + "maintaining product contracts. Additional responsibilities involve providing feedback on marketing strategy and\n"
                + "effectiveness reviews of sales activities and territory analysis, in addition to developing territory plans with your\n"
                + " District Manager Basic Qualifications: Masters or Bachelor's Degree and 2 years of Sales experience OR\n"
                + "Associate's degree and 6 years of Sales experience OR High school diploma/GED and 8 years of Sales experience\n"
                + "Preferred Qualifications: A Bachelor's degree in Life Sciences or Business Administration2+ years of sales\n"
                + "and/or marketing experience within pharmaceutical, biotech, healthcare, or medical device industries Product or\n"
                + "hospital sales experience in the areas of cardiovascular, oncology, nephrology, dermatology, rheumatology and\n"
                + "inflammation Neurology, endocrinology, hematology, gastroenterology, or infectious diseases, and the diseases and\n"
                + "treatments involved with these specialties Local Market knowledge Core Competencies: Planning and Organizing \n"
                + "Work, Engaging Others through Effective Communication, Drive to Achieve, Individual Leadership and Impact\n"
                + "Amgen is committed to unlocking the potential of biology for patients suffering from serious illnesses by\n"
                + "discovering, developing, manufacturing and delivering innovative human therapeutics. This approach begins by using\n"
                + "tools like advanced human genetics to unravel the complexities of disease and understand the fundamentals of human\n"
                + "biology. Amgen focuses on areas of high unmet medical need and leverages its expertise to strive for solutions that\n"
                + "improve health outcomes and dramatically improve people’s lives. A biotechnology pioneer since 1980, Amgen has\n"
                + "grown to be one of the world’s leading independent biotechnology companies, has reached millions of patients around\n"
                + "the world and is developing a pipeline of medicines with breakaway potential. Amgen is an Equal Opportunity\n"
                + "employer and will consider all qualified applicants for employment without regard to race, color, religion, sex,\n"
                + "sexual orientation, gender identity, national origin, protected veteran status, or disability status.";
        trainer.printResultsFromModel("", text);

    }
}
