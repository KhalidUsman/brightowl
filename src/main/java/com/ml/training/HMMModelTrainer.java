/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.training;

import com.ML_Package.Muc6ChunkParser;
import com.aliasi.chunk.CharLmRescoringChunker;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;
import com.ml.validation.ChunkingCollector;
import com.ml.validation.PrintStatistics;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.xml.sax.InputSource;

/**
 *
 * @author shahzad
 */
public class HMMModelTrainer implements Serializable{

    static final int NUM_FOLDS = 10;

    static int HMM_N_GRAM_LENGTH = 8;
    static int NUM_CHARS = 256;
    static double HMM_INTERPOLATION_RATIO = 8;
    static int NUM_ANALYSES_RESCORED = 50;

    private final Parser<ObjectHandler<Chunking>> mParser;
    private final PrintStatistics printer;
    

    public HMMModelTrainer(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
        printer = new PrintStatistics();
    }
    
    

    public void trainSaveAndValidate(File[] corpusFiles, File modelFile) throws ClassNotFoundException, IOException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        for (int i = 0; i < corpusFiles.length; ++i) {
            mParser.parse(corpusFiles[i]);
        }

        List<Chunking> files = refCollector.mChunkingList;
        
        int numSentences = files.size();
        System.out.println("Total files found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
        //uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
        /**
         * tokenizerFactory - Tokenizer factory for boundaries.
         * numChunkingsRescored - Number of underlying chunkings rescored. nGram
         * - N-gram length for all models. numChars - Number of characters in
         * the training and run-time character sets. interpolationRatio -
         * Underlying language-model interpolation ratios.
         */
        CharLmRescoringChunker chunker
                = new CharLmRescoringChunker(tokenizerFactory,
                        NUM_ANALYSES_RESCORED,
                        HMM_N_GRAM_LENGTH,
                        NUM_CHARS,
                        HMM_INTERPOLATION_RATIO,
                        true);

        System.out.println("training labeled data");
        for (int i = 0; i < files.size(); ++i) {
            chunker.handle(files.get(i));
        }

//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }
        System.out.println("Compiling and Writing Model to File=" + modelFile);
        AbstractExternalizable.compileTo(chunker, modelFile);
        System.out.println("Model Created");
        Chunker compiledChunker = (Chunker) AbstractExternalizable.readObject(modelFile);

        foldEvaluator.setChunker(compiledChunker);

        System.out.println("evaluating");
        for (int i = 0; i < files.size(); ++i) {
            try {
                foldEvaluator.handle(files.get(i));
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println(files.get(i));
            }
        }
        printer.printEval("X-VAL", foldEvaluator);
    }
    
    public void trainSaveAndValidateWithDic(File[] corpusFiles, File modelFile, Map<String, String> dicMap) throws ClassNotFoundException, IOException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        
        for (int i = 0; i < corpusFiles.length; ++i) {
//            InputStream inputStream= new FileInputStream(corpusFiles[i]);
//            Reader reader = new InputStreamReader(inputStream,"UTF-8");
//            InputSource is = new InputSource(reader);
//            is.setEncoding("UTF-8");
            if (corpusFiles[i].isFile() && corpusFiles[i].getName().endsWith(".txt")) {
                mParser.parse(corpusFiles[i]);
            }
        }

        List<Chunking> files = refCollector.mChunkingList;
        
        int numSentences = files.size();
        System.out.println("Total files found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
        //uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
        /**
         * tokenizerFactory - Tokenizer factory for boundaries.
         * numChunkingsRescored - Number of underlying chunkings rescored. nGram
         * - N-gram length for all models. numChars - Number of characters in
         * the training and run-time character sets. interpolationRatio -
         * Underlying language-model interpolation ratios.
         */
        CharLmRescoringChunker chunker
                = new CharLmRescoringChunker(tokenizerFactory,
                        NUM_ANALYSES_RESCORED,
                        HMM_N_GRAM_LENGTH,
                        NUM_CHARS,
                        HMM_INTERPOLATION_RATIO,
                        true);

        System.out.println("training labeled data");
        for (int i = 0; i < files.size(); ++i) {
            chunker.handle(files.get(i));
        }
        
        dicMap.entrySet().forEach((e) -> {
            chunker.trainDictionary(e.getKey(),e.getValue());
        });

//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }
        System.out.println("Compiling and Writing Model to File=" + modelFile);
        AbstractExternalizable.compileTo(chunker, modelFile);
        System.out.println("Model Created");
        Chunker compiledChunker = (Chunker) AbstractExternalizable.readObject(modelFile);

        foldEvaluator.setChunker(compiledChunker);

        System.out.println("evaluating");
        for (int i = 0; i < files.size(); ++i) {
            try {
                foldEvaluator.handle(files.get(i));
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println(files.get(i));
            }
        }
        printer.printEval("X-VAL", foldEvaluator);
    }
    
    public void trainSaveAndValidateWithDicFromHDFS(File[] corpusFiles, File modelFile, Map<String, String> dicMap) throws ClassNotFoundException, IOException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        
        for (int i = 0; i < corpusFiles.length; ++i) {
//            InputStream inputStream= new FileInputStream(corpusFiles[i]);
//            Reader reader = new InputStreamReader(inputStream,"UTF-8");
//            InputSource is = new InputSource(reader);
//            is.setEncoding("UTF-8");
            if (corpusFiles[i].isFile() && (!corpusFiles[i].getName().endsWith(".crc")) && !corpusFiles[i].getName().equals("_SUCCESS")) {
                mParser.parse(corpusFiles[i]);
            }
        }

        List<Chunking> files = refCollector.mChunkingList;
        
        int numSentences = files.size();
        System.out.println("Total files found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
        //uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
        /**
         * tokenizerFactory - Tokenizer factory for boundaries.
         * numChunkingsRescored - Number of underlying chunkings rescored. nGram
         * - N-gram length for all models. numChars - Number of characters in
         * the training and run-time character sets. interpolationRatio -
         * Underlying language-model interpolation ratios.
         */
        CharLmRescoringChunker chunker
                = new CharLmRescoringChunker(tokenizerFactory,
                        NUM_ANALYSES_RESCORED,
                        HMM_N_GRAM_LENGTH,
                        NUM_CHARS,
                        HMM_INTERPOLATION_RATIO,
                        true);

        System.out.println("training labeled data");
        for (int i = 0; i < files.size(); ++i) {
            chunker.handle(files.get(i));
        }
        
        dicMap.entrySet().forEach((e) -> {
            chunker.trainDictionary(e.getKey(),e.getValue());
        });

//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }
        System.out.println("Compiling and Writing Model to File=" + modelFile);
        AbstractExternalizable.compileTo(chunker, modelFile);
        System.out.println("Model Created");
//        Chunker compiledChunker = (Chunker) AbstractExternalizable.readObject(modelFile);
//
//        foldEvaluator.setChunker(compiledChunker);
//
//        System.out.println("evaluating");
//        for (int i = 0; i < files.size(); ++i) {
//            try {
//                foldEvaluator.handle(files.get(i));
//            } catch (Exception e) {
//                e.printStackTrace(System.out);
//                System.out.println(files.get(i));
//            }
//        }
//        printer.printEval("X-VAL", foldEvaluator);
    }
}
