/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.training;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import org.apache.tika.exception.TikaException;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.txt.TXTParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author shahzad
 */
public class TextPreProcessor {

    /**
     * 
     * @param input
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws SAXException
     * @throws TikaException 
     */
    public static String normalizeText(String input) throws UnsupportedEncodingException, IOException, SAXException, TikaException {        
        InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8.name()));
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        TXTParser TexTParser = new TXTParser();
        ParseContext pcontext = new ParseContext();
        TexTParser.parse(stream, handler, metadata, pcontext);        
        String normalizedString = normalizeStringToCapitalLetter(handler.toString());
        return normalizedString.replaceAll("OR", " OR ").replaceAll("AND", " AND ").replaceAll("  ", " ").replaceAll("<[^>]*>", " ").replaceAll("<", " ").replaceAll(">", " ").replaceAll("\\s+", " ").replaceAll("([\\ud800-\\udbff\\udc00-\\udfff])", "").replaceAll(";", "").replaceAll(":", "").replaceAll("•", "").replaceAll("[()]", "").trim();   
    }

    /**
     * 
     * @param input
     * @return 
     */
    public static String getTextLanguage(String input) {
        LanguageIdentifier identifier = new LanguageIdentifier(input);
        return identifier.getLanguage();
    }

    /**
     * 
     * @param input
     * @return 
     */
    public static String normalizeStringToCapitalLetter(String input) {

        StringBuilder normalized = new StringBuilder();
        boolean lastCharCaps = false;
        for (int y = 0; y < input.length(); y++) {
            if (y > 0 && Character.isUpperCase(input.charAt(y)) && (!Character.isUpperCase(input.charAt(y + 1)))) {
                if (!Character.isUpperCase(input.charAt(y - 1))) {
                    normalized.append(" ");
                }
            }
            normalized.append(input.charAt(y));
        }
        return normalized.toString();
    }
}
