/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.training;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.ml.validation.ChunkingCollector;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author shahzad
 */
public class Dictionary implements Serializable{

    private final Parser<ObjectHandler<Chunking>> mParser;
    private final static int DIC_THRESH_HOLD = 500;

    public Dictionary(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
    }

    public Map<String, String> getDictionaryItems(File dicFile, boolean cutOff) throws IOException {

        Map<String, String> dic = new HashMap<>();
        Map<String, Integer> dicCount = new HashMap<>();
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        mParser.parse(dicFile);

        List<Chunking> cl = refCollector.mChunkingList;

        for (Chunking ck : cl) {
            String line = (String) ck.charSequence();
            Set<Chunk> cs = ck.chunkSet();
            for (Chunk c : cs) {
                if (!dicCount.containsKey(c.type())) {
                    dicCount.put(c.type(), 0);
                }
                int count = dicCount.get(c.type());
                if (cutOff) {
                    if (count <= DIC_THRESH_HOLD) {
                        dic.put(line.substring(c.start(), c.end()), c.type());
                        dicCount.put(c.type(), ++count);
                    }
                } else {
                    dic.put(line.substring(c.start(), c.end()), c.type());
                    dicCount.put(c.type(), ++count);
                }
            }
        }
        //System.out.println("Total dic items: " + dic.size());
        return dic;
    }
    
    public Set<String> getNGramSpecificTypeFromDic(File dicFile, String type, int gram) throws IOException {
        Set<String> dic = new HashSet<>();
        Map<String, String> dicMap = getDictionaryItems(dicFile, false);
        final String gramString;
        if(gram > 4){
            gramString = (gram -1) + ",";
        }
        else{
            gramString = "" + (gram -1);
        }
        
        dicMap.entrySet().forEach((e) -> {
            if (e.getValue().equals(type)) {
                // Only consider the words based on provided gram(s) as input
                if (e.getKey().matches("^ *[a-zA-Z0-9_\\(\\)\\!\\'.\\\"\\&\\[\\]\\’]+( +[a-zA-Z0-9_\\(\\)\\!\\'.\\\"\\&\\[\\]\\’]+){"+gramString+"}$")) {
                    dic.add(e.getKey());
                }
            }
        });
        return dic;
    }

    public Set<String> getSpecificTypeFromDic(File dicFile, String type) throws IOException {
        Set<String> dic = new HashSet<>();
        Map<String, String> dicMap = getDictionaryItems(dicFile, false);
        dicMap.entrySet().forEach((e) -> {
            if (e.getValue().equals(type)) {
                if (e.getKey().matches(".*[A-Za-z] [A-Za-z].*")) {
                    dic.add(e.getKey());
                }
            }

        });
        return dic;
    }
}
