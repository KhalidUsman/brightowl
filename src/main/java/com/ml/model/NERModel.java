/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.model;

import com.config.AppConfig;
import com.ml.training.HMMModel;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author shahzad
 */
public class NERModel {

    private static NERModel ner;

    private static HMMModel hmm;

    /**
     * 
     * @param env
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    private NERModel(String env) throws IOException, ClassNotFoundException {
        AppConfig config = AppConfig.createFromYamlSettings(env);        
        hmm = new HMMModel(config.getProjectModelPath());
    }


    /**
     * Create a static method to get instance.
     * @param env
     * @return 
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public static NERModel getInstance(String env) throws IOException, ClassNotFoundException {
        if (ner == null) {
            ner = new NERModel(env);
        }
        return ner;
    }

    /**
     * 
     * @param input
     * @return Map<String, String>
     */
    public Map<String, String> predictResults(String input) {
        return hmm.getPredictedCategories(input);
    }
}
