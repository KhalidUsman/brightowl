/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.aliasi.chunk.CharLmRescoringChunker;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import org.xml.sax.InputSource;

/**
 *
 * @author shahzad
 */
public class KFoldValidation {
    
    private final Parser<ObjectHandler<Chunking>> mParser;
    private final PrintStatistics printer;
    
    public KFoldValidation(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
        printer = new PrintStatistics();
    }
    
    static final int NUM_FOLDS = 10;

    static int HMM_N_GRAM_LENGTH = 8;
    static int NUM_CHARS = 256;
    static double HMM_INTERPOLATION_RATIO = 8;
    static int NUM_ANALYSES_RESCORED = 10;
    
    public void validate(File refFile) throws IOException, ClassNotFoundException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        mParser.parse(refFile);
        List<Chunking> sentences = refCollector.mChunkingList;
        int numSentences = sentences.size();
        System.out.println("Total sentences found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;
        
        ChunkerEvaluator evaluator
                = new ChunkerEvaluator(null); // set chunker in loop
        evaluator.setMaxConfidenceChunks(1);
        evaluator.setMaxNBest(1);
        evaluator.setVerbose(false);

        for (int fold = NUM_FOLDS; --fold >= 0;) {
            int startTestFold = fold * numSentences / NUM_FOLDS;
            int endTestFold = (fold + 1) * numSentences / NUM_FOLDS;
            if (fold == NUM_FOLDS - 1) {
                endTestFold = numSentences; // round up on last fold
            }
            System.out.println("-----------------------------------------------");
            System.out.printf("FOLD=%2d  start sent=%5d  end sent=%5d\n",
                    fold, startTestFold, endTestFold);

            ChunkerEvaluator foldEvaluator
                    = new ChunkerEvaluator(null);
            foldEvaluator.setMaxConfidenceChunks(1);
            foldEvaluator.setMaxNBest(1);
            foldEvaluator.setVerbose(false);

            // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
            // uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
            CharLmRescoringChunker chunker
                    = new CharLmRescoringChunker(tokenizerFactory,
                            NUM_ANALYSES_RESCORED,
                            HMM_N_GRAM_LENGTH,
                            NUM_CHARS,
                            HMM_INTERPOLATION_RATIO,
                            true);

            System.out.println("training labeled data");
            for (int i = 0; i < startTestFold; ++i) {
                chunker.handle(sentences.get(i));
            }
            for (int i = endTestFold; i < numSentences; ++i) {
                chunker.handle(sentences.get(i));
            }
//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }

            System.out.println("compiling");
            Chunker compiledChunker
                    = (Chunker) AbstractExternalizable.compile(chunker);

            foldEvaluator.setChunker(compiledChunker);
            evaluator.setChunker(compiledChunker);

            System.out.println("evaluating");
            for (int i = startTestFold; i < endTestFold; ++i) {
                try {
                    foldEvaluator.handle(sentences.get(i));
                    evaluator.handle(sentences.get(i));
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    System.out.println(sentences.get(i));
                }
            }
            printer.printEval("FOLD=" + fold, foldEvaluator);
        }
        System.out.println("\n===============================================");
        System.out.println("COMBINED CROSS-VALIDATION RESULTS");
        printer.printEval("X-VAL" , evaluator);
    }
    
    public void validateFromMultipleFiles(File[] corpusFiles) throws IOException, ClassNotFoundException {
        System.out.println("Started the validation without Dictionary");
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        for (int i = 0; i < corpusFiles.length; ++i) {
            if (corpusFiles[i].isFile() && (!corpusFiles[i].getName().endsWith(".crc")) && !corpusFiles[i].getName().equals("_SUCCESS")) {
            
//            InputStream inputStream= new FileInputStream(corpusFiles[i]);
//            Reader reader = new InputStreamReader(inputStream,"UTF-8");
//            InputSource is = new InputSource(reader);
//            is.setEncoding("UTF-8");
                mParser.parse(corpusFiles[i]);
            }
        }
        List<Chunking> sentences = refCollector.mChunkingList;
        int numSentences = sentences.size();
        System.out.println("Total sentences found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;
        
        ChunkerEvaluator evaluator
                = new ChunkerEvaluator(null); // set chunker in loop
        evaluator.setMaxConfidenceChunks(1);
        evaluator.setMaxNBest(1);
        evaluator.setVerbose(false);

        for (int fold = NUM_FOLDS; --fold >= 0;) {
            int startTestFold = fold * numSentences / NUM_FOLDS;
            int endTestFold = (fold + 1) * numSentences / NUM_FOLDS;
            if (fold == NUM_FOLDS - 1) {
                endTestFold = numSentences; // round up on last fold
            }
            System.out.println("-----------------------------------------------");
            System.out.printf("FOLD=%2d  start sent=%5d  end sent=%5d\n",
                    fold, startTestFold, endTestFold);

            ChunkerEvaluator foldEvaluator
                    = new ChunkerEvaluator(null);
            foldEvaluator.setMaxConfidenceChunks(1);
            foldEvaluator.setMaxNBest(1);
            foldEvaluator.setVerbose(false);

            // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
            // uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
            CharLmRescoringChunker chunker
                    = new CharLmRescoringChunker(tokenizerFactory,
                            NUM_ANALYSES_RESCORED,
                            HMM_N_GRAM_LENGTH,
                            NUM_CHARS,
                            HMM_INTERPOLATION_RATIO,
                            true);

            System.out.println("training labeled data");
            for (int i = 0; i < startTestFold; ++i) {
                chunker.handle(sentences.get(i));
            }
            for (int i = endTestFold; i < numSentences; ++i) {
                chunker.handle(sentences.get(i));
            }
//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }

            System.out.println("compiling");
            Chunker compiledChunker
                    = (Chunker) AbstractExternalizable.compile(chunker);

            foldEvaluator.setChunker(compiledChunker);
            evaluator.setChunker(compiledChunker);

            System.out.println("evaluating");
            for (int i = startTestFold; i < endTestFold; ++i) {
                try {
                    foldEvaluator.handle(sentences.get(i));
                    evaluator.handle(sentences.get(i));
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    System.out.println(sentences.get(i));
                }
            }
            printer.printEval("FOLD=" + fold, foldEvaluator);
        }
        System.out.println("\n===============================================");
        System.out.println("COMBINED CROSS-VALIDATION RESULTS");
        printer.printEval("X-VAL" , evaluator);
    }
    
    public void validateFromMultipleFilesWithDic(File[] corpusFiles, Map<String, String> dicMap) throws IOException, ClassNotFoundException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        for (int i = 0; i < corpusFiles.length; ++i) {
            if (corpusFiles[i].isFile() && (!corpusFiles[i].getName().endsWith(".crc")) && !corpusFiles[i].getName().equals("_SUCCESS")) {
            
//            InputStream inputStream= new FileInputStream(corpusFiles[i]);
//            Reader reader = new InputStreamReader(inputStream,"UTF-8");
//            InputSource is = new InputSource(reader);
//            is.setEncoding("UTF-8");
                mParser.parse(corpusFiles[i]);
            }
        }
        List<Chunking> sentences = refCollector.mChunkingList;
        int numSentences = sentences.size();
        System.out.println("Total sentences found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;
        
        ChunkerEvaluator evaluator
                = new ChunkerEvaluator(null); // set chunker in loop
        evaluator.setMaxConfidenceChunks(1);
        evaluator.setMaxNBest(1);
        evaluator.setVerbose(false);

        for (int fold = NUM_FOLDS; --fold >= 0;) {
            int startTestFold = fold * numSentences / NUM_FOLDS;
            int endTestFold = (fold + 1) * numSentences / NUM_FOLDS;
            if (fold == NUM_FOLDS - 1) {
                endTestFold = numSentences; // round up on last fold
            }
            System.out.println("-----------------------------------------------");
            System.out.printf("FOLD=%2d  start sent=%5d  end sent=%5d\n",
                    fold, startTestFold, endTestFold);

            ChunkerEvaluator foldEvaluator
                    = new ChunkerEvaluator(null);
            foldEvaluator.setMaxConfidenceChunks(1);
            foldEvaluator.setMaxNBest(1);
            foldEvaluator.setVerbose(false);

            // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
            // uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
            CharLmRescoringChunker chunker
                    = new CharLmRescoringChunker(tokenizerFactory,
                            NUM_ANALYSES_RESCORED,
                            HMM_N_GRAM_LENGTH,
                            NUM_CHARS,
                            HMM_INTERPOLATION_RATIO,
                            true);

            System.out.println("training labeled data");
            for (int i = 0; i < startTestFold; ++i) {
                chunker.handle(sentences.get(i));
            }
            for (int i = endTestFold; i < numSentences; ++i) {
                chunker.handle(sentences.get(i));
            }
            
            dicMap.entrySet().forEach((e) -> {
                chunker.trainDictionary(e.getKey(), e.getValue());
            });
//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }

            System.out.println("compiling");
            Chunker compiledChunker
                    = (Chunker) AbstractExternalizable.compile(chunker);

            foldEvaluator.setChunker(compiledChunker);
            evaluator.setChunker(compiledChunker);

            System.out.println("evaluating");
            for (int i = startTestFold; i < endTestFold; ++i) {
                try {
                    foldEvaluator.handle(sentences.get(i));
                    evaluator.handle(sentences.get(i));
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    System.out.println(sentences.get(i));
                }
            }
            printer.printEval("FOLD=" + fold, foldEvaluator);
        }
        System.out.println("\n===============================================");
        System.out.println("COMBINED CROSS-VALIDATION RESULTS");
        printer.printEval("X-VAL" , evaluator);
    }
}
