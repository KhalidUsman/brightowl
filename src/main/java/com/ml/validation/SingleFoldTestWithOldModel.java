/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.ML_Package.NER_CrossValidation;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.aliasi.util.AbstractExternalizable;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author shahzad
 */
public class SingleFoldTestWithOldModel {
    
    private final Parser<ObjectHandler<Chunking>> mParser;
    private final PrintStatistics printer;
    
    public SingleFoldTestWithOldModel(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
        printer = new PrintStatistics();
    }
    
    public ChunkerEvaluator validate(File refFile) throws IOException, ClassNotFoundException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        mParser.parse(refFile);
        List<Chunking> refChunkings = refCollector.mChunkingList;

        System.out.println("Total sentences found " + refChunkings.size());

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        File modelFile = new File("src/main/resources/EducationSkillsLocationFuncPersonKnow.model");
        Chunker compiledChunker = (Chunker) AbstractExternalizable.readObject(modelFile);

        foldEvaluator.setChunker(compiledChunker);

        System.out.println("evaluating");
        for (int i = 0; i < refChunkings.size(); ++i) {
            try {
                foldEvaluator.handle(refChunkings.get(i));
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println(refChunkings.get(i));
            }
        }
        //System.out.println(foldEvaluator.toString());
        printer.printEval("X-VAL", foldEvaluator);
        return foldEvaluator;

    }
}
