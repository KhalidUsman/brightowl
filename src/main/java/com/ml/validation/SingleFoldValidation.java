/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.Keyword_Extraction.Rake;
import com.ML_Package.NER_CrossValidation;
import com.aliasi.chunk.CharLmHmmChunker;
import com.aliasi.chunk.CharLmRescoringChunker;
import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.aliasi.hmm.HmmCharLmEstimator;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.ScoredObject;
import com.aliasi.util.Strings;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author shahzad
 */
public class SingleFoldValidation {

    static final int NUM_FOLDS = 10;

    static int HMM_N_GRAM_LENGTH = 8;
    static int NUM_CHARS = 256;
    static double HMM_INTERPOLATION_RATIO = 8;
    static int NUM_ANALYSES_RESCORED = 50;

    private final Parser<ObjectHandler<Chunking>> mParser;
    private final PrintStatistics printer;

    public SingleFoldValidation(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
        printer = new PrintStatistics();
    }

    public void validate(File[] refFile) throws ClassNotFoundException, IOException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        for (int i = 0; i < refFile.length; ++i) {
             mParser.parse(refFile[i]);
        }
       
        List<Chunking> sentences = refCollector.mChunkingList;
        int numSentences = sentences.size();
        System.out.println("Total sentences found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
        //uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
        /**
         * tokenizerFactory - Tokenizer factory for boundaries.
         * numChunkingsRescored - Number of underlying chunkings rescored. nGram
         * - N-gram length for all models. numChars - Number of characters in
         * the training and run-time character sets. interpolationRatio -
         * Underlying language-model interpolation ratios.
         */
        CharLmRescoringChunker chunker
                = new CharLmRescoringChunker(tokenizerFactory,
                        NUM_ANALYSES_RESCORED,
                        HMM_N_GRAM_LENGTH,
                        NUM_CHARS,
                        HMM_INTERPOLATION_RATIO,
                        true);

        System.out.println("training labeled data");
        for (int i = 0; i < sentences.size(); ++i) {
            chunker.handle(sentences.get(i));
        }

//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }
        System.out.println("compiling");
        Chunker compiledChunker
                = (Chunker) AbstractExternalizable.compile(chunker);

        foldEvaluator.setChunker(compiledChunker);

        System.out.println("evaluating");
        for (int i = 0; i < sentences.size(); ++i) {
            try {
                foldEvaluator.handle(sentences.get(i));
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println(sentences.get(i));
            }
        }
        printer.printEval("X-VAL", foldEvaluator);

String input = "Advanced Bionics is a global leader in developing the most advanced cochlear implant systems in the world. Always\n" +
"focused on innovations that improve quality of life, AB has consistently made the industry and rsquo;s leading\n" +
"advancements in hearing technology. In fact, with six sound coding programs, we offer more ways to hear than any\n" +
"other cochlear implant company.Our company is part of the Sonova Group, the leading provider of hearing healthcare\n" +
"solutions and parent company of Phonak, the leading hearing instrument brand.As an AB employee, you will contribute\n" +
"to advancing biomedical technology worldwide and work on challenging projects with smart, driven, friendly people.AB\n" +
"provides thriving work environments around the globe; each fosters creativity, teamwork, global competitiveness, and\n" +
"professional growth.Experience the professional and personal fulfillment that comes with making a difference in\n" +
"people and rsquo;s lives. Apply to Advanced Bionics today.  				  			  			  		 \n" +
"\n" +
"Advanced Bionics / USA / Valencia (CA) Intern Systems Engineer \n" +
"\n" +
"Your tasks 				  			 \n" +
"\n" +
"TASKS  and  RESPONSIBILITES:Develop hardware and/or software infrastructure to perform product evaluation, debugging and\n" +
"algorithm development.Conduct feasibility work on product / system components and features in area of technical\n" +
"expertise.Study and be able to propose suitable components, algorithms and solutions at the least in the area of\n" +
"expertise.Work with Electrical (RF, Board  and  IC), Firmware, Software teams during the product development and\n" +
"system integration phase. \n" +
" 							  			  		  		  		 \n" +
"Your profile  					 \n" +
"				  				  					  			  			 \n" +
"SPECIAL SKILLS  and  KNOWLEDGE: Completed or pursuing Master and rsquo;s or\n" +
"Doctoral Degree in Electrical or Electronics Engineering.Strong hands-on technical ability in at least two or more of\n" +
"the following: Analog, Digital Circuits, Embedded systems, DSP, RF, ICs, Programming, Lab instruments. Should be\n" +
"competent with the relevant tools in the area of expertise (MATLAB, Embedded tools, SPICE, VHDL/Verilog, System C, C,\n" +
"C++, Python, etc.).Individuals with strong analog/digital circuit theory and good programming experience are\n" +
"preferred.Should enjoy working in team while demonstrating the initiative and ability to take on and solve problems\n" +
"outside of the individual and rsquo;s area of technical expertise.Capable of working independently with high level\n" +
"input and limited guidance.Greater than 3.5 GPA and demonstrated technical writing skills. \n" +
"							  			  		  		  		  		  		  					 \n" +
"Advanced Bionics is committed to creating a diverse environment and is proud to be\n" +
"an affirmative action/ equal opportunity employer. All qualified applicants will receive consideration for employment\n" +
"without regard to race, color, religion, gender, gender identity or expression, sexual orientation, national origin,\n" +
"genetics, disability, age, or veteran status.  				  			  							 \n" +
"Advanced Bionics LLC28515\n" +
"Westinghouse PlaceValencia, CA 91355 +1 661 362 4747";
            input = input.replaceAll( "([\\ud800-\\udbff\\udc00-\\udfff])", "");
            input = input.replace("B. sc.", "B.Sc");
            input = input.replace("B. Sc.", "B.Sc");
            input = input.replace("B. s. c.", "B.Sc");
            input = input.replace("M. sc.", "M.sc");
            input = input.replace("M. s. c.", "M.Sc");
            input = input.replace("M. Sc.", "M.Sc");
            input = input.replace("P. hd.", "P.hd");
            input = input.replace("P. Hd.", "P.hd");
            input = input.replace("P. h. d.", "P.hc");
            String[] lines = input.split("(?<=[.!?]) +");
            
            Rake rakeObj = new Rake();
            String crawlFeatures[] = rakeObj.run(input);
            
        
        //String [] lines = sections;
       
        for (final String line : lines) {
            Chunking chunking = chunker.chunk(line);
            Set<Chunk> test = chunking.chunkSet();
            for (Chunk c : test) {
                
                System.out.println("Test String is (" + line.substring(c.start(), c.end()) + ") Starts: "+ c.start() + " Ends:" + c.end() + " Score: " + c.score() + " Category is = " + c.type());
            }
        }

    }
}
