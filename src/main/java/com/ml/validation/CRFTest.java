/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.ML_Package.Muc6ChunkParser;
import com.aliasi.chunk.BioTagChunkCodec;
import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.chunk.IoTagChunkCodec;
import com.aliasi.chunk.TagChunkCodec;
import com.aliasi.corpus.Corpus;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.aliasi.crf.ChainCrfChunker;
import com.aliasi.crf.ChainCrfFeatureExtractor;
import com.aliasi.io.LogLevel;
import com.aliasi.io.Reporter;
import com.aliasi.io.Reporters;
import com.aliasi.stats.AnnealingSchedule;
import com.aliasi.stats.RegressionPrior;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.ScoredObject;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author shahzad
 */
public class CRFTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //File refFile = new File("src/main/resources/testSkillRaw.txt");
        //File refFile = new File("/Users/shahzad/sentiance/brightowl/data/EducationSkillsLocationFuncPersonKnow.txt");
        File refFile = new File("/Users/shahzad/sentiance/brightowl/data/TrainingNew.txt");
        
        Parser<ObjectHandler<Chunking>> mParser
                = new Muc6ChunkParser();
        
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);

        mParser.parse(refFile);
        
        List<Chunking> sentences = refCollector.mChunkingList;
        
        Corpus<ObjectHandler<Chunking>> corpus
            = new M6ToCorpaCorpus(sentences);

        TokenizerFactory tokenizerFactory
            = IndoEuropeanTokenizerFactory.INSTANCE;
        boolean enforceConsistency = true;
        TagChunkCodec tagChunkCodec
            = new BioTagChunkCodec(tokenizerFactory,
                                   enforceConsistency);

        ChainCrfFeatureExtractor<String> featureExtractor
            = new SimpleChainCrfFeatureExtractor();

        int minFeatureCount = 1;

        boolean cacheFeatures = true;

        boolean addIntercept = true;

        double priorVariance = 2.0;
        boolean uninformativeIntercept = true;
        RegressionPrior prior
            = RegressionPrior.gaussian(priorVariance,
                                       uninformativeIntercept);
        int priorBlockSize = 50;

        double initialLearningRate = 0.05;
        double learningRateDecay = 0.995;
        AnnealingSchedule annealingSchedule
            = AnnealingSchedule.exponential(initialLearningRate,
                                            learningRateDecay);

        double minImprovement = 0.00001;
        int minEpochs = 10;
        int maxEpochs = 5000;

        Reporter reporter
            = Reporters.stdOut().setLevel(LogLevel.DEBUG);

        System.out.println("\nEstimating");
        ChainCrfChunker crfChunker
            = ChainCrfChunker.estimate(corpus,
                                       tagChunkCodec,
                                       tokenizerFactory,
                                       featureExtractor,
                                       addIntercept,
                                       minFeatureCount,
                                       cacheFeatures,
                                       prior,
                                       priorBlockSize,
                                       annealingSchedule,
                                       minImprovement,
                                       minEpochs,
                                       maxEpochs,
                                       reporter);
//
//         File modelFile = new File("/Users/shahzad/sentiance/brightowl/data/crfChunker4(Block50-feature1).model");
//        System.out.println("\nCompiling to file=" + modelFile);
//        AbstractExternalizable.serializeTo(crfChunker,modelFile);

//ChainCrfChunker crfChunker
//            = (ChainCrfChunker) 
//            AbstractExternalizable.readObject(modelFile);

            String input = "We are a clinical-stage biopharmaceutical company that is creating and developing a pipeline of differentiated and antibodies therapeutics. Our unique and  platform and and  proprietary and  Fc and  engineering and  technologies and combined with the complementary expertise of our people enabled us to build a clinical-stage portfolio of novel and product and  candidates in oncology and  and and  auto-immune and  diseases – tailored and  from and  discovery and  through and  development and  to and address patient needs. and We and  are and  currently and  looking and  for several Research and  Associates/Senior and  Research and  Associates with and  at and  least and  2 and  years and  of and working experience in the laboratory to join our Research and Discovery teams.The candidate will be an active member of a project team working closely with colleagues and external partners and to and  discover and  and and  develop and  therapeutic and  antibodies. The and  candidate and  should and  have and  a and  drive and  to and  generate and and  report and qualitative results together and in tight collaboration with other members of the project team in a professional and and and  challenging and  fashion. and  This and  all and  happens and  in and  a and  dynamic, and  informal and  and and  stimulating and  environment and  where and  you and  and and your colleagues work to reach the set goals. Every employee gets the opportunity to get involved in all the various and steps within her/his project. and Profile: and Degree B. sc. or M. Sc. (Industrial Engineer), Master in Biochemistry/ Biotechnology or equivalent and More than 2 years of working experience in laboratory, preferably in an industry setting  and Knowledge and  and hands and  on and  experience and  in molecular and  biology, protein and  related and  work, and  cell and  based or and biochemical assays is a requirement and Keen to learn new technics and to share expertise with others and Team player, enthusiastic, accurate in execution and reporting and Can find a solution to any unexpected problem in a creative way and Good communication skills and Sense for initiative, quality, accuracy and detail and Good writing skills in English and Job description: and Responsibilities: and Discover, characterize and optimize therapeutic antibodies and Plan and execute experiments independently including interpretation and reporting of results and Actively and  participate and  in and  project and  teams, and  organize and  work and understand and  priorities as and  well and  as completes assignments accordingly and Fulfill general lab supporting activities and Technical expertise area: and Molecular and cellular biology and Protein expression and purifications and Biochemical and cellular assays and Offer: and Full time position in a collegial, dynamic and growing company and You will become part of a team that allows you to access a broad array of technologies with varied daily activities and to take initiatives and contribute to the success of the company and Competitive salary package and";
            input = input.replace("B. sc.", "B.Sc");
            input = input.replace("B. Sc.", "B.Sc");
            input = input.replace("B. s. c.", "B.Sc");
            input = input.replace("M. sc.", "M.sc");
            input = input.replace("M. s. c.", "M.Sc");
            input = input.replace("M. Sc.", "M.Sc");
            input = input.replace("P. hd.", "P.hd");
            input = input.replace("P. Hd.", "P.hd");
            input = input.replace("P. h. d.", "P.hc");
            String[] sections = input.split("(?<=[.!?]) +");
            
        
        String [] lines = {input};
        //{
//            "Master of Science in Biomedical Sciences  \n" ,
//"Bachelor of Science  B.S.  \n" ,
//"Master  \n" ,
//"PhD  \n" ,
//"Graduated Nursing  \n" ,
//"Post-graduate DEGREE \n" ,
//"Bachelor's DEGREE in RN  \n" ,
//"Master of Science MSc  \n" ,
//"12th DEGREE  A-levels  \n" ,
//"Licenciéeen Philologie Romane  \n" ,
//"M.A.  \n" ,
//"PhD in Biology  \n" ,
//"Bachelor's DEGREE  ",
//"Anesthesia\n" ,
//"Cardiovascular diseases\n" ,
//"Dermatology\n" ,
//"Emergency medicine\n" ,
//"Endocrinology and metabolism",
//"Gent University",
//"John Studies in Gent University",
//"Data Management for Clinical Research from Vanderbilt University",
//"Design and Interpretation of Clinical Trials (Johns Hopkins University)"
                
        //};
        for (int i = 0; i < lines.length; ++i) {
            String arg = lines[i];
            char[] cs = arg.toCharArray();

            System.out.println("\nFIRST BEST");
            Chunking chunking = crfChunker.chunk(arg);
            System.out.println(chunking);

            int maxNBest = 10;
            System.out.println("\n" + maxNBest + " BEST CONDITIONAL");
            System.out.println("Rank log p(tags|tokens)  Tagging");
            Iterator<ScoredObject<Chunking>> it
                = crfChunker.nBestConditional(cs,0,cs.length,maxNBest);
            for (int rank = 0; rank < maxNBest && it.hasNext(); ++rank) {
                ScoredObject<Chunking> scoredChunking = it.next();
                System.out.println(rank + "    " + scoredChunking.score() + " " + scoredChunking.getObject());
            }

            System.out.println("\nMARGINAL CHUNK PROBABILITIES");
            System.out.println("Rank Chunk Phrase");
            int maxNBestChunks = 10;
            Iterator<Chunk> nBestChunkIt = crfChunker.nBestChunks(cs,0,cs.length,maxNBestChunks);
            for (int n = 0; n < maxNBestChunks && nBestChunkIt.hasNext(); ++n) {
                Chunk chunk = nBestChunkIt.next();
                System.out.println(n + " " + chunk + " " + arg.substring(chunk.start(),chunk.end()));
            }
        }


//        ChunkerEvaluator foldEvaluator
//                = new ChunkerEvaluator(null);
//        foldEvaluator.setMaxConfidenceChunks(1);
//        foldEvaluator.setMaxNBest(1);
//        foldEvaluator.setVerbose(false);
//
//        
//
//        foldEvaluator.setChunker(crfChunker);
//
//        System.out.println("evaluating");
//        for (int i = 0; i < sentences.size(); ++i) {
//            try {
//                foldEvaluator.handle(sentences.get(i));
//            } catch (Exception e) {
//                e.printStackTrace(System.out);
//                System.out.println(sentences.get(i));
//            }
//        }
//        final PrintStatistics printer = new PrintStatistics();
//        //System.out.println(foldEvaluator.toString());
//        printer.printEval("X-VAL", foldEvaluator);
    }
}
