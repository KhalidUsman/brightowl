/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.ML_Package.Muc6ChunkParser;
import com.ML_Package.NER_CrossValidation;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.ml.model.NERModel;
import com.ml.training.Dictionary;
import com.ml.training.TextPreProcessor;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.commons.lang.WordUtils;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

/**
 *
 * @author shahzad
 */
public class Validator {

    private final Parser<ObjectHandler<Chunking>> mParser;
    private final PrintStatistics printer;
    private ChunkerEvaluator eval;
    private static final String DIC_DATA_PATH = "/Users/shahzad/sentiance/brightowl/data/EducationSkillsLocationFuncPersonKnow.txt";

    public Validator(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
        printer = new PrintStatistics();
    }

    public void singleFoldTestWithOldModel(File refFile) throws IOException, ClassNotFoundException {
        SingleFoldTestWithOldModel validator = new SingleFoldTestWithOldModel(mParser);
        validator.validate(refFile);
    }

    public void kFold(File refFile) throws IOException, ClassNotFoundException {
        KFoldValidation validator = new KFoldValidation(mParser);
        validator.validate(refFile);
    }

    public void kFoldCorpus(String corpusPath) throws IOException, ClassNotFoundException {

        File dicFile = new File(DIC_DATA_PATH);
        Parser<ObjectHandler<Chunking>> dicParser
                = new Muc6ChunkParser();

        Dictionary dic = new Dictionary(dicParser);
        Map<String, String> dicMap = dic.getDictionaryItems(dicFile, true);

        ArrayList<File> files = new ArrayList<>();

        try (Stream<Path> paths = Files.walk(Paths.get(corpusPath))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(f -> files.add(f.toFile()));
        }
        File[] corpus = (File[]) files.toArray(new File[files.size()]);
        KFoldValidation validator = new KFoldValidation(mParser);
        //validator.validateFromMultipleFilesWithDic(corpus, dicMap);
        validator.validateFromMultipleFiles(corpus);
    }

    public void singleFoldTrainAndTest(File[] refFile) throws IOException, ClassNotFoundException {
        SingleFoldValidation validator = new SingleFoldValidation(mParser);
        validator.validate(refFile);
    }

    public void getPredictedCategories(String input) throws IOException, ClassNotFoundException, UnsupportedEncodingException, SAXException, TikaException {
        String crawlString = WordUtils.capitalize(TextPreProcessor.normalizeText(input));
        System.out.println("After normalize = "  + crawlString);
        //System.out.println("After Label = "  + crawlString.replaceAll("\\s+", " ").replaceAll(" ,",",").replaceAll(" \\.","\\."));
        NERModel testObj = NERModel.getInstance("local");
        final Map<String, String> categories = testObj.predictResults(crawlString);
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, UnsupportedEncodingException, SAXException, TikaException {
        //File refFile = new File("/Users/shahzad/sentiance/brightowl/data/EducationSkillsLocationFuncPersonKnow.txt");
        File refFile = new File("/Users/shahzad/sentiance/brightowl/data/TrainingNew.txt");
        File refFile1 = new File("/Users/shahzad/sentiance/brightowl/data/TrainingNew1.txt");
        //File refFile = new File("src/main/resources/testSkillRaw.txt");
        File[] files = {refFile, refFile1};
        @SuppressWarnings("deprecation")
        Parser<ObjectHandler<Chunking>> parser
                = new Muc6ChunkParser();
        Validator validator = new Validator(parser);
        //validator.singleFoldTestWithOldModel(refFile);
        int offset = 30000;
        //validator.kFoldCorpus("/Users/shahzad/sentiance/brightowl/data/TrainingDataNew/Training-" + offset);
        //validator.singleFoldTrainAndTest(files);
        String input = "Gain disease information and product knowledge and fully •implement promotion strategy by daily customer calls, with good selling and communication skills; Develop and maintain relationships with clients (doctor and pharmacist) by efficiently delivering product information, promoting the SFDA approved product prescribing information to medical and related professionals. 2. Achieve and exceed the sales target ( Market actual usage) and carry out the promotion activities under legal compliance; establish a good image of the company by daily calls, hospital lectures and largescale promotion activities. 3. Gather market information effectively to draft and execute regional action plans based on regional promotion plans. 4. Understand company policies, objectives, values ​​and culture, timely submit the report of adverse drug events to medical department in Compliance with national laws and regulations, industry standards, company codes of conduct, corporate system, regulations and SOP.   Qualifications 1. Medical / Pharmaceutical / bachelor degree or above, or equivalent 2. Foreign pharmaceutical company working experience is preferred 3. Be honest and integrity 4. Willing to take challenge Job Segment: Medical, Pharmaceutical, Compliance, Healthcare, Science, Legal";
        System.out.println("Raw Text = " + input);
        validator.getPredictedCategories(input);
        //System.out.println(new String(" hello       there   ").trim().replaceAll("\\s+", " "));


    }
}
