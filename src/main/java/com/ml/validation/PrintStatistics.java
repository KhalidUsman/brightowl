/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.chunk.ChunkingEvaluation;
import com.aliasi.classify.PrecisionRecallEvaluation;
import java.util.Set;

/**
 *
 * @author shahzad
 */
public class PrintStatistics {
    
    static final String DEGREE_TAG = "DEGREE";
    static final String EDUCATION_TAG = "EDUCATION";
    static final String LOCATION_TAG = "LOCATION";
    static final String SKILL_TAG = "SKILL";
    static final String FUNCTIONAL_TITLE_TAG = "FUNCTIONTITLE";
    static final String KNOWLEDGE_TAG = "KNOWLEDGE";
    static final String ORGNIZATION_TAG = "ORGANIZATION";
    
    static final String[] REPORT_TYPES = new String[]{
        DEGREE_TAG,
        EDUCATION_TAG,
        LOCATION_TAG,
        SKILL_TAG,
        FUNCTIONAL_TITLE_TAG,
        KNOWLEDGE_TAG,
        ORGNIZATION_TAG

    };
    
    public void printEval(String msg, ChunkerEvaluator evaluator) {
        System.out.println("--------------------------------------------------");
        System.out.println("Printing Evaluator Results");
        ChunkingEvaluation chunkingEval = evaluator.evaluation();
//        Set<ChunkAndCharSeq> ccsSet = chunkingEval.truePositiveSet();
//        for (ChunkAndCharSeq ccs : ccsSet) {
//            System.out.println("String: " + ccs.charSequence() + " -- Type: " + ccs.chunk().type());
//        }
//        Set<Chunking[]> chunkingSet = chunkingEval.cases();
//        ChunkingEvaluation mEvaluation
//        = new ChunkingEvaluation();
//        for (Chunking[] sentences : chunkingSet) {
//            System.out.println("Test String: " + sentences[0].charSequence());
//            int i = 0;
//            for (Chunking chunking : sentences) {
//                CharSequence sentence = chunking.charSequence();
//
//                for (Chunk chunk : chunking.chunkSet()) {
//                    if (i == 0) {
//                        System.out.print("Input - ");
//                    } else {
//                        System.out.print("Output - ");
//                    }
//                    System.out.println("SubString:  [" + sentence.subSequence(chunk.start(), chunk.end()) + "] : Start - End: "+ chunk.start() + " - " + chunk.end() + " : Category is = " + chunk.type());
//                }
//                i++;
//            }
//       //       mEvaluation.addCase(sentences[0], sentences[1]);
//        }
        //System.out.println(mEvaluation.toString());

//        Set<Chunk> test = chunking.chunkSet();
//            for (Chunk c : test) {
//                System.out.println("Test String is = " + feature.substring(c.start(), c.end()) + " : Category is = " + c.type());
//                categories.put(feature.substring(c.start(), c.end()), c.type());
//            }
        for (String tag : REPORT_TYPES) {
            PrecisionRecallEvaluation prEvalByType
                    = chunkingEval
                            .perTypeEvaluation(tag).precisionRecallEvaluation();
            //Set<ChunkAndCharSeq> ch = chunkingEval.perTypeEvaluation(tag).falsePositiveSet();
            if(prEvalByType.precision() >= 0.00){
            System.out.printf("%10s    %6s P=%5.3f R=%5.3f F=%5.3f\n",
                    msg,
                    tag,
                    prEvalByType.precision(),
                    prEvalByType.recall(),
                    prEvalByType.fMeasure());
            }
        }
//
        PrecisionRecallEvaluation prEval
                = chunkingEval.precisionRecallEvaluation();
        System.out.println("");
        System.out.printf("%10s  COMBINED P=%5.3f R=%5.3f F=%5.3f\n",
                msg,
                prEval.precision(),
                prEval.recall(),
                prEval.fMeasure());

        // uncomment to dump out precsion-recall evals
        // ScoredPrecisionRecallEvaluation scoredPrEval
        // = evaluator.confidenceEvaluation();
        // double[][] prCurve = scoredPrEval.prCurve(true);
        // System.out.printf("%5s %5s\n","REC","PREC");
        // for (double[] rp : prCurve) {
        // if (rp[0] < 0.7) continue;
        // System.out.printf("%5.3f %5.3f\n",
        // rp[0],rp[1]);
        // }
        // System.out.printf("MAX F=%5.3f\n",scoredPrEval.maximumFMeasure());
    }
    
}
