/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ml.validation;

import com.aliasi.chunk.Chunking;
import com.aliasi.corpus.ObjectHandler;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author shahzad
 */
public class ChunkingCollector
        implements ObjectHandler<Chunking> {

    public final List<Chunking> mChunkingList = new ArrayList<>();

    @Override
    public void handle(Chunking chunking) {
        mChunkingList.add(chunking);
    }
}
