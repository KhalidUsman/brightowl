package com.config;

import org.yaml.snakeyaml.Yaml;

import java.io.*;

/**
 * 
 * @author shahzad
 */
public class AppConfig {

    private String sparkMaster;
    private String sparkHome;
    private String appJar;
    private String labelMainClass;
    private String outputLogDirectory;
    private String errorLogDirectory;
    private String defaultNumberOfExecutors;
    private String defaultExecutorMemory;
    private String defaultExecutorCores;
    private boolean verbose;
    private String tempFolder;
    private String rawDataLocation;
    private String trainingDataLocation;
    
    private String dbHost;
    private String dbPort;
    private String dbUser;
    private String dbPassword;
    private String dbTable;
    private String dbName;
    
    private String projectModelPath;
    private String runType;
    

    public static AppConfig createFromYamlSettings(String stage) {

        String settingsFileName = "/settings-" + stage + ".yml";
        Yaml yaml = new Yaml();
        String cwd = System.getProperty("user.dir");

        File yamlFile = new File(cwd + File.separator + settingsFileName);

        if (yamlFile.exists()) {
            try {
                FileInputStream fis = new FileInputStream(yamlFile);
                AppConfig consolidatorConfig = yaml.loadAs(fis, AppConfig.class);
                return consolidatorConfig;

            } catch (IOException ioe) {

                ioe.printStackTrace();
                return null;
            }
        } else {
            InputStream yamlIS = AppConfig.class.getResourceAsStream(settingsFileName);
            AppConfig consolidatorConfig = yaml.loadAs(yamlIS, AppConfig.class);
            return consolidatorConfig;
        }
    }

    public String getSparkMaster() {
        return sparkMaster;
    }

    public void setSparkMaster(String sparkMaster) {
        this.sparkMaster = sparkMaster;
    }

    public String getSparkHome() {
        return sparkHome;
    }

    public void setSparkHome(String sparkHome) {
        this.sparkHome = sparkHome;
    }

    public String getAppJar() {
        return appJar;
    }

    public void setAppJar(String appJar) {
        this.appJar = appJar;
    }

    public String getLabelMainClass() {
        return labelMainClass;
    }

    public void setLabelMainClass(String labelMainClass) {
        this.labelMainClass = labelMainClass;
    }

    public String getOutputLogDirectory() {
        return outputLogDirectory;
    }

    public void setOutputLogDirectory(String outputLogDirectory) {
        this.outputLogDirectory = outputLogDirectory;
    }

    public String getErrorLogDirectory() {
        return errorLogDirectory;
    }

    public void setErrorLogDirectory(String errorLogDirectory) {
        this.errorLogDirectory = errorLogDirectory;
    }

    public String getDefaultNumberOfExecutors() {
        return defaultNumberOfExecutors;
    }

    public void setDefaultNumberOfExecutors(String defaultNumberOfExecutors) {
        this.defaultNumberOfExecutors = defaultNumberOfExecutors;
    }

    public String getDefaultExecutorMemory() {
        return defaultExecutorMemory;
    }

    public void setDefaultExecutorMemory(String defaultExecutorMemory) {
        this.defaultExecutorMemory = defaultExecutorMemory;
    }

    public String getDefaultExecutorCores() {
        return defaultExecutorCores;
    }

    public void setDefaultExecutorCores(String defaultExecutorCores) {
        this.defaultExecutorCores = defaultExecutorCores;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public String getTempFolder() {
        return tempFolder;
    }

    public void setTempFolder(String tempFolder) {
        this.tempFolder = tempFolder;
    }

    public String getRawDataLocation() {
        return rawDataLocation;
    }

    public void setRawDataLocation(String rawDataLocation) {
        this.rawDataLocation = rawDataLocation;
    }

    public String getTrainingDataLocation() {
        return trainingDataLocation;
    }

    public void setTrainingDataLocation(String trainingDataLocation) {
        this.trainingDataLocation = trainingDataLocation;
    } 

    public String getDbHost() {
        return dbHost;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbTable() {
        return dbTable;
    }

    public void setDbTable(String dbTable) {
        this.dbTable = dbTable;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getProjectModelPath() {
        return projectModelPath;
    }

    public void setProjectModelPath(String projectModelPath) {
        this.projectModelPath = projectModelPath;
    }

    public String getRunType() {
        return runType;
    }

    public void setRunType(String runType) {
        this.runType = runType;
    }
}
