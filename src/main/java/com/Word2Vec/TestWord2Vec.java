package com.Word2Vec;

import com.dbConnection.Connect_Database;
import org.datavec.api.util.ClassPathResource;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;

import java.io.*;
import java.util.Collection;

public class TestWord2Vec {
//    public static Collection<String> main(String arg) throws Exception {
//        String keyword = arg;
//        //String keyword = "science";
//        String modelPath = new ClassPathResource("model.txt").getFile().getAbsolutePath();
//        Word2Vec word2Vec = WordVectorSerializer.readWord2VecModel(modelPath);
//        Collection<String> lst = word2Vec.wordsNearest(keyword, 10);
//        System.out.println("10 Words closest to " + keyword + " : " + lst);
//        return lst;
//    }

    public static void main(String[] args) throws Exception {
        String modelPath = args[0];
        //String keywordFile = args[1];
        String keywordsFilePath = args[1];
        //String outputFile = args[2];

        //String modelPath = new ClassPathResource("model.txt").getFile().getAbsolutePath();
        /*
        FileWriter fw = new FileWriter(outputFile);
        for(String feed_word : lst){
            String str = "Similartity " + keyword + " : " + feed_word + " = " + word2Vec.similarity(keyword, feed_word);
            //System.out.println(str);
            fw.write(str);
            fw.write(System.getProperty( "line.separator" ));
        }
        fw.close();
        */

        Connect_Database dao = new Connect_Database();
        dao.readDataBase();

        File file = new File(keywordsFilePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String keyword;

        Word2Vec word2Vec = WordVectorSerializer.readWord2VecModel(modelPath);
        while ((keyword = br.readLine()) != null) {
            keyword = keyword.toLowerCase().replace(" ", "_");
            Collection<String> lst = word2Vec.wordsNearest(keyword, 10);
            for(String feed_word : lst){
                //String str = "Similartity " + keyword + " : " + feed_word + " = " + word2Vec.similarity(keyword, feed_word);
                dao.InsertOrUpdateRecord(keyword, feed_word, word2Vec.similarity(keyword, feed_word));
            }
        }
        dao.close();
        System.out.println("Done");
    }
}
