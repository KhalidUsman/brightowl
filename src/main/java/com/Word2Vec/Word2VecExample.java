package com.Word2Vec;

import org.datavec.api.util.ClassPathResource;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

public class Word2VecExample {
    private static Logger log = LoggerFactory.getLogger(Word2VecExample.class);
    Word2Vec vec = null;

    public static void main(String[] args) throws Exception {
        Word2VecExample word2vecExp = new Word2VecExample();
        //word2vecExp.train();
        word2vecExp.test();
    }

    public void train() throws IOException {
        // Gets Path to Text file
        String filePath = new ClassPathResource("Crawled.txt").getFile().getAbsolutePath();

        log.info("Load & Vectorize Sentences....");
        // Strip white space before and after for each line
        SentenceIterator iter = new BasicLineIterator(filePath);
        // Split on white spaces in the line to get words
        TokenizerFactory t = new DefaultTokenizerFactory();

        /*
            CommonPreprocessor will apply the following regex to each token: [\d\.:,"'\(\)\[\]|/?!;]+
            So, effectively all numbers, punctuation symbols and some special symbols are stripped off.
            Additionally it forces lower case for all tokens.
         */
        t.setTokenPreProcessor(new CommonPreprocessor());

        log.info("Building model....");
        Word2Vec vec = new Word2Vec.Builder()
                .minWordFrequency(5)
                .iterations(1)
                .layerSize(100)
                .seed(42)
                .windowSize(5)
                .iterate(iter)
                .tokenizerFactory(t)
                .build();

        log.info("Fitting Word2Vec model....");
        vec.fit();
        log.info("Writing word vectors to text file....");
        String modelPath = new ClassPathResource("model.txt").getFile().getAbsolutePath();
        WordVectorSerializer.writeWordVectors(vec, modelPath);
    }

    public void test() throws IOException {
        log.info("Closest Words:");
        String modelPath = new ClassPathResource("model.txt").getFile().getAbsolutePath();
        Word2Vec word2Vec = WordVectorSerializer.readWord2VecModel(modelPath);
        Collection<String> lst = word2Vec.wordsNearest("master", 10);
        System.out.println("10 Words closest to 'project': " + lst);
    }
}
