package com.dbConnection;

import com.config.AppConfig;
import com.dbConnection.ReturnTripple;
import static com.ml.ranking.Main.EXPERT_ID;
import static com.ml.ranking.Main.PROFILE;
import static com.ml.ranking.Main.PROJECT_ID;
import static com.ml.ranking.Main.VERBOSE_DEBUG;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.mysql.jdbc.DatabaseMetaData;
//serverDbName = 'betabrig_khalid'
public class Connect_Database {

    ReturnTripple returnTripple;

    static int bulkEntryCount = 50;
    static int counter = 0;
    static StringBuilder sql = new StringBuilder();
    static ArrayList<String> sqlList = new ArrayList<>();

    public Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    final private String host = "brightowl.pro";
    final private String user = "betabrig_ml_kh";
    final private String passwd = "kSH#0!Ze^)n5[LU;o0";
    final private String dbName = "/betabrig_khalid?";

    final private String proHost = "brightowl.pro";
    final private String proUser = "betabrig_ml_kh";
    final private String proPasswd = "kSH#0!Ze^)n5[LU;o0";
    final private String proDbName = "/betabrig_brightowl?";
    AppConfig config = null;

    public Connect_Database() {
        config = AppConfig.createFromYamlSettings(PROFILE);
    }

    public Connect_Database(String env) {
        config = AppConfig.createFromYamlSettings(env);
    }

    public Connection getDBConnection() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
//            connect = DriverManager
//                    .getConnection("jdbc:mysql://" + proHost + proDbName
//                            + "user=" + proUser + "&password=" + proPasswd + "&useUnicode=true&characterEncoding=UTF-8");
//          
//            System.out.println("Connecting to -> Host: " + config.getDbHost() + ", DB: " + config.getDbName() +
//                            ", User: " +config.getDbUser() + ", Pass: " + config.getDbPassword());
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + config.getDbHost() + "/" + config.getDbName(),
                            config.getDbUser(), config.getDbPassword());
//            System.out.println("Connection Established Successfull and the DATABASE NAME IS:"
//                    + connect.getMetaData().getDatabaseProductName());

        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Unable to make connection with DB");
            throw ex;
        }
        return connect;
    }

    public void readDataBase() throws Exception {
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + proHost + proDbName
                            + "user=" + proUser + "&password=" + proPasswd + "&useUnicode=true&characterEncoding=UTF-8");

//            connect = DriverManager
//                    .getConnection("jdbc:mysql://" + config.getDbHost() + config.getDbName()
//                            + "user=" + config.getDbUser() + "&password=" + config.getDbPassword());
            statement = connect.createStatement();
//            System.out.println("Connection Established Successfull and the DATABASE NAME IS:"
//                    + connect.getMetaData().getDatabaseProductName());
        } catch (Exception e) {
            System.out.println("Unable to make connection with DB");
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void saveRecords(ArrayList<Map<String, Double>> matchingArr, String sql) throws Exception {
        try {
            preparedStatement = connect.prepareStatement(sql);
            for (Map<String, Double> map : matchingArr) {
                for (Entry<String, Double> entry : map.entrySet()) {
                    if (entry.getKey() == "expert_id") {
                        preparedStatement.setInt(1, (int) ((double) entry.getValue()));
                    } else if (entry.getKey() == "project_id") {
                        preparedStatement.setInt(2, (int) ((double) entry.getValue()));
                    } else if (entry.getKey() == "ml_score") {
                        preparedStatement.setFloat(3, (float) ((double) entry.getValue()));
                        preparedStatement.setFloat(4, (float) ((double) entry.getValue()));
                    }
                }
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public void InsertOrUpdateRecord(String keyword, String relatedKeywords, double score) throws Exception {
        try {
            //String relatedKeywords = String.join(", ", list);
            String sql = "INSERT INTO word2vec (w2c_keyword, w2c_related_keywords, w2c_score) VALUES('" + keyword + "' , '" + relatedKeywords + "' , '" + score + "')";
            System.out.println("SQL = " + sql);
            statement = connect.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public void saveRecord(String sql) throws Exception {
        try {
            //System.out.println("Processing SQL = " + sql);
            statement = connect.createStatement();
            statement.executeUpdate(sql + String.join(",", sqlList));
            System.out.println("Records Saved!");
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public String prepareSaveRecord(Map<String, Double> map) throws Exception {
        try {
            int projectId = 0;
            int expertId = 0;
            float ml_Score = 0;
            for (Entry<String, Double> entry : map.entrySet()) {
                if (entry.getKey() == "expert_id") {
                    expertId = (int) ((double) entry.getValue());
                } else if (entry.getKey() == "project_id") {
                    projectId = (int) ((double) entry.getValue());
                } else if (entry.getKey() == "ml_score") {
                    ml_Score = (float) ((double) entry.getValue());
                }
            }
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            //String sql = "INSERT INTO z_expert_project_complete_" + expertId + " (project_id, ml_score_kh, match_result, match_ml, ml_score_pm, created_at, updated_at) VALUES(" + projectId + "," + ml_Score + ",0 ,0 ,0, '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
            String sql = "(" + projectId + "," + ml_Score + ",0 ,0 ,0, '" + currentTimestamp + "', '" + currentTimestamp + "')";
            //System.out.println("SQL = " + sql);
            return sql;

        } catch (Exception e) {
            throw e;
        }
    }

    public String prepareSaveRecordExtended(Map<String, Double> map) throws Exception {
        try {
            int projectId = 0;
            int expertId = 0;
            float ml_Score, ml_score_kh_cosine, ml_score_kh_cosine_text, ml_score_kh_distance, ml_score_skill, ml_score_function_title, ml_score_location, ml_score_education, ml_score_language, ml_score_personality, ml_score_knowledge, ml_factor;
            ml_Score = ml_score_kh_cosine = ml_score_kh_cosine_text = ml_score_kh_distance = ml_score_skill = ml_score_function_title = ml_score_location = ml_score_education = ml_score_language = ml_score_personality = ml_score_knowledge = ml_factor = 0;
            for (Entry<String, Double> entry : map.entrySet()) {
                if (entry.getKey().equals("expert_id")) {
                    expertId = (int) ((double) entry.getValue());
                } else if ("project_id".equals(entry.getKey())) {
                    projectId = (int) ((double) entry.getValue());
                } else if ("ml_score".equals(entry.getKey())) {
                    ml_Score = (float) ((double) entry.getValue());
                } else if ("ml_score_cosine".equals(entry.getKey())) {
                    ml_score_kh_cosine = (float) ((double) entry.getValue());
                } else if ("ml_score_cosine_text".equals(entry.getKey())) {
                    ml_score_kh_cosine_text = (float) ((double) entry.getValue());
                } else if ("ml_score_distance".equals(entry.getKey())) {
                    ml_score_kh_distance = (float) ((double) entry.getValue());
                } else if ("ml_score_skill".equals(entry.getKey())) {
                    ml_score_skill = (int) ((double) entry.getValue());
                } else if ("ml_score_function_title".equals(entry.getKey())) {
                    ml_score_function_title = (float) ((double) entry.getValue());
                } else if ("ml_score_location".equals(entry.getKey())) {
                    ml_score_location = (float) ((double) entry.getValue());
                } else if ("ml_score_education".equals(entry.getKey())) {
                    ml_score_education = (float) ((double) entry.getValue());
                } else if ("ml_score_language".equals(entry.getKey())) {
                    ml_score_language = (float) ((double) entry.getValue());
                } else if ("ml_score_personality".equals(entry.getKey())) {
                    ml_score_personality = (float) ((double) entry.getValue());
                } else if ("ml_score_knowledge".equals(entry.getKey())) {
                    ml_score_knowledge = (float) ((double) entry.getValue());
                } else if ("ml_factor".equals(entry.getKey())) {
                    ml_factor = (float) ((double) entry.getValue());
                }
            }
            //ml_score_skill, ml_score_function_title, ml_score_location, ml_score_education, ml_score_language, ml_score_personality, ml_score_knowledge, ml_factor
            Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            //String sql = "INSERT INTO z_expert_project_complete_" + expertId + " (project_id, ml_score_kh, match_result, match_ml, ml_score_pm, created_at, updated_at) VALUES(" + projectId + "," + ml_Score + ",0 ,0 ,0, '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
            String sql = "(" + projectId + "," + expertId + "," + ml_Score + "," + ml_score_kh_cosine + "," + ml_score_kh_cosine_text + "," + ml_score_kh_distance + ",0 ,0 ,0, '" + currentTimestamp + "', '" + currentTimestamp + "' ," + ml_score_skill + "," + ml_score_function_title + "," + ml_score_location + "," + ml_score_education + "," + ml_score_language + "," + ml_score_personality + "," + ml_score_knowledge + "," + ml_factor + ")";
            //System.out.println("SQL = " + sql);
            return sql;

        } catch (Exception e) {
            throw e;
        }
    }

    public void createMlTable() throws SQLException {
        Statement st = null;
        try {
            st = connect.createStatement();
            String createSql = "CREATE TABLE IF NOT EXISTS  z_expert_project_complete_all"
                    + "(project_id int(11) NOT NULL ,"
                    + " expert_id int(11) NOT NULL,"
                    + " match_result float(8,2) NOT NULL DEFAULT '0',"
                    + " match_ml float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_pm float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh_cosine float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh_cosine_text float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh_distance float(8,2) NOT NULL DEFAULT '0',"
                    + " created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                    + " updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                    + " ml_score_skill float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_function_title float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_location float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_education float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_language float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_personality float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_knowledge float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_factor float(8,2) NOT NULL DEFAULT '0',"
                    + " PRIMARY KEY ( project_id, expert_id ))";
            st.executeUpdate(createSql);
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
    
    public void createAndTruncateMlTable() throws SQLException {
        Statement st = null;
        try {
            st = connect.createStatement();
            String createSql = "CREATE TABLE IF NOT EXISTS  z_expert_project_complete_all"
                    + "(project_id int(11) NOT NULL ,"
                    + " expert_id int(11) NOT NULL,"
                    + " match_result float(8,2) NOT NULL DEFAULT '0',"
                    + " match_ml float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_pm float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh_cosine float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh_cosine_text float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_kh_distance float(8,2) NOT NULL DEFAULT '0',"
                    + " created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                    + " updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                    + " ml_score_skill float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_function_title float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_location float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_education float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_language float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_personality float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_score_knowledge float(8,2) NOT NULL DEFAULT '0',"
                    + " ml_factor float(8,2) NOT NULL DEFAULT '0',"
                    + " PRIMARY KEY ( project_id, expert_id ))";
            st.executeUpdate(createSql);
            st.executeUpdate("TRUNCATE TABLE z_expert_project_complete_all");
        } catch (SQLException e) {
            throw e;
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void InsertOrUpdateRecordSingleTable(ArrayList<Map<String, Double>> matchingArr) throws Exception {
        System.out.println("Writing data to database");
        try {
            int bulkEntryCount = 100;
            int counter = 0;
            StringBuilder sql = new StringBuilder();
            for (Map<String, Double> map : matchingArr) {

                for (Entry<String, Double> entry : map.entrySet()) {

                    if (entry.getKey() == "expert_id") {

                        if (counter == 0) {
                            sql.setLength(0);
                            sql.append("INSERT INTO z_expert_project_complete_all (project_id, expert_id, ml_score_kh, ml_score_kh_cosine, ml_score_kh_cosine_text, ml_score_kh_distance, match_result, match_ml, ml_score_pm, created_at, updated_at) VALUES ");
                        }
                        sql.append(this.prepareSaveRecordExtended(map));

                        counter++;
                        //System.out.println("Counter: " + counter);
                        //System.out.println("counter % bulkEntryCount: " + (counter % bulkEntryCount));
                        if (counter % bulkEntryCount == 0) {
                            this.saveRecord(sql.toString());
                            counter = 0;
                        } else {
                            sql.append(", ");
                        }
                    }
                }
            }
            System.out.println("Saving from outside loop");
            this.saveRecord(sql.toString().substring(0, sql.toString().trim().length() - 1));

            System.out.println("Records Saved!");
        } catch (Exception e) {
            throw e;
        }
    }

    public void InsertOrUpdateRecordSingleTableContinous(Map<String, Double> map) throws Exception {

        try {

//                for (Entry<String, Double> entry : map.entrySet()) {
//
//                    if (entry.getKey().equalsIgnoreCase("expert_id")) {
//                matchingDic.put("ml_score_skill", cosine_Score.containsKey("skill") ? cosine_Score.get("skill") : 0.0);
//                                    matchingDic.put("ml_score_function_title", cosine_Score.containsKey("function_title") ? cosine_Score.get("function_title") : 0.0);
//                                    matchingDic.put("ml_score_location", cosine_Score.containsKey("location") ? cosine_Score.get("location") : 0.0);
//                                    matchingDic.put("ml_score_education", cosine_Score.containsKey("education") ? cosine_Score.get("education") : 0.0);
//                                    matchingDic.put("ml_score_language", cosine_Score.containsKey("language") ? cosine_Score.get("language") : 0.0);
//                                    matchingDic.put("ml_score_personality", cosine_Score.containsKey("personality") ? cosine_Score.get("personality") : 0.0);
//                                    matchingDic.put("ml_score_knowledge", cosine_Score.containsKey("knowledge") ? cosine_Score.get("knowledge") : 0.0);
//                                    matchingDic.put("ml_factor", cosine_Score.containsKey("ml_factor") ? cosine_Score.get("ml_factor") : 0.0);
            if (counter == 0) {
                sql.setLength(0);
                sql.append("INSERT INTO z_expert_project_complete_all (project_id, expert_id, ml_score_kh, ml_score_kh_cosine, ml_score_kh_cosine_text, ml_score_kh_distance, match_result, match_ml, ml_score_pm, created_at, updated_at, ml_score_skill, ml_score_function_title, ml_score_location, ml_score_education, ml_score_language, ml_score_personality, ml_score_knowledge, ml_factor) VALUES ");
            }
            sqlList.add(this.prepareSaveRecordExtended(map));
            //sql.append(this.prepareSaveRecordExtended(map));

            counter++;
            //System.out.println("Counter: " + counter);
            //System.out.println("counter % bulkEntryCount: " + (counter % bulkEntryCount));
            if (counter % bulkEntryCount == 0) {
                this.getDBConnection();
                //sql.append(String.join(",", sqlList));
                this.saveRecord(sql.toString());
                counter = 0;
                sql.setLength(0);
                sqlList.removeAll(sqlList);
            } else {
                //sql.append(", ");
            }
//                    }
//                }

            //this.saveRecord(sql.toString().substring(0, sql.toString().trim().length() - 1));
        } catch (Exception e) {
            throw e;
        }
    }

    public void insertRemaining() throws Exception {
        if (sql.length() > 0) {
            try {
                this.getDBConnection();
                //sql.append(String.join(",", sqlList));
                System.out.println(sql.toString());
                System.out.println("Saving the remaining portion ");
                //this.saveRecord(sql.toString().substring(0, sql.toString().trim().length() - 1));
                this.saveRecord(sql.toString());
                sql.setLength(0);
                counter = 0;
                sqlList.removeAll(sqlList);
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void InsertOrUpdateRecord(ArrayList<Map<String, Double>> matchingArr) throws Exception {
        System.out.println("Writing data to database");
        try {
            this.getDBConnection();
            int bulkEntryCount = 100;
            int counter = 0;
            StringBuilder sql = new StringBuilder();
            boolean tableCreated = false;
            for (Map<String, Double> map : matchingArr) {

                for (Entry<String, Double> entry : map.entrySet()) {

                    if (entry.getKey() == "expert_id") {
                        DatabaseMetaData dbm = (DatabaseMetaData) connect.getMetaData();
                        String tableName = "z_expert_project_complete_" + (int) ((double) entry.getValue());
//                        ResultSet tables = dbm.getTables(null, null, tableName, null);
//                        if (tables.next()) {
//                            this.saveRecord(map);
//                        } else {
//                            String createSql = "CREATE TABLE IF NOT EXISTS  " + tableName
//                                    + "(project_id int(11) NOT NULL ,"
//                                    + " match_result float(8,2) NOT NULL DEFAULT '0',"
//                                    + " match_ml float(8,2) NOT NULL DEFAULT '0',"
//                                    + " ml_score_pm float(8,2) NOT NULL DEFAULT '0',"
//                                    + " ml_score_kh float(8,2) NOT NULL DEFAULT '0',"
//                                    + " created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"
//                                    + " updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"
//                                    + " PRIMARY KEY ( project_id ))";
//                            Statement st = connect.createStatement();
//                            st.executeUpdate(createSql);
//                            this.saveRecord(map);
//                        }
                        if (!tableCreated) {
                            String createSql = "CREATE TABLE IF NOT EXISTS  " + tableName
                                    + "(project_id int(11) NOT NULL ,"
                                    + " match_result float(8,2) NOT NULL DEFAULT '0',"
                                    + " match_ml float(8,2) NOT NULL DEFAULT '0',"
                                    + " ml_score_pm float(8,2) NOT NULL DEFAULT '0',"
                                    + " ml_score_kh float(8,2) NOT NULL DEFAULT '0',"
                                    + " created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                                    + " updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                                    + " PRIMARY KEY ( project_id ))";
                            Statement st = connect.createStatement();
                            st.executeUpdate(createSql);
                            st.executeUpdate("TRUNCATE TABLE " + tableName);
                            tableCreated = true;
                        }
                        if (counter == 0) {
                            sql.setLength(0);
                            sql.append("INSERT INTO z_expert_project_complete_" + (int) ((double) entry.getValue()) + " (project_id, ml_score_kh, match_result, match_ml, ml_score_pm, created_at, updated_at) VALUES ");
                        }
                        sql.append(this.prepareSaveRecord(map));

                        counter++;
                        //System.out.println("Counter: " + counter);
                        //System.out.println("counter % bulkEntryCount: " + (counter % bulkEntryCount));
                        if (counter % bulkEntryCount == 0) {
                            this.saveRecord(sql.toString());
                            counter = 0;
                        } else {
                            sql.append(", ");
                        }
                    }
                }
            }
            System.out.println("Saving from outside loop");
            this.saveRecord(sql.toString().substring(0, sql.toString().trim().length() - 1));

            System.out.println("Records Saved!");
        } catch (Exception e) {
            throw e;
        }
    }

    public void updateRecords(String sql) throws Exception {
        Statement st = null;
        try {
            st = connect.createStatement();
            st.executeUpdate(sql);
            //preparedStatement = connect.prepareStatement(sql);
            //preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void deleteRecords(ArrayList<Map<String, Double>> matchingArr, String sql) throws Exception {
        try {
            preparedStatement = connect.prepareStatement(sql);
            for (Map<String, Double> map : matchingArr) {
                for (Entry<String, Double> entry : map.entrySet()) {
                    if (entry.getKey() == "expert_id") {
                        preparedStatement.setInt(1, (int) ((double) entry.getValue()));
                    } else if (entry.getKey() == "project_id") {
                        preparedStatement.setInt(2, (int) ((double) entry.getValue()));
                    } else if (entry.getKey() == "ml_score") {
                        preparedStatement.setFloat(3, (float) ((double) entry.getValue()));
                        preparedStatement.setFloat(4, (float) ((double) entry.getValue()));
                    }
                }
            }
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public ResultSet getAllExperts() throws Exception {
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT expert_id FROM expert");
            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public ResultSet getAllProjects() throws Exception {
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT project_id,crawled_job_detail,is_crawled FROM projects");
            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public ResultSet getExperts() throws Exception {
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE profile_complete_percent >= 60");
            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public boolean checkPresentBefore(int expert_id, int project_id) throws Exception {
        boolean found = false;
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT expert_id FROM z_expert_project_complete_all WHERE expert_id = 60" + expert_id + " and project_id=" + project_id);
            while (resultSet.next()) {
                if (resultSet.getString(1).length() > 0) {
                    found = true;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }

        return found;
    }

    public ResultSet getProjectsWithId() throws Exception {
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT project_id FROM projects WHERE k_ml_update = 1 ORDER BY updated_at DESC LIMIT 1");
            //resultSet = statement.executeQuery("SELECT project_id,crawled_job_detail,is_crawled FROM projects");
            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public ResultSet getProjects() throws Exception {
        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT project_id FROM projects");
            // and project_id > 759 and project_id < 761
            // and project_id > 759 and project_id < 761 LIMIT 500
            if (VERBOSE_DEBUG) {
                resultSet = statement.executeQuery("SELECT project_id FROM projects where project_id = " + PROJECT_ID);
            } else {
                resultSet = statement.executeQuery("SELECT project_id FROM projects where project_status=1 order by project_id desc limit 4000");
            }

            return resultSet;
        } catch (Exception e) {
            throw e;
        }
//        finally{
//            try {
//                statement.close();
//            } catch (SQLException ex) {
//                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    public ResultSet getProjectsByDate(String date) throws Exception {
        System.out.println("Getting the projects for the date: " + date);
        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT project_id FROM projects");
            // and project_id > 759 and project_id < 761
            // and project_id > 759 and project_id < 761 LIMIT 500
            if (VERBOSE_DEBUG) {
                resultSet = statement.executeQuery("select * from projects where DATE(created_at) = DATE('" + date + "') and project_status=1 and project_id = " + PROJECT_ID + " order by created_at desc");
            } else {
                resultSet = statement.executeQuery("select * from projects where DATE(created_at) = DATE('" + date + "') and project_status=1 order by created_at desc");
                System.out.println("select * from projects where DATE(created_at) = DATE('" + date + "') and project_status=1 order by created_at desc");
            }

            return resultSet;
        } catch (Exception e) {
            throw e;
        }
//        finally{
//            try {
//                statement.close();
//            } catch (SQLException ex) {
//                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    public ResultSet getProjectsWithLimit(int offset, int records) throws Exception {
        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT project_id FROM projects");
            resultSet = statement.executeQuery("SELECT project_id FROM projects where is_crawled=1 limit " + offset + "," + records);

            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public ResultSet getExpertsWithId() throws Exception {
        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE profile_complete_percent >= 60 AND k_ml_update = 1 ORDER BY updated_at DESC");
            if (VERBOSE_DEBUG) {
                resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE expert_id=" + EXPERT_ID);
            } else {
                resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE profile_complete_percent >= 60 AND k_ml_update = 1 ORDER BY updated_at");
            }
            return resultSet;
        } catch (Exception e) {
            throw e;
        }
//        finally{
//            try {
//                statement.close();
//            } catch (SQLException ex) {
//                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    public ResultSet getExpertsWithIdAndProjects() throws Exception {
        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE profile_complete_percent >= 60 AND k_ml_update = 1 ORDER BY updated_at DESC");
            if (VERBOSE_DEBUG) {

                resultSet = statement.executeQuery("select e.expert_id, p.project_id \n"
                        + "from \n"
                        + "(SELECT expert_id FROM expert WHERE expert_id= " + EXPERT_ID + " ) e, \n"
                        + "(SELECT project_id FROM projects where project_id= " + PROJECT_ID + ") p\n"
                );

                //resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE expert_id=" + EXPERT_ID);
            } else {
                resultSet = statement.executeQuery("select e.expert_id, p.project_id \n"
                        + "from \n"
                        + "(SELECT expert_id FROM expert WHERE profile_complete_percent >= 60 AND k_ml_update = 1) e, \n"
                        + "(SELECT project_id FROM projects where project_status=1) p\n"
                        + "WHERE \n"
                        + "(e.expert_id, p.project_id) \n"
                        + "NOT IN \n"
                        + "(select expert_id, project_id from z_expert_project_complete_all) limit 100");
            }
            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public ResultSet getCrawledProjects() throws Exception {
        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT project_description FROM projects WHERE is_crawled = 1 AND project_lang_code = 'en' LIMIT 10000");
            //resultSet = statement.executeQuery("SELECT project_id,crawled_job_detail,is_crawled FROM projects");
            return resultSet;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    private void writeMetaData(ResultSet resultSet) throws SQLException {
        // Now get some metadata from the database
        // Result set get the result of the SQL query
        System.out.println("The columns in the table are: ");
        System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
            System.out.println("Column " + i + " " + resultSet.getMetaData().getColumnName(i));
        }
    }

    private void writeResultSet(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String project = resultSet.getString("project_id");
        }
    }

    ReturnTripple getInterested() {

        String experts[];
        String projects[];
        String interests[];

        List<String> expert = new ArrayList<String>();
        List<String> project = new ArrayList<String>();
        List<String> interest = new ArrayList<String>();

        try {
            //resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
            // resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space");

            // To randomize and make the data balanced.
            // SELECT COUNT(`eps_id`) FROM `expert_project_space` WHERE `current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied'
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space WHERE expert_id IN (SELECT expert_id FROM expert WHERE profile_complete_percent >= 60) AND (`current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied' OR `current_space` = 'dislike')");

            while (resultSet.next()) {
                expert.add(String.valueOf(resultSet.getInt(1)));
                project.add(String.valueOf(resultSet.getInt(2)));
                interest.add(resultSet.getString(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        experts = expert.toArray(new String[expert.size()]);
        projects = project.toArray(new String[project.size()]);
        interests = interest.toArray(new String[interest.size()]);

        this.returnTripple = new ReturnTripple(experts, projects, interests);
        return this.returnTripple;
    }

    ReturnTripple getBalancedInterested() {

        String experts[];
        String projects[];
        String interests[];

        List<String> expert = new ArrayList<String>();
        List<String> project = new ArrayList<String>();
        List<String> interest = new ArrayList<String>();

        try {
            //resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
            // resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space");

            // To randomize and make the data balanced.
            // SELECT COUNT(`eps_id`) FROM `expert_project_space` WHERE `current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied'
            statement = connect.createStatement();
            resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space WHERE `current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied'");

            int count = 0;
            while (resultSet.next()) {
                expert.add(String.valueOf(resultSet.getInt(1)));
                project.add(String.valueOf(resultSet.getInt(2)));
                interest.add(resultSet.getString(3));
                count++;
            }
            System.out.println("Total like records = " + count);
            try {
                statement = connect.createStatement();
                resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space WHERE `current_space` = 'dislike' LIMIT " + count);
                while (resultSet.next()) {
                    expert.add(String.valueOf(resultSet.getInt(1)));
                    project.add(String.valueOf(resultSet.getInt(2)));
                    interest.add(resultSet.getString(3));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        experts = expert.toArray(new String[expert.size()]);
        projects = project.toArray(new String[project.size()]);
        interests = interest.toArray(new String[interest.size()]);

        this.returnTripple = new ReturnTripple(experts, projects, interests);
        return this.returnTripple;
    }

    public String[] getUniqueProjects() {

        String projects[];
        List<String> project = new ArrayList<String>();

        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
            resultSet = statement.executeQuery("SELECT DISTINCT project_id FROM `expert_project_space` WHERE project_id > 40000 AND project_id < 45001");
            while (resultSet.next()) {
                project.add(String.valueOf(resultSet.getInt(1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(Connect_Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        projects = project.toArray(new String[project.size()]);
        return projects;
    }

    public String[] getUniqueExperts() {

        String experts[];
        List<String> expert = new ArrayList<String>();

        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT DISTINCT expert_id FROM `expert_project_space` WHERE `expert_id` > 4000");
            resultSet = statement.executeQuery("SELECT DISTINCT expert_id FROM `expert_project_space` WHERE `expert_id` IN (SELECT expert_id FROM expert WHERE profile_complete_percent >= 60)");

            while (resultSet.next()) {
                expert.add(String.valueOf(resultSet.getInt(1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        experts = expert.toArray(new String[expert.size()]);
        return experts;
    }

    public void savePersonalities() throws IOException {

        List<String> personality = new ArrayList<String>();

        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
            resultSet = statement.executeQuery("SELECT p_title FROM personality");

            File fout = new File("Personality.txt");
            FileOutputStream fos = new FileOutputStream(fout);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            bw.write("<root>");
            bw.newLine();

            while (resultSet.next()) {
                personality.add(resultSet.getString(1));
                String keyStr = "<s> <ENAMEX TYPE=" + '"' + "PERSONALITY" + '"' + ">" + (resultSet.getString(1)).trim() + "</ENAMEX> </s>";
                bw.write(keyStr);
                bw.newLine();
            }
            bw.write("</root>");
            bw.newLine();
            bw.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveKnowledge() throws IOException {

        List<String> knowledge = new ArrayList<String>();

        try {
            statement = connect.createStatement();
            //resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
            resultSet = statement.executeQuery("SELECT know_title FROM knowledge");

            File fout = new File("Knowledge.txt");
            FileOutputStream fos = new FileOutputStream(fout);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            bw.write("<root>");
            bw.newLine();

            while (resultSet.next()) {
                knowledge.add(resultSet.getString(1));
                String keyStr = "<s> <ENAMEX TYPE=" + '"' + "KNOWLEDGE" + '"' + ">" + (resultSet.getString(1)).trim() + "</ENAMEX> </s>";
                bw.write(keyStr);
                bw.newLine();
            }
            bw.write("</root>");
            bw.newLine();
            bw.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // You need to close the resultSet
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
