
/*
 * Carrot2 project.
 *
 * Copyright (C) 2002-2016, Dawid Weiss, Stanisław Osiński.
 * All rights reserved.
 *
 * Refer to the full license file "carrot2.LICENSE"
 * in the root folder of the repository checkout or at:
 * http://www.carrot2.org/carrot2.LICENSE
 */

package com.un_supervised;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.dbConnection.Connect_Database;
import org.carrot2.clustering.lingo.LingoClusteringAlgorithm;
import org.carrot2.clustering.stc.STCClusteringAlgorithm;
import org.carrot2.clustering.synthetic.ByUrlClusteringAlgorithm;
import org.carrot2.core.Cluster;
import org.carrot2.core.Controller;
import org.carrot2.core.ControllerFactory;
import org.carrot2.core.Document;
import org.carrot2.core.IDocumentSource;
import org.carrot2.core.ProcessingResult;
//import org.carrot2.examples.ConsoleFormatter;

/**
 * This example shows how to cluster a set of documents available as an {@link ArrayList}.
 * This setting is particularly useful for quick experiments with custom data for which
 * there is no corresponding {@link IDocumentSource} implementation. For production use,
 * it's better to implement a {@link IDocumentSource} for the custom document source, so
 * that e.g., the {@link Controller} can cache its results, if needed.
 *
 //* @see ClusteringDataFromDocumentSources
 //* @see UsingCachingController
 */
public class LingoClustering
{
    public static void main(String [] args) throws Exception {

        {
            //String cleansingFilePath = args[0];
            String cleansingFilePath = "C:/Users/Xorlogics-ML/BrightOwl/src/main/resources/Cleansing.txt";
            Connect_Database dao = new Connect_Database();
            dao.readDataBase();
            ResultSet crawledProjects = dao.getCrawledProjects();

            File file = new File(cleansingFilePath);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;

            final ArrayList<Document> documentsArr = new ArrayList<Document>();

            while (crawledProjects.next()) {

                String crawledText = crawledProjects.getString("project_description").replaceAll("\\<[^>]*>"," ").toLowerCase();
                while ((st = br.readLine()) != null) {
                    String[] replace_Arr = st.split("!");
                    if (replace_Arr[1].equals("space")) {
                        crawledText = crawledText.replace(replace_Arr[0].toLowerCase()," ");
                    }
                    else if (replace_Arr[1].equals("remove")) {
                        crawledText = crawledText.replace(replace_Arr[0].toLowerCase(),"");
                    }
                    else {
                        crawledText = crawledText.replace(replace_Arr[0].toLowerCase(),replace_Arr[1].toLowerCase());
                    }
                }
                crawledText = crawledText.replaceAll("[^A-Za-z0-9 .,$']", " ").replaceAll("\\s{2,}", " ");
                documentsArr.add(new Document(crawledText));
            }

            final Controller controller = ControllerFactory.createSimple();

            final ProcessingResult byTopicClusters = controller.process(documentsArr, null,
                    LingoClusteringAlgorithm.class);
            final ProcessingResult byTopicClustersSTC = controller.process(documentsArr, null,
                    STCClusteringAlgorithm.class);

            final List<Cluster> clustersByTopic = byTopicClusters.getClusters();
            final List<Cluster> clustersByTopicSTC = byTopicClustersSTC.getClusters();

            /* Perform clustering by domain. In this case query is not useful, hence it is null. */
            final ProcessingResult byDomainClusters = controller.process(documentsArr, null,
                    ByUrlClusteringAlgorithm.class);
            final List<Cluster> clustersByDomain = byDomainClusters.getClusters();

            System.out.println("Lingo" + clustersByTopic);
            System.out.println("STC" + clustersByTopicSTC);
            System.out.println("URL" + clustersByDomain);
        }
    }
}
