package com.ML_Package;

//import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import java.io.FileReader;
//import java.util.ArrayList;
import java.util.Set;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.Chunking;
import com.aliasi.util.AbstractExternalizable;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.datavec.api.util.ClassPathResource;

public class NER_Testing {

    private Chunker chunker = null;

    public NER_Testing() throws ClassNotFoundException, IOException {
        String filePath = new ClassPathResource("EducationSkillsLocationFuncPersonKnow.model").getFile().getAbsolutePath();
        File modelFile = new File(filePath);
        chunker = (Chunker) AbstractExternalizable.readObject(modelFile);
    }

    public Map<String, String> getCategories(String[] crawlFeatures) {

        final Map<String, String> categories = new HashMap<String, String>();
        for (final String feature : crawlFeatures) {
            Chunking chunking = chunker.chunk(feature);
            Set<Chunk> test = chunking.chunkSet();
            for (Chunk c : test) {
                System.out.println("Test String is = " + feature.substring(c.start(), c.end()) + " : Category is = " + c.type());
                categories.put(feature.substring(c.start(), c.end()), c.type());
            }
        }
        return categories;
    }

    public static void main(String[] args) throws Exception {

//        File f = new File("src/main/resources/testSkill.txt");
//
//        BufferedReader b = new BufferedReader(new FileReader(f));
//
//        String readLine = "";
//
//        System.out.println("Reading file using Buffered Reader");
//        List<String> checkList = new ArrayList<>();
//        
//        while ((readLine = b.readLine()) != null) {
//            checkList.add(readLine);
//            //System.out.println(readLine);
//        }
//        
        NER_Testing ner = new NER_Testing();
//        
//        String[] stockArr = new String[checkList.size()];
//        stockArr = checkList.toArray(stockArr);
        
        String text = new String(Files.readAllBytes(Paths.get("src/main/resources/testSkill.txt")), StandardCharsets.UTF_8);
        String text2 = "About 10 + years of experience in IT industry emphasis on analysis, designing, development, and testing.\n";
        String text3 ="Experience in all phases of software development life cycle including project planning, analysis, design, development, testing, deployment, and maintenance.\n";
String text4  = "Strong Object Orient Programming and Designing skills\n" +
"Strong Project Management skills.\n" +
"Good Hands on experience in Distributed Computing Environment (Hadoop, Spark, Java Streaming APIs)\n" +
"Good Hands on experience in Big Data Tools like ( HDFS, YARN, Sqoop, Sqoop2, Hive, SOLR, Hue, Kafka, Storm, etc)\n" +
"Strong Hands on experience on installing and managing Cloudera.\n" +
"Strong skills in managing and working on linux servers (Ubuntu, CentOS, Redhat)\n" +
"Strong development skills in NoSQL (SOLR, MongoDB, Elastic Search)\n" +
"Strong hands on experience in designing and development of Lambda Architecture with in all sections (Batch Layer, Speed Layer and Serving Layer)\n" +
"Good Hands on experience in Machine Learning Algorithms (SVM, Linear Regression, GBM, Gradient Decent, Ensembles)\n" +
"Strong in RDBMS, in writing Stored Procedures, Indexes and Triggers using MYSQL and Query Optimization.\n" +
"Strong programming skills in JAVA using Native, Maven and SPRINGS\n" +
"Strong programming skills in Web Services, Ajax, JQuery, JQuery Ajax, EXTJS, XML, XSL, XSLT,  XPath, XQuery, SOAP and Web 2.0\n" +
"Good hands on experience in cloud computing (Amazon S3, EC2, RDS, VPC)\n" +
"Strong programming skills in PHP frameworks (Codeigniter, Zend,Yii) and Smarty.\n" +
"Good hand on experience of testing high end products with thousands of user traffic.\n" +
"Good Hands on experience in EWay, DOBA, Google, Amazon, Paypal(Parallel, Adaptive, Standard, Pro), 2Checkout, Authorize.net APIs and web services.\n" +
"Strong skills in data mining, text mining using different methods and techniques\n" +
"Good Hands on experience in R and Weka.\n" +
"Expert in using GIT/GIT Flow (both on command line and UI Tools) and SVN for code versioning and maintenance.";

        String [] s1 = {text2,text3,text4}; 
        
        ner.getCategories(s1);
    }

}
