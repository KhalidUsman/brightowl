/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ML_Package;

import com.aliasi.chunk.CharLmRescoringChunker;
import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.ChunkAndCharSeq;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.ChunkerEvaluator;
import com.aliasi.chunk.Chunking;
import com.aliasi.chunk.ChunkingEvaluation;
import com.aliasi.classify.PrecisionRecallEvaluation;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.Parser;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.datavec.api.util.ClassPathResource;

/**
 *
 * @author shahzad
 */
public class NER_CrossValidation {

    private final Parser<ObjectHandler<Chunking>> mParser;

    static final String DEGREE_TAG = "DEGREE";
    static final String EDUCATION_TAG = "EDUCATION";
    static final String LOCATION_TAG = "LOCATION";
    static final String SKILL_TAG = "SKILL";
    static final String FUNCTIONAL_TITLE_TAG = "FUNCTIONTITLE";
    static final String KNOWLEDGE_TAG = "KNOWLEDGE";
    static final String ORGNIZATION_TAG = "ORGANIZATION";
    static final int NUM_FOLDS = 10;

    static int HMM_N_GRAM_LENGTH = 8;
    static int NUM_CHARS = 256;
    static double HMM_INTERPOLATION_RATIO = 8;
    static int NUM_ANALYSES_RESCORED = 10;

    static final String[] REPORT_TYPES = new String[]{
        DEGREE_TAG,
        EDUCATION_TAG,
        LOCATION_TAG,
        SKILL_TAG,
        FUNCTIONAL_TITLE_TAG,
        KNOWLEDGE_TAG,
        ORGNIZATION_TAG

    };

    private final ChunkingEvaluation mEvaluation
            = new ChunkingEvaluation();

    public NER_CrossValidation(Parser<ObjectHandler<Chunking>> parser) {
        mParser = parser;
    }

    public ChunkingEvaluation evaluation() {
        return mEvaluation;
    }

    public void singleFoldTestWithOldModel(File refFile) throws IOException, ClassNotFoundException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        mParser.parse(refFile);
        List<Chunking> refChunkings = refCollector.mChunkingList;

        System.out.println("Total sentences found " + refChunkings.size());

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        File modelFile = new File("src/main/resources/EducationSkillsLocationFuncPersonKnow.model");
        Chunker compiledChunker = (Chunker) AbstractExternalizable.readObject(modelFile);

        foldEvaluator.setChunker(compiledChunker);

        System.out.println("evaluating");
        for (int i = 0; i < refChunkings.size(); ++i) {
            try {
                foldEvaluator.handle(refChunkings.get(i));
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println(refChunkings.get(i));
            }
        }
        //System.out.println(foldEvaluator.toString());
        printEval("X-VAL", foldEvaluator);

    }

    static void printEval(String msg, ChunkerEvaluator evaluator) {
        System.out.println("In the PrintEval");
        ChunkingEvaluation chunkingEval = evaluator.evaluation();
//        Set<ChunkAndCharSeq> ccsSet = chunkingEval.truePositiveSet();
//        for (ChunkAndCharSeq ccs : ccsSet) {
//            System.out.println("String: " + ccs.charSequence() + " -- Type: " + ccs.chunk().type());
//        }
        Set<Chunking[]> chunkingSet = chunkingEval.cases();
//        ChunkingEvaluation mEvaluation
//        = new ChunkingEvaluation();
        for (Chunking[] sentences : chunkingSet) {
            System.out.println("Test String: " + sentences[0].charSequence());
            int i = 0;
            for (Chunking chunking : sentences) {
                CharSequence sentence = chunking.charSequence();

                for (Chunk chunk : chunking.chunkSet()) {
                    if (i == 0) {
                        System.out.print("Input - ");
                    } else {
                        System.out.print("Output - ");
                    }
                    System.out.println("SubString:  [" + sentence.subSequence(chunk.start(), chunk.end()) + "] : Start - End: "+ chunk.start() + " - " + chunk.end() + " : Category is = " + chunk.type());
                }
                i++;
            }
       //       mEvaluation.addCase(sentences[0], sentences[1]);
        }
        //System.out.println(mEvaluation.toString());

//        Set<Chunk> test = chunking.chunkSet();
//            for (Chunk c : test) {
//                System.out.println("Test String is = " + feature.substring(c.start(), c.end()) + " : Category is = " + c.type());
//                categories.put(feature.substring(c.start(), c.end()), c.type());
//            }
        for (String tag : REPORT_TYPES) {
            PrecisionRecallEvaluation prEvalByType
                    = chunkingEval
                            .perTypeEvaluation(tag).precisionRecallEvaluation();
            //Set<ChunkAndCharSeq> ch = chunkingEval.perTypeEvaluation(tag).falsePositiveSet();
            System.out.printf("%10s    %6s P=%5.3f R=%5.3f F=%5.3f\n",
                    msg,
                    tag,
                    prEvalByType.precision(),
                    prEvalByType.recall(),
                    prEvalByType.fMeasure());
        }
//
        PrecisionRecallEvaluation prEval
                = chunkingEval.precisionRecallEvaluation();
        System.out.println("");
        System.out.printf("%10s  COMBINED P=%5.3f R=%5.3f F=%5.3f\n",
                msg,
                prEval.precision(),
                prEval.recall(),
                prEval.fMeasure());

        // uncomment to dump out precsion-recall evals
        // ScoredPrecisionRecallEvaluation scoredPrEval
        // = evaluator.confidenceEvaluation();
        // double[][] prCurve = scoredPrEval.prCurve(true);
        // System.out.printf("%5s %5s\n","REC","PREC");
        // for (double[] rp : prCurve) {
        // if (rp[0] < 0.7) continue;
        // System.out.printf("%5.3f %5.3f\n",
        // rp[0],rp[1]);
        // }
        // System.out.printf("MAX F=%5.3f\n",scoredPrEval.maximumFMeasure());
    }

    public void kFold(File refFile) throws IOException, ClassNotFoundException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        mParser.parse(refFile);
        List<Chunking> sentences = refCollector.mChunkingList;
        int numSentences = sentences.size();
        System.out.println("Total sentences found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;

        for (int fold = NUM_FOLDS; --fold >= 0;) {
            int startTestFold = fold * numSentences / NUM_FOLDS;
            int endTestFold = (fold + 1) * numSentences / NUM_FOLDS;
            if (fold == NUM_FOLDS - 1) {
                endTestFold = numSentences; // round up on last fold
            }
            System.out.println("-----------------------------------------------");
            System.out.printf("FOLD=%2d  start sent=%5d  end sent=%5d\n",
                    fold, startTestFold, endTestFold);

            ChunkerEvaluator foldEvaluator
                    = new ChunkerEvaluator(null);
            foldEvaluator.setMaxConfidenceChunks(1);
            foldEvaluator.setMaxNBest(1);
            foldEvaluator.setVerbose(false);

            // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
            // uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
            CharLmRescoringChunker chunker
                    = new CharLmRescoringChunker(tokenizerFactory,
                            NUM_ANALYSES_RESCORED,
                            HMM_N_GRAM_LENGTH,
                            NUM_CHARS,
                            HMM_INTERPOLATION_RATIO,
                            true);

            System.out.println("training labeled data");
            for (int i = 0; i < startTestFold; ++i) {
                chunker.handle(sentences.get(i));
            }
            for (int i = endTestFold; i < numSentences; ++i) {
                chunker.handle(sentences.get(i));
            }
//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }

            System.out.println("compiling");
            Chunker compiledChunker
                    = (Chunker) AbstractExternalizable.compile(chunker);

            foldEvaluator.setChunker(compiledChunker);

            System.out.println("evaluating");
            for (int i = startTestFold; i < endTestFold; ++i) {
                try {
                    foldEvaluator.handle(sentences.get(i));
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                    System.out.println(sentences.get(i));
                }
            }
            printEval("FOLD=" + fold, foldEvaluator);
        }
    }

    public void singleFoldTrainAndTest(File refFile) throws ClassNotFoundException, IOException {
        ChunkingCollector refCollector = new ChunkingCollector();
        mParser.setHandler(refCollector);
        
        mParser.parse(refFile);
        List<Chunking> sentences = refCollector.mChunkingList;
        int numSentences = sentences.size();
        System.out.println("Total sentences found " + numSentences);

        TokenizerFactory tokenizerFactory
                = IndoEuropeanTokenizerFactory.INSTANCE;

        ChunkerEvaluator foldEvaluator
                = new ChunkerEvaluator(null);
        foldEvaluator.setMaxConfidenceChunks(1);
        foldEvaluator.setMaxNBest(1);
        foldEvaluator.setVerbose(false);

        // comment to use plain HmmChunker
//            HmmCharLmEstimator hmmEstimator
//                    = new HmmCharLmEstimator(HMM_N_GRAM_LENGTH,
//                            NUM_CHARS,
//                            HMM_INTERPOLATION_RATIO);
        // uncomment to evaluation plain Hmm chunker
//             CharLmHmmChunker chunker
//             = new CharLmHmmChunker(tokenizerFactory,hmmEstimator,true);
        CharLmRescoringChunker chunker
                = new CharLmRescoringChunker(tokenizerFactory,
                        NUM_ANALYSES_RESCORED,
                        HMM_N_GRAM_LENGTH,
                        NUM_CHARS,
                        HMM_INTERPOLATION_RATIO,
                        true);

        System.out.println("training labeled data");
        for (int i = 0; i < sentences.size(); ++i) {
            chunker.handle(sentences.get(i));
        }

//            if (USE_DICTIONARY) {
//                System.out.println("training dictionary");
//                for (String locName : locDict)
//                    if (locName.length() > 0)
//                        chunker.trainDictionary(locName,LOC_TAG);
//                for (String orgName : orgDict)
//                    if (orgName.length() > 0)
//                        chunker.trainDictionary(orgName,ORG_TAG);
//                for (String persName : persDict)
//                    if (persName.length() > 0)
//                        chunker.trainDictionary(persName,PERS_TAG);
//            }
        System.out.println("compiling");
        Chunker compiledChunker
                = (Chunker) AbstractExternalizable.compile(chunker);

        foldEvaluator.setChunker(compiledChunker);

        System.out.println("evaluating");
        for (int i = 0; i < sentences.size(); ++i) {
            try {
                foldEvaluator.handle(sentences.get(i));
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println(sentences.get(i));
            }
        }
        printEval("X-VAL", foldEvaluator);

    }

    private static class ChunkingCollector
            implements ObjectHandler<Chunking> {

        private final List<Chunking> mChunkingList = new ArrayList<Chunking>();

        public void handle(Chunking chunking) {
            mChunkingList.add(chunking);
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //File refFile = new File("src/main/resources/EducationSkillsLocationFuncPersonKnow.txt");
        File refFile = new File("src/main/resources/testSkillRaw.txt");

        @SuppressWarnings("deprecation")
        Parser<ObjectHandler<Chunking>> parser
                = new Muc6ChunkParser();
        NER_CrossValidation scorer = new NER_CrossValidation(parser);
        scorer.singleFoldTestWithOldModel(refFile);
        //scorer.kFold(refFile);
        //scorer.singleFoldTrainAndTest(refFile);

        //System.out.println(scorer.evaluation().toString());
    }
}
