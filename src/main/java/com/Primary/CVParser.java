package com.Primary;

import com.dbConnection.Connect_Database;
import com.dbConnection.Expert;
import org.apache.commons.lang3.ArrayUtils;

import java.io.FileWriter;
import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class CVParser {

    static <T> T[] joinArrayGeneric(T[]... arrays) {
        int length = 0;
        for (T[] array : arrays) {
            length += array.length;
        }

        //T[] result = new T[length];
        final T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), length);

        int offset = 0;
        for (T[] array : arrays) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static boolean detectLanguage(String str) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9 ]");
        return p.matcher(str).find();
    }

    public static void main(String[] args) throws Exception {

        String textFilePath = args[0];
        //String textFilePath = "C:\\Users\\Xorlogics-ML\\Desktop\\Crawl.txt";

        Connect_Database dao = new Connect_Database();
        dao.readDataBase();
        ResultSet expertsForExperts = dao.getExperts();
        List<Integer> listExpForExperts = new ArrayList<Integer>();
        while (expertsForExperts.next()) {
            listExpForExperts.add(expertsForExperts.getInt("expert_Id"));
        }
        dao.close();

        FileWriter fw = new FileWriter(textFilePath);
        for (int i=0; i<listExpForExperts.size(); i++) {
            Expert expertObj = new Expert( listExpForExperts.get( i ) );
            String expertPersonalities[] = expertObj.getExpertPersonalities();
            String expertKnowledge[] = expertObj.getExpertKnowledge();
            String expertExpertise[] = expertObj.getExpertExpertise();
            String expertEducation[] = expertObj.getExpertEducation();
            String expertLanguages[] = expertObj.getExpertLanguages();
            String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
            String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
            String[] combineFuncTitles = (String[]) ArrayUtils.addAll( expertFuncTitles, expertFuncTitlesInterested );
            String expertLocation[] = expertObj.getExpertLocation();
            String expertCertificates[] = expertObj.getExpertCertificates();
            String expertTraining[] = expertObj.getExpertTraining();
            String expertPublications = expertObj.getExpertPublication();

            String expertSummary = expertObj.getExpertSummary();
            String expertLinkedInSummary = expertObj.getExpertLinkedInSummary();

            String[] combineExpert = joinArrayGeneric( expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation, expertCertificates, expertTraining);
            String combineExpertStr = Arrays.toString( combineExpert ) + " " + expertPublications + " " + expertSummary + " " + expertLinkedInSummary;
            String cleanExpertStr = combineExpertStr.replaceAll("\\<[^>]*>"," ").replace("&nbsp;"," ").replace( "amp;", "" ).replace( "[", "" ).replace( "]", "" ).replace( "&", " and " ).replace( ",", " " ).replace( ":", " ").replace( "|", " " ).replace( "(", " " ).replace( ")", " " ).replace( ".", " " );

            fw.write( cleanExpertStr );
            fw.write( System.getProperty( "line.separator" ) );
            expertObj.close();
        }
        fw.close();
        System.out.println("Done");
    }
}
