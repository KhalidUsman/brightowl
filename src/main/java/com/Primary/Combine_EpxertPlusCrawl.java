package com.Primary;

import com.dbConnection.Connect_Database;
import com.dbConnection.Expert;
import org.apache.commons.lang3.ArrayUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Combine_EpxertPlusCrawl {

    static <T> T[] joinArrayGeneric(T[]... arrays) {
        int length = 0;
        for (T[] array : arrays) {
            length += array.length;
        }

        //T[] result = new T[length];
        final T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), length);

        int offset = 0;
        for (T[] array : arrays) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static boolean detectLanguage(String str) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9 ]");
        return p.matcher(str).find();
    }

    public static void main(String[] args) throws Exception {

        String textFilePath = args[0];

        Connect_Database daoCrawl = new Connect_Database();
        daoCrawl.readDataBase();
        ResultSet crawledProjects = daoCrawl.getCrawledProjects();

        FileWriter fw = new FileWriter(textFilePath);
        while (crawledProjects.next()) {
            String crawledText = crawledProjects.getString("project_description").replaceAll("\\<[^>]*>"," ").replace( "&gt;" , " ").replace("&nbsp;"," ").replace("&amp;"," ")
                    .replace("&"," and ").replace("/"," or ").replaceAll("[ ]+"," ").replaceAll("[^A-Za-z0-9 .]", " ").replace("e.g."," ").replace("e.g"," ")
                    .replace("e g"," ").replace("’","").replace("Bachelor s", "Bachelors").replace("B sc","BSc").replace("M Sc","MSc").replace("B.sc","BSc")
                    .replace("M.Sc","MSc").replace("T cell", "T-cell").replace("B cell", "B-cell").replace("infl ammation", "inflammation").replace("P hD","PhD").replace("P.hD","PhD")
                    .replace("R D", "R and D").replace("pre clini cal", "pre-clinical").replace("clini cal", "clinical").replace("e commerce", "e-commerce").replace("cru ciate", "cruciate")
                    .replace("Dail y", "Daily").replace("3years", "3 years").replace("dis ability", "disability").replace("opportu nities", "opportunities").replace("therapeu tic", "therapeutic")
                    .replace("mappi ngs", "mappings").replace("stakehol der", "stakeholder").replace("i n", "in").replace("theo ries", "theories").replace("Microso ft", "Microsoft")
                    .replace("benchm arks", "benchmarks").replace("fo r", "for").replace("sr." , "senior").replace("jr.", "junior").replace("i.e."," ").replace("i.e"," ")
                    .replace("i e,"," ").replace("i e"," ");
            String[] sentences = crawledText.split("\\.\\s+");
            for (String str : sentences) {
                fw.write(str);
                fw.write(System.getProperty( "line.separator" ));
            }
        }
        fw.write(System.getProperty( "line.separator" ));
        System.out.println("Done");
    }
}
