package com.Primary;

import org.datavec.api.util.ClassPathResource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class KeywordReplacer {
    public static void main(String[] args) throws Exception {

        String keywordPath = args[0];
        String inputPath = args[1];
        String outputPath = args[2];
        FileInputStream fstream = new FileInputStream(keywordPath);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String strLine;

        //String fileUpdatePath = new ClassPathResource("Crawl.txt").getFile().getAbsolutePath();
        Path path = Paths.get(inputPath);
        Path path1 = Paths.get(outputPath);
        Charset charset = StandardCharsets.UTF_8;
        String content = new String( Files.readAllBytes(path), charset);
        //Read File Line By Line
        while ((strLine = br.readLine()) != null)   {
            String underScoreStr = " " + strLine.replace(" ", "_").toLowerCase() + " ";
            content = content.replaceAll(" " + strLine.toLowerCase() + " ", underScoreStr);
        }
        //Close the input stream
        Files.write(path1, content.getBytes(charset));
        br.close();
        System.out.println("Done");
    }
}
