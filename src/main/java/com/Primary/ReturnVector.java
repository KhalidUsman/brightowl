/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Primary;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 *
 * @author shahzad
 */
public class ReturnVector implements Serializable {

    Hashtable<String, values> word_freq_vector = new Hashtable<String, values>();
    ArrayList<String> Distinct_words_text_1_2 = new ArrayList<String>();

    public ReturnVector(Hashtable<String, values> word_freq_text, ArrayList<String> Distinct_words) {
        this.word_freq_vector = word_freq_text;
        this.Distinct_words_text_1_2 = Distinct_words;
    }

}

// Calculate Cosine Similarity between two texts
class values implements Serializable {

    double val1;
    double val2;

    values(double v1, double v2) {
        this.val1 = v1;
        this.val2 = v2;
    }

    public void Update_VAl(double v1, double v2) {
        this.val1 = v1;
        this.val2 = v2;
    }
}
