package com.Primary;

import com.dbConnection.Connect_Database;
import com.google.common.base.Optional;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

public class LangDetector {

    /*
    public static Boolean detectLang(String query) {
        // Language Detection

        String API_Key = "d0c3e1b8f654321e8c4d67955dba3b80";

        String text = "http://ws.detectlanguage.com/0.2/detect?q=" + query + "&key=" + API_Key;
        text = Normalizer.normalize(text, Normalizer.Form.NFD);
        String url = text.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        System.out.println("Original String = " + url);
        String keyword = "";
        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("q", keyword));
        postParameters.add(new BasicNameValuePair("key", "d0c3e1b8f654321e8c4d67955dba3b80"));

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(url);
            HttpResponse result = httpClient.execute(request);
            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            try {
                JSONParser parser = new JSONParser();
                Object resultObject = parser.parse(json);
                if (resultObject instanceof JSONObject) {
                    JSONObject obj =(JSONObject)resultObject;
                    JSONObject objData = (JSONObject)obj.get("data");
                    JSONArray arr = (JSONArray) objData.get("detections");
                    JSONObject dicObj = (JSONObject)arr.get(0);
                    System.out.println("Khalid string is " + dicObj.get("language"));
                    String langCode = (String) dicObj.get("language");
                    if (langCode.equals("en"))
                        return true;
                    return false;
                }
                else {
                    System.out.println("It's not a JSON Object");
                    return false;
                }

            } catch (Exception e) {
                // TODO: handle exception
                return false;
            }

        } catch (IOException ex) {
            // TODO: handle exception
            return false;
        }
    }
    */

    public static boolean detectLanguage(String str) {
        Pattern p = Pattern.compile("[^a-zA-Z0-9 ./!]");
        return p.matcher(str).find();
    }

    public static void main(String[] args) throws Exception {

        String textFilePath = args[0];
        String cleansingFilePath = args[1];
        //String textFilePath = "DataCleansing.txt";
        Connect_Database dao = new Connect_Database();
        dao.readDataBase();
        ResultSet crawledProjects = dao.getCrawledProjects();

        File file = new File(cleansingFilePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;

        FileWriter fw = new FileWriter(textFilePath);

        while (crawledProjects.next()) {

            String crawledText = crawledProjects.getString("project_description").replaceAll("\\<[^>]*>"," ").toLowerCase();
            while ((st = br.readLine()) != null) {
                String[] replace_Arr = st.split("!");
                if (replace_Arr[1].equals("space")) {
                    crawledText = crawledText.replace(replace_Arr[0].toLowerCase()," ");
                }
                else if (replace_Arr[1].equals("remove")) {
                    crawledText = crawledText.replace(replace_Arr[0].toLowerCase(),"");
                }
                else {
                    crawledText = crawledText.replace(replace_Arr[0].toLowerCase(),replace_Arr[1].toLowerCase());
                }
            }
            crawledText = crawledText.replaceAll("[^A-Za-z0-9 .,$']", " ").replaceAll("\\s{2,}", " ");
            String[] sentences = crawledText.split("\\.\\s+");
            for (String str : sentences) {
                fw.write(str);
                fw.write(System.getProperty( "line.separator" ));
            }
            fw.write(System.getProperty( "line.separator" ));
        }
        fw.close();
        System.out.println("Done");
    }
}
