package com.Keyword_Extraction.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.datavec.api.util.ClassPathResource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

/**
 * Created by askdrcatcher on 3/16/17.
 */
public class FileUtil implements Iterator<String> {

    private String filePath;
    private LineIterator iterator;

    public FileUtil(String filePath) {
        this.filePath = filePath;
    }

    private boolean isFilePathEmpty() {
        if (filePath == null) return true;
        filePath = filePath.trim();
        return filePath.isEmpty();
    }

    private File getFile() throws FileNotFoundException {
        ClassLoader thisClassLoader = getClass().getClassLoader();
        URL fileUrl = this.isFilePathEmpty() ? thisClassLoader.getResource("FoxStoplist.txt") :
                thisClassLoader.getResource("SmartStoplist.txt");
        String filePath = new ClassPathResource("SmartStoplist.txt").getFile().getAbsolutePath();
        return new File(filePath);
    }

    private File getUserProvidedFile() {
        ClassLoader thisClassLoader = getClass().getClassLoader();
        URL fileUrl = thisClassLoader.getResource(filePath);
        return new File(fileUrl.getFile());
    }

    public FileUtil iterator() throws IOException {
        //close already created iterator
        if(iterator != null) closeIterator();
        iterator = FileUtils.lineIterator(getFile(), "UTF-8");
        return this;
    }

    private void closeIterator () {
        LineIterator.closeQuietly(iterator);
        iterator = null;
    }

    @Override
    public boolean hasNext() {
        boolean hasNext = iterator.hasNext();
        if (!hasNext) closeIterator();
        return hasNext;
    }

    @Override
    public void remove() {
        System.out.println("Remove");
    }

    @Override
    public String next() {
        return iterator.nextLine();
    }
}
