package com.CrossValidation;

import org.datavec.api.util.ClassPathResource;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;

import java.io.*;

public class CrossValidation_kFold {

    public CrossValidation_kFold() throws FileNotFoundException {
    }

     //To separate all of the categories from combined category file
//    public static void main (String [] args) throws IOException {
//        String filePathCorpus = new ClassPathResource("EducationSkillsLocationFuncPersonKnow.txt").getFile().getAbsolutePath();
//
//        FileWriter fw = new FileWriter("C:\\Users\\Xorlogics-ML\\Desktop\\Separate Files\\PERSONALITY.txt");
//
//        File file = new File(filePathCorpus);
//        BufferedReader br = new BufferedReader(new FileReader(file));
//        String keyword;
//
//        while ((keyword = br.readLine()) != null) {
//            if (keyword.contains("PERSONALITY") && (!keyword.contains("EDUCATION") && !keyword.contains("DEGREE") && !keyword.contains("SKILL") && !keyword.contains("LANGUAGE") && !keyword.contains("LOCATION") && !keyword.contains("FUNCTIONTITLE") && !keyword.contains("KNOWLEDGE"))) {
//                fw.write(keyword);
//                fw.write(System.getProperty( "line.separator" ));
//            }
//        }
//        fw.close();
//    }

    // To separate for 10 folds
//    public static void main (String [] args) throws IOException {
//
//        for (int j=0;j<10;j++) {
//            File file = new File("C:\\Users\\Xorlogics-ML\\Desktop\\Separate Files\\PERSONALITY.txt");
//            BufferedReader br = new BufferedReader(new FileReader(file));
//            String keyword;
//            int i = 0;
//            String fileTrain = "C:\\Users\\Xorlogics-ML\\Desktop\\Separate Files\\Fold" + (j+1) + "\\PERSONALITY_Train.txt";
//            String fileTest = "C:\\Users\\Xorlogics-ML\\Desktop\\Separate Files\\Fold" + (j+1) + "\\PERSONALITY_Test.txt";
//            FileWriter fw_train = new FileWriter(fileTrain);
//            FileWriter fw_test = new FileWriter(fileTest);
//            while ((keyword = br.readLine()) != null) {
//                i++;
//                if (i%10==j) {
//                    fw_test.write(keyword);
//                    fw_test.write(System.getProperty( "line.separator" ));
//                }
//                else {
//                    fw_train.write(keyword);
//                    fw_train.write(System.getProperty( "line.separator" ));
//                }
//            }
//            fw_train.close();
//            fw_test.close();
//            br.close();
//        }
//    }

    // File read / write
        public static void main (String [] args) throws IOException {

        String resultsFilePath = "C:\\Users\\Xorlogics-ML\\Desktop\\Separate Files\\Fold1\\Combine_Test_Final.txt";

        FileWriter fw1 = new FileWriter("C:\\Users\\Xorlogics-ML\\Desktop\\actual.txt");
        FileWriter fw2 = new FileWriter("C:\\Users\\Xorlogics-ML\\Desktop\\predicted.txt");

        File file = new File(resultsFilePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String keyword;

        while ((keyword = br.readLine()) != null) {
            System.out.println(keyword);
            fw1.write(keyword.split("!")[1]);
            fw1.write(System.getProperty( "line.separator" ));
            fw2.write(keyword.split("!")[2]);
            fw2.write(System.getProperty( "line.separator" ));
        }
        fw1.close();
        fw2.close();
    }

//    // To save in array
//    public static void main (String [] args) throws IOException {
//
//        String resultsFilePath = "C:\\Users\\Xorlogics-ML\\Desktop\\MATLAB\\predicted.txt";
//
//        FileWriter fw1 = new FileWriter("C:\\Users\\Xorlogics-ML\\Desktop\\predicted.txt");
//
//        File file = new File(resultsFilePath);
//        BufferedReader br = new BufferedReader(new FileReader(file));
//        String keyword;
//
//        while ((keyword = br.readLine()) != null) {
//            fw1.write("'" + keyword + "'" + ",");
//        }
//        fw1.close();
//    }
}
